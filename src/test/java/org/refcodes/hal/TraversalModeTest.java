// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.hal;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.refcodes.runtime.SystemProperty;

public class TraversalModeTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testHrefModes() {
		String eHrefText;
		for ( TraversalMode eMode : TraversalMode.values() ) {
			eHrefText = "";
			if ( !eMode.isImportChildren() ) {
				eHrefText += ( eHrefText.length() != 0 ? "_" : "" ) + "NONE";
			}
			if ( eMode.nextMode() != null && eMode.nextMode().isImportChildren() && eMode.isImportChildren() ) {
				eHrefText += ( eHrefText.length() != 0 ? "_" : "" ) + "RECURSIVE";
			}
			if ( eMode.isImportChildren() ) {
				eHrefText += ( eHrefText.length() != 0 ? "_" : "" ) + "IMPORT_CHILDREN";
			}
			if ( eMode.isKeepSelfHrefs() ) {
				eHrefText += ( eHrefText.length() != 0 ? "_" : "" ) + "KEEP_SELF_HREFS";
			}
			if ( eMode.isKeepDanglingHrefs() ) {
				eHrefText += ( eHrefText.length() != 0 ? "_" : "" ) + "KEEP_DANGLING_HREFS";
			}
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( eMode.name() + " := " + eHrefText );
			}
			assertEquals( eHrefText, eMode.name() );
		}
	}
}
