// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.hal;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.refcodes.data.Scheme;
import org.refcodes.exception.MarshalException;
import org.refcodes.net.PortManagerSingleton;
import org.refcodes.rest.HttpRestClient;
import org.refcodes.rest.RestRequestBuilder;
import org.refcodes.rest.RestResponse;
import org.refcodes.rest.RestfulHttpClient;
import org.refcodes.runtime.Execution;
import org.refcodes.runtime.SystemProperty;
import org.refcodes.struct.ext.factory.JsonCanonicalMapFactorySingleton;
import org.refcodes.struct.ext.factory.TomlCanonicalMapFactorySingleton;
import org.refcodes.web.HttpStatusException;
import org.refcodes.web.MediaType;
import org.refcodes.web.Url;

public class HalClientTest extends AbstractHalClientTest {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Disabled("!!! NEEDED IN CASE OF EXTERNAL DEBUGGING !!!")
	@Test
	public void testStartHalServer() throws MalformedURLException, HttpStatusException {
		final Integer theHalPort = PortManagerSingleton.getInstance().bindAnyPort();
		final HalServer theHalServer = new HalServer( theHalPort );
		createTestFixures( theHalServer );
		System.out.println( "Take a break(point) here." );
	}

	@Disabled("!!! IGNORE AS LONG AS THE FASTERXML SERIALIZATION FAILS DUE TO THIRD PARTY BUGS !!!")
	@Test
	public void testCreate() throws MalformedURLException, HttpStatusException {
		final Integer theHalPort = PortManagerSingleton.getInstance().bindAnyPort();
		final HalServer theHalServer = new HalServer( theHalPort );
		createTestFixures( theHalServer );
		final HalClient theHalClient = new HalClient( new Url( Scheme.HTTP, "localhost", theHalPort, "/" ) );
		final User theUser = new User( "John Romero", "John@Romero.com" );
		HalData theResult = theHalClient.create( "users", theUser );
		final User theOtherUser = theResult.toType( User.class );
		assertEquals( theUser, theOtherUser );
		final Person thePerson = createPerson( "C" );
		theResult = theHalClient.create( "persons", thePerson );
		final Person theOtherPerson = theResult.toType( Person.class );
		assertEquals( thePerson, theOtherPerson );
	}

	@Test
	public void testReadLibraryEntity() throws HttpStatusException, MarshalException, IOException {
		final Integer theHalPort = PortManagerSingleton.getInstance().bindAnyPort();
		final HalServer theHalServer = new HalServer( theHalPort );
		createTestFixures( theHalServer );
		final HalClient theHalClient = new HalClient( new Url( Scheme.HTTP, "localhost", theHalPort, "/" ) );
		String eValue;
		String[][][] eFixure;
		HalData eHalData;
		int y;
		for ( TraversalMode eMode : TraversalMode.values() ) {
			final List<HalData> eHalDatas = theHalClient.readAll( "libraries", eMode );
			eFixure = LIBARARIES.get( eMode );
			if ( eFixure != null ) {
				for ( int x = 0; x < eFixure.length; x++ ) {
					eHalData = eHalDatas.get( x );
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println();
					}
					if ( SystemProperty.LOG_TESTS.isEnabled() ) {
						System.out.println( TomlCanonicalMapFactorySingleton.getInstance().toMarshaled( eHalData ) );
					}
					assertEquals( eFixure[x].length, eHalData.size() );
					y = 0;
					for ( String eKey : eHalData.sortedKeys() ) {
						eValue = toNormalizedValue( theHalPort, eHalData.get( eKey ) );
						if ( SystemProperty.LOG_TESTS.isEnabled() ) {
							System.out.println( eKey + " = " + eValue + " (" + x + ", " + y + ")" );
						}
						assertEquals( eFixure[x][y][0], eKey );
						assertEquals( eFixure[x][y][1], eValue );
						y++;
					}
				}
			}
			else {
				System.err.println( "Warning, TraversalMode <" + eMode + "> is not being tested!" );
			}
		}
		theHalServer.destroy();
	}

	@Test
	public void testReadPaginatedLibraryEntity() throws HttpStatusException, MarshalException, IOException {
		final Integer theHalPort = PortManagerSingleton.getInstance().bindAnyPort();
		final HalServer theHalServer = new HalServer( theHalPort );
		createTestFixures( theHalServer );
		final HalClient theHalClient = new HalClient( new Url( Scheme.HTTP, "localhost", theHalPort, "/" ) );
		for ( TraversalMode eMode : TraversalMode.values() ) {
			final HalDataPage eHalDatas = theHalClient.readPage( "libraries", 1, 1, eMode );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "Result page number = " + eHalDatas.getPageNumber() );
				System.out.println( "Result page size = " + eHalDatas.getPageSize() );
				System.out.println( "Result page count = " + eHalDatas.getPageCount() );
			}
			assertEquals( 1, eHalDatas.getPageNumber() );
			assertEquals( 1, eHalDatas.getPageSize() );
			assertEquals( 2, eHalDatas.getPageCount() );
			assertEquals( 1, eHalDatas.size() );
		}
		theHalServer.destroy();
	}

	@Test
	public void testIntrospectAllHalStruct() throws MalformedURLException, HttpStatusException, MarshalException {
		final Integer theHalPort = PortManagerSingleton.getInstance().bindAnyPort();
		final HalServer theHalServer = new HalServer( theHalPort );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Starting HAL on port <" + theHalPort + ">: HTTP:\\localhost:" + theHalPort );
		}
		final HalClient theHalClient = new HalClient( new Url( Scheme.HTTP, "localhost", theHalPort, "/" ) );
		final String[] theEntites = theHalClient.entities();
		HalStruct eEntityStructure;
		for ( String eEntity : theEntites ) {
			for ( TraversalMode eMode : TraversalMode.values() ) {
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println( "*** " + eEntity + ": " + eMode + " ***" );
				}
				eEntityStructure = theHalClient.introspect( eEntity, eMode );
				if ( SystemProperty.LOG_TESTS.isEnabled() ) {
					System.out.println();
					System.out.println( "\"" + eEntity + "\" : " + JsonCanonicalMapFactorySingleton.getInstance().toMarshaled( eEntityStructure ) );
					System.out.println( "---" );
				}
			}
		}
		theHalServer.destroy();
	}

	@Test
	@Disabled("!!! FORE REVERSE ENGINEERING PURPOSES !!!")
	public void reverseEngineerHalStruct() throws MalformedURLException, HttpStatusException, MarshalException {
		final Integer theHalPort = PortManagerSingleton.getInstance().bindAnyPort();
		final HalServer theHalServer = new HalServer( theHalPort );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Starting HAL on port <" + theHalPort + ">: HTTP:\\localhost:" + theHalPort );
		}
		// ---------------------------------------------------------------------
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			final RestfulHttpClient theRestClient = new HttpRestClient();
			final RestRequestBuilder theRequest = theRestClient.buildGet( Scheme.HTTP, "localhost", theHalPort, "/profile" );
			final RestResponse theResponse = theRequest.toRestResponse();
			System.out.println( theResponse.getHttpBody() );
			System.out.println();
		}
		// ---------------------------------------------------------------------
		final HalClient theHalClient = new HalClient( new Url( Scheme.HTTP, "localhost", theHalPort, "/" ) );
		final String theEntity = "libraries";
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			final RestfulHttpClient theRestClient = new HttpRestClient();
			System.out.println( MediaType.APPLICATION_HAL_JSON + " --> " + theEntity );
			RestRequestBuilder theRequest = theRestClient.buildGet( Scheme.HTTP, "localhost", theHalPort, "/profile/" + theEntity );
			theRequest.getHeaderFields().putAcceptTypes( MediaType.APPLICATION_HAL_JSON );
			RestResponse theResponse = theRequest.toRestResponse();
			System.out.println( theResponse.getHttpBody() );
			System.out.println( MediaType.APPLICATION_SCHEMA_JSON + " --> " + theEntity );
			theRequest = theRestClient.buildGet( Scheme.HTTP, "localhost", theHalPort, "/profile/" + theEntity );
			theRequest.getHeaderFields().putAcceptTypes( MediaType.APPLICATION_SCHEMA_JSON );
			theResponse = theRequest.toRestResponse();
			System.out.println( theResponse.getHttpBody() );
		}
		final HalStruct theEntityStructure = theHalClient.introspect( theEntity, TraversalMode.NONE_KEEP_SELF_HREFS );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "---" );
			System.out.println( "\"" + theEntity + "\" : " + JsonCanonicalMapFactorySingleton.getInstance().toMarshaled( theEntityStructure ) );
			System.out.println( "---" );
			System.out.println( "\"" + theEntity + "\" : " + JsonCanonicalMapFactorySingleton.getInstance().toMarshaled( theEntityStructure.toPayload() ) );
			System.out.println( "---" );
		}
		// ---------------------------------------------------------------------
		theHalServer.destroy();
	}

	@Disabled("!!! FORE REVERSE ENGINEERING PURPOSES !!!")
	@Test
	public void reverseEngineerAllHalStruct() throws MalformedURLException, HttpStatusException, MarshalException {
		final Integer theHalPort = PortManagerSingleton.getInstance().bindAnyPort();
		final HalServer theHalServer = new HalServer( theHalPort );
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "Starting HAL on port <" + theHalPort + ">: HTTP:\\localhost:" + theHalPort );
		}
		// ---------------------------------------------------------------------
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			final RestfulHttpClient theRestClient = new HttpRestClient();
			final RestRequestBuilder theRequest = theRestClient.buildGet( Scheme.HTTP, "localhost", theHalPort, "/profile" );
			final RestResponse theResponse = theRequest.toRestResponse();
			System.out.println( theResponse.getHttpBody() );
			System.out.println();
		}
		// ---------------------------------------------------------------------
		final HalClient theHalClient = new HalClient( new Url( Scheme.HTTP, "localhost", theHalPort, "/" ) );
		final String[] theEntites = theHalClient.entities();
		HalStruct eEntityStructure;
		for ( String eEntity : theEntites ) {
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				final RestfulHttpClient theRestClient = new HttpRestClient();
				System.out.println( MediaType.APPLICATION_HAL_JSON + " --> " + eEntity );
				RestRequestBuilder theRequest = theRestClient.buildGet( Scheme.HTTP, "localhost", theHalPort, "/profile/" + eEntity );
				theRequest.getHeaderFields().putAcceptTypes( MediaType.APPLICATION_HAL_JSON );
				RestResponse theResponse = theRequest.toRestResponse();
				System.out.println( theResponse.getHttpBody() );
				System.out.println( MediaType.APPLICATION_SCHEMA_JSON + " --> " + eEntity );
				theRequest = theRestClient.buildGet( Scheme.HTTP, "localhost", theHalPort, "/profile/" + eEntity );
				theRequest.getHeaderFields().putAcceptTypes( MediaType.APPLICATION_SCHEMA_JSON );
				theResponse = theRequest.toRestResponse();
				System.out.println( theResponse.getHttpBody() );
			}
			eEntityStructure = theHalClient.introspect( eEntity, TraversalMode.RECURSIVE_IMPORT_CHILDREN_KEEP_SELF_HREFS_KEEP_DANGLING_HREFS );
			if ( SystemProperty.LOG_TESTS.isEnabled() ) {
				System.out.println( "---" );
				System.out.println( "\"" + eEntity + "\" : " + JsonCanonicalMapFactorySingleton.getInstance().toMarshaled( eEntityStructure ) );
				System.out.println( "---" );
			}
		}
		// ---------------------------------------------------------------------
		theHalServer.destroy();
	}

	@Disabled("!!! FORE REVERSE ENGINEERING PURPOSES !!!")
	@Test
	public void reverseEngineerAllHalData() throws MalformedURLException, HttpStatusException, MarshalException {
		final Integer theHalPort = PortManagerSingleton.getInstance().bindAnyPort();
		final HalServer theHalServer = new HalServer( theHalPort );
		createTestFixures( theHalServer );
		System.out.println( "Starting HAL on port <" + theHalPort + ">: HTTP:\\localhost:" + theHalPort );
		// ---------------------------------------------------------------------
		final RestfulHttpClient theRestClient = new HttpRestClient();
		RestRequestBuilder theRequest = theRestClient.buildGet( Scheme.HTTP, "localhost", theHalPort, "/books" );
		RestResponse theResponse = theRequest.toRestResponse();
		System.out.println( theResponse.getHttpBody() );
		System.out.println();
		theRequest = theRestClient.buildGet( Scheme.HTTP, "localhost", theHalPort, "/libraries" );
		theResponse = theRequest.toRestResponse();
		System.out.println( theResponse.getHttpBody() );
		System.out.println();
		// ---------------------------------------------------------------------
		final HalClient theHalClient = new HalClient( new Url( Scheme.HTTP, "localhost", theHalPort, "/" ) );
		final String[] theEntites = theHalClient.entities();
		for ( String eEntity : theEntites ) {
			System.out.println( "### " + eEntity + " ###" );
			System.out.println();
			final List<HalData> eHalDatas = theHalClient.readAll( eEntity, TraversalMode.RECURSIVE_IMPORT_CHILDREN_KEEP_DANGLING_HREFS );
			for ( HalData eMap : eHalDatas ) {
				for ( String eKey : eMap.sortedKeys() ) {
					System.out.println( eKey + ": " + eMap.get( eKey ) );
				}
				System.out.println();
			}
		}
		// ---------------------------------------------------------------------
		theHalServer.destroy();
	}

	@Disabled("!!! FORE REVERSE ENGINEERING PURPOSES !!!")
	@Test
	public void reverseEngineerHalData() throws HttpStatusException, MarshalException, IOException {
		final Integer theHalPort = PortManagerSingleton.getInstance().bindAnyPort();
		final HalServer theHalServer = new HalServer( theHalPort );
		createTestFixures( theHalServer );
		System.out.println( "Starting HAL on port <" + theHalPort + ">: HTTP:\\localhost:" + theHalPort );
		// ---------------------------------------------------------------------
		final RestfulHttpClient theRestClient = new HttpRestClient();
		RestRequestBuilder theRequest = theRestClient.buildGet( Scheme.HTTP, "localhost", theHalPort, "/books" );
		RestResponse theResponse = theRequest.toRestResponse();
		System.out.println( theResponse.getHttpBody() );
		System.out.println();
		theRequest = theRestClient.buildGet( Scheme.HTTP, "localhost", theHalPort, "/libraries" );
		theResponse = theRequest.toRestResponse();
		System.out.println( theResponse.getHttpBody() );
		System.out.println();
		// ---------------------------------------------------------------------
		final HalClient theHalClient = new HalClient( new Url( Scheme.HTTP, "localhost", theHalPort, "/" ) );
		final TraversalMode eMode = TraversalMode.RECURSIVE_IMPORT_CHILDREN_KEEP_SELF_HREFS;
		final List<HalData> eHalDatas = theHalClient.readAll( "libraries", eMode );
		for ( HalData eMap : eHalDatas ) {
			for ( String eKey : eMap.sortedKeys() ) {
				System.out.println( eKey + ": " + eMap.get( eKey ) );
			}
			final Library theLib = eMap.toPayload().toType( Library.class );
			System.out.println( theLib.toString() );
			System.out.println();
		}
		// ---------------------------------------------------------------------
		theHalServer.destroy();
	}

	@Test
	@Disabled("!!! FORE REVERSE ENGINEERING PURPOSES !!!")
	public void saveReverseData() throws HttpStatusException, MarshalException, IOException {
		final Integer theHalPort = PortManagerSingleton.getInstance().bindAnyPort();
		final HalServer theHalServer = new HalServer( theHalPort );
		createTestFixures( theHalServer );
		System.out.println( "Starting HAL on port <" + theHalPort + ">: HTTP:\\localhost:" + theHalPort );
		// ---------------------------------------------------------------------
		final RestfulHttpClient theRestClient = new HttpRestClient();
		RestRequestBuilder theRequest = theRestClient.buildGet( Scheme.HTTP, "localhost", theHalPort, "/books" );
		RestResponse theResponse = theRequest.toRestResponse();
		System.out.println( theResponse.getHttpBody() );
		System.out.println();
		theRequest = theRestClient.buildGet( Scheme.HTTP, "localhost", theHalPort, "/libraries" );
		theResponse = theRequest.toRestResponse();
		System.out.println( theResponse.getHttpBody() );
		System.out.println();
		// ---------------------------------------------------------------------
		final HalClient theHalClient = new HalClient( new Url( Scheme.HTTP, "localhost", theHalPort, "/" ) );
		String eValue;
		for ( TraversalMode eMode : TraversalMode.values() ) {
			final List<HalData> eHalDatas = theHalClient.readAll( "libraries", eMode );
			int index = 0;
			for ( HalData eHalData : eHalDatas ) {
				final File eFile = new File( Execution.toLauncherDir(), eMode + "_" + index++ + ".txt" );
				System.out.println( "Writing to file <" + eFile.getAbsolutePath() + "> ..." );
				if ( eFile.exists() ) {
					eFile.delete();
				}
				try ( FileWriter eWriter = new FileWriter( eFile ) ) {
					for ( String eKey : eHalData.sortedKeys() ) {
						System.out.println( eKey + ": " + eHalData.get( eKey ) );
						eValue = toNormalizedValue( theHalPort, eHalData.get( eKey ) );
						if ( eValue != null ) {
							eValue = "\"" + eValue + "\"";
						}
						eWriter.write( "{ \"" + eKey + "\", " + eValue + " }, " + System.lineSeparator() );
					}
					final Library theLib = eHalData.toPayload().toType( Library.class );
					System.out.println( theLib.toString() );
					System.out.println();
				}
			}
		}
		// ---------------------------------------------------------------------
		theHalServer.destroy();
	}

	@Disabled("!!! LAUNCH THE FUNCODES-SPRINBOOT APPLICATION ON PORT 8080 BEFORE TESTING !!!")
	@Test
	public void testCreateOnSpringBootService() throws MalformedURLException, HttpStatusException {
		final Integer theHalPort = 8080;
		final HalClient theHalClient = new HalClient( new Url( Scheme.HTTP, "localhost", theHalPort, "/" ) );
		final User theUser = new User( "John Romero", "John@Romero.com" );
		HalData theResult = theHalClient.create( "users", theUser );
		final User theOtherUser = theResult.toType( User.class );
		assertEquals( theUser, theOtherUser );
		final Person thePerson = createPerson( "C" );
		theResult = theHalClient.create( "persons", thePerson );
		final Person theOtherPerson = theResult.toType( Person.class );
		assertEquals( thePerson, theOtherPerson );
	}
}
