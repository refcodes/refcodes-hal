// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.hal;

import jakarta.persistence.Embeddable;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

@Embeddable
public class Location {
	@NotNull
	@Size(max = 100)
	private String addressLine1;

	@NotNull
	@Size(max = 100)
	private String addressLine2;

	@NotNull
	@Size(max = 100)
	private String city;

	@NotNull
	@Size(max = 100)
	private String state;

	@NotNull
	@Size(max = 100)
	private String country;

	@NotNull
	@Size(max = 6)
	private String zipCode;

	public Location() {

	}

	public Location( String addressLine1, String addressLine2, String city, String state, String country, String zipCode ) {
		this.addressLine1 = addressLine1;
		this.addressLine2 = addressLine2;
		this.city = city;
		this.state = state;
		this.country = country;
		this.zipCode = zipCode;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1( String addressLine1 ) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2( String addressLine2 ) {
		this.addressLine2 = addressLine2;
	}

	public String getCity() {
		return city;
	}

	public void setCity( String city ) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState( String state ) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry( String country ) {
		this.country = country;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode( String zipCode ) {
		this.zipCode = zipCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( addressLine1 == null ) ? 0 : addressLine1.hashCode() );
		result = prime * result + ( ( addressLine2 == null ) ? 0 : addressLine2.hashCode() );
		result = prime * result + ( ( city == null ) ? 0 : city.hashCode() );
		result = prime * result + ( ( country == null ) ? 0 : country.hashCode() );
		result = prime * result + ( ( state == null ) ? 0 : state.hashCode() );
		result = prime * result + ( ( zipCode == null ) ? 0 : zipCode.hashCode() );
		return result;
	}

	@Override
	public boolean equals( Object obj ) {
		if ( this == obj ) {
			return true;
		}
		if ( obj == null ) {
			return false;
		}
		if ( getClass() != obj.getClass() ) {
			return false;
		}
		final Location other = (Location) obj;
		if ( addressLine1 == null ) {
			if ( other.addressLine1 != null ) {
				return false;
			}
		}
		else if ( !addressLine1.equals( other.addressLine1 ) ) {
			return false;
		}
		if ( addressLine2 == null ) {
			if ( other.addressLine2 != null ) {
				return false;
			}
		}
		else if ( !addressLine2.equals( other.addressLine2 ) ) {
			return false;
		}
		if ( city == null ) {
			if ( other.city != null ) {
				return false;
			}
		}
		else if ( !city.equals( other.city ) ) {
			return false;
		}
		if ( country == null ) {
			if ( other.country != null ) {
				return false;
			}
		}
		else if ( !country.equals( other.country ) ) {
			return false;
		}
		if ( state == null ) {
			if ( other.state != null ) {
				return false;
			}
		}
		else if ( !state.equals( other.state ) ) {
			return false;
		}
		if ( zipCode == null ) {
			if ( other.zipCode != null ) {
				return false;
			}
		}
		else if ( !zipCode.equals( other.zipCode ) ) {
			return false;
		}
		return true;
	}
}