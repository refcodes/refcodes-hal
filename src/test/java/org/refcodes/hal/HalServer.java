// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.hal;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Simple launcher for the HAL server.
 */
@SpringBootApplication
@Configuration
@EnableJpaRepositories(basePackages = "org.refcodes.hal")
public class HalServer {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private ConfigurableApplicationContext _ctx;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	public HalServer() {}

	public HalServer( String[] args ) {
		start( args );
	}

	public HalServer( int aPort ) {
		// System.setProperty( "server.port", "" + aPort );
		// System.setProperty( "eureka.client.serviceUrl.defaultZone",
		// "http://localhost:" + aPort + "/eureka" );
		final String[] args = new String[] { "--server.port=" + aPort };
		start( args );
	}

	// /////////////////////////////////////////////////////////////////////////
	// MAIN:
	// /////////////////////////////////////////////////////////////////////////

	public static void main( String[] args ) {
		new HalServer( args );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	public void start( String[] args ) {
		System.setOut( new NullPrintStream() );
		_ctx = SpringApplication.run( HalServer.class, args );
	}

	public LibraryRepository getLibraryRepository() {
		return _ctx.getBean( LibraryRepository.class );
	}

	public BookRepository getBookRepository() {
		return _ctx.getBean( BookRepository.class );
	}

	public AuthorRepository getAuthorRepository() {
		return _ctx.getBean( AuthorRepository.class );
	}

	public AddressRepository getAddressRepository() {
		return _ctx.getBean( AddressRepository.class );
	}

	public PersonRepository getPersonRepository() {
		return _ctx.getBean( PersonRepository.class );
	}

	public UserRepository getUserRepository() {
		return _ctx.getBean( UserRepository.class );
	}

	public void destroy() {
		SpringApplication.exit( _ctx );
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	private static class NullPrintStream extends PrintStream {

		public NullPrintStream() {
			super( new NullByteArrayOutputStream() );
		}
	}

	private static class NullByteArrayOutputStream extends ByteArrayOutputStream {

		@Override
		public void write( int b ) {
			// do nothing
		}

		@Override
		public void write( byte[] b, int off, int len ) {
			// do nothing
		}

		@Override
		public void writeTo( OutputStream out ) throws IOException {
			// do nothing
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// DAEMONS:
	// /////////////////////////////////////////////////////////////////////////

}
