// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.hal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import org.junit.jupiter.api.BeforeAll;

abstract class AbstractHalClientTest {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////
	// @formatter:off
	private static final String[][][] LIBRARY_IMPORT_CHILDREN = {
		{
			{ "/address/library", null }, 
			{ "/address/location", "Location_A" }, 
			{ "/books/0/authors", null }, 
			{ "/books/0/isbn", null }, 
			{ "/books/0/library", null }, 
			{ "/books/0/title", "Book_A1" }, 
			{ "/name", "Library_A" } 
		},
		{
			{ "/address/library", null }, 
			{ "/address/location", "Location_B" }, 
			{ "/books/0/authors", null }, 
			{ "/books/0/isbn", null }, 
			{ "/books/0/library", null }, 
			{ "/books/0/title", "Book_B1" }, 
			{ "/name", "Library_B" } 
		}
	};
	// @formatter:on
	// @formatter:off
	private static final String[][][] LIBRARY_IMPORT_CHILDREN_KEEP_DANGLING_HREFS = {
		{
			{ "/address/library", null }, 
			{ "/address/library/@self", "http://localhost:PORT/addresses/1/library" }, 
			{ "/address/location", "Location_A" }, 
			{ "/books/0/authors", null }, 
			{ "/books/0/authors/@self", "http://localhost:PORT/books/1/authors" }, 
			{ "/books/0/isbn", null }, 
			{ "/books/0/library", null }, 
			{ "/books/0/library/@self", "http://localhost:PORT/books/1/library" }, 
			{ "/books/0/title", "Book_A1" }, 
			{ "/name", "Library_A" }
		},
		{
			{ "/address/library", null }, 
			{ "/address/library/@self", "http://localhost:PORT/addresses/2/library" }, 
			{ "/address/location", "Location_B" }, 
			{ "/books/0/authors", null }, 
			{ "/books/0/authors/@self", "http://localhost:PORT/books/2/authors" }, 
			{ "/books/0/isbn", null }, 
			{ "/books/0/library", null }, 
			{ "/books/0/library/@self", "http://localhost:PORT/books/2/library" }, 
			{ "/books/0/title", "Book_B1" }, 
			{ "/name", "Library_B" }
		}
	};
	// @formatter:on
	// @formatter:off
	private static final String[][][] LIBRARY_IMPORT_CHILDREN_KEEP_SELF_HREFS = {
		{
			{ "/@self", "http://localhost:PORT/libraries/1" }, 
			{ "/address/@self", "http://localhost:PORT/addresses/1" }, 
			{ "/address/library", null }, 
			{ "/address/location", "Location_A" }, 
			{ "/books/0/@self", "http://localhost:PORT/books/1" }, 
			{ "/books/0/authors", null }, 
			{ "/books/0/isbn", null }, 
			{ "/books/0/library", null }, 
			{ "/books/0/title", "Book_A1" }, 
			{ "/books/@self", "http://localhost:PORT/libraries/1/books" }, 
			{ "/name", "Library_A" }
		},
		{
			{ "/@self", "http://localhost:PORT/libraries/2" }, 
			{ "/address/@self", "http://localhost:PORT/addresses/2" }, 
			{ "/address/library", null }, 
			{ "/address/location", "Location_B" }, 
			{ "/books/0/@self", "http://localhost:PORT/books/2" }, 
			{ "/books/0/authors", null }, 
			{ "/books/0/isbn", null }, 
			{ "/books/0/library", null }, 
			{ "/books/0/title", "Book_B1" }, 
			{ "/books/@self", "http://localhost:PORT/libraries/2/books" }, 
			{ "/name", "Library_B" }
		}
	};
	// @formatter:on
	// @formatter:off
	private static final String[][][] LIBRARY_NONE = {
		{
			{ "/address", null }, 
			{ "/books", null }, 
			{ "/name", "Library_A" }
		},
		{
			{ "/address", null }, 
			{ "/books", null }, 
			{ "/name", "Library_B" }
		}
	};
	// @formatter:on
	// @formatter:off
	private static final String[][][] LIBRARY_NONE_KEEP_DANGLING_HREFS = {
		{
			{ "/address", null }, 
			{ "/address/@self", "http://localhost:PORT/libraries/1/libraryAddress" }, 
			{ "/books", null }, 
			{ "/books/@self", "http://localhost:PORT/libraries/1/books" }, 
			{ "/name", "Library_A" }
		},
		{
			{ "/address", null }, 
			{ "/address/@self", "http://localhost:PORT/libraries/2/libraryAddress" }, 
			{ "/books", null }, 
			{ "/books/@self", "http://localhost:PORT/libraries/2/books" }, 
			{ "/name", "Library_B" }
		}
	};
	// @formatter:on
	// @formatter:off
	private static final String[][][] LIBRARY_NONE_KEEP_SELF_HREFS = {
		{
			{ "/@self", "http://localhost:PORT/libraries/1" }, 
			{ "/address", null }, 
			{ "/books", null }, 
			{ "/name", "Library_A" }
		},
		{
			{ "/@self", "http://localhost:PORT/libraries/2" }, 
			{ "/address", null }, 
			{ "/books", null }, 
			{ "/name", "Library_B" }
		}
	};
	// @formatter:on
	// @formatter:off
	private static final String[][][] LIBRARY_NONE_KEEP_SELF_HREFS_KEEP_DANGLING_HREFS = {
		{
			{ "/@self", "http://localhost:PORT/libraries/1" }, 
			{ "/address", null }, 
			{ "/address/@self", "http://localhost:PORT/libraries/1/libraryAddress" }, 
			{ "/books", null }, 
			{ "/books/@self", "http://localhost:PORT/libraries/1/books" }, 
			{ "/name", "Library_A" }
		},
		{
			{ "/@self", "http://localhost:PORT/libraries/2" }, 
			{ "/address", null }, 
			{ "/address/@self", "http://localhost:PORT/libraries/2/libraryAddress" }, 
			{ "/books", null }, 
			{ "/books/@self", "http://localhost:PORT/libraries/2/books" }, 
			{ "/name", "Library_B" }
		}
	};
	// @formatter:on
	// @formatter:off
	private static final String[][][] LIBRARY_IMPORT_CHILDREN_KEEP_SELF_HREFS_KEEP_DANGLING_HREFS = {
		{
			{ "/@self", "http://localhost:PORT/libraries/1" }, 
			{ "/address/@self", "http://localhost:PORT/addresses/1" }, 
			{ "/address/library", null }, 
			{ "/address/library/@self", "http://localhost:PORT/addresses/1/library" }, 
			{ "/address/location", "Location_A" }, 
			{ "/books/0/@self", "http://localhost:PORT/books/1" }, 
			{ "/books/0/authors", null }, 
			{ "/books/0/authors/@self", "http://localhost:PORT/books/1/authors" }, 
			{ "/books/0/isbn", null }, 
			{ "/books/0/library", null }, 
			{ "/books/0/library/@self", "http://localhost:PORT/books/1/library" }, 
			{ "/books/0/title", "Book_A1" }, 
			{ "/books/@self", "http://localhost:PORT/libraries/1/books" }, 
			{ "/name", "Library_A" }
		},
		{
			{ "/@self", "http://localhost:PORT/libraries/2" }, 
			{ "/address/@self", "http://localhost:PORT/addresses/2" }, 
			{ "/address/library", null }, 
			{ "/address/library/@self", "http://localhost:PORT/addresses/2/library" }, 
			{ "/address/location", "Location_B" }, 
			{ "/books/0/@self", "http://localhost:PORT/books/2" }, 
			{ "/books/0/authors", null }, 
			{ "/books/0/authors/@self", "http://localhost:PORT/books/2/authors" }, 
			{ "/books/0/isbn", null }, 
			{ "/books/0/library", null }, 
			{ "/books/0/library/@self", "http://localhost:PORT/books/2/library" }, 
			{ "/books/0/title", "Book_B1" }, 
			{ "/books/@self", "http://localhost:PORT/libraries/2/books" }, 
			{ "/name", "Library_B" }
		}
	};
	// @formatter:on
	// @formatter:off
	private static final String[][][] LIBRARY_RECURSIVE_IMPORT_CHILDREN = {
		{
			{ "/address/library", null }, 
			{ "/address/location", "Location_A" }, 
			{ "/books/0/authors/0/books/0", null }, 
			{ "/books/0/authors/0/name", "authorA1" }, 
			{ "/books/0/authors/1/books/0", null }, 
			{ "/books/0/authors/1/name", "authorA2" }, 
			{ "/books/0/isbn", null }, 
			{ "/books/0/library", null }, 
			{ "/books/0/title", "Book_A1" }, 
			{ "/name", "Library_A" }
		},
		{
			{ "/address/library", null }, 
			{ "/address/location", "Location_B" }, 
			{ "/books/0/authors/0/books/0", null }, 
			{ "/books/0/authors/0/name", "authorB1" }, 
			{ "/books/0/authors/1/books/0", null }, 
			{ "/books/0/authors/1/name", "authorB2" }, 
			{ "/books/0/isbn", null }, 
			{ "/books/0/library", null }, 
			{ "/books/0/title", "Book_B1" }, 
			{ "/name", "Library_B" }
		}
	};
	// @formatter:on
	// @formatter:off
	private static final String[][][] LIBRARY_RECURSIVE_IMPORT_CHILDREN_KEEP_DANGLING_HREFS = {
		{
			{ "/address/library", null }, 
			{ "/address/library/@self", "http://localhost:PORT/libraries/1" }, 
			{ "/address/location", "Location_A" }, 
			{ "/books/0/authors/0/books/0", null }, 
			{ "/books/0/authors/0/books/0/@self", "http://localhost:PORT/books/1" }, 
			{ "/books/0/authors/0/name", "authorA1" }, 
			{ "/books/0/authors/1/books/0", null }, 
			{ "/books/0/authors/1/books/0/@self", "http://localhost:PORT/books/1" }, 
			{ "/books/0/authors/1/name", "authorA2" }, 
			{ "/books/0/isbn", null }, 
			{ "/books/0/library", null }, 
			{ "/books/0/library/@self", "http://localhost:PORT/libraries/1" }, 
			{ "/books/0/title", "Book_A1" }, 
			{ "/name", "Library_A" }
		},
		{
			{ "/address/library", null }, 
			{ "/address/library/@self", "http://localhost:PORT/libraries/2" }, 
			{ "/address/location", "Location_B" }, 
			{ "/books/0/authors/0/books/0", null }, 
			{ "/books/0/authors/0/books/0/@self", "http://localhost:PORT/books/2" }, 
			{ "/books/0/authors/0/name", "authorB1" }, 
			{ "/books/0/authors/1/books/0", null }, 
			{ "/books/0/authors/1/books/0/@self", "http://localhost:PORT/books/2" }, 
			{ "/books/0/authors/1/name", "authorB2" }, 
			{ "/books/0/isbn", null }, 
			{ "/books/0/library", null }, 
			{ "/books/0/library/@self", "http://localhost:PORT/libraries/2" }, 
			{ "/books/0/title", "Book_B1" }, 
			{ "/name", "Library_B" }
		}
	};
	// @formatter:on
	// @formatter:off
	private static final String[][][] LIBRARY_RECURSIVE_IMPORT_CHILDREN_KEEP_SELF_HREFS = {
		{
			{ "/@self", "http://localhost:PORT/libraries/1" }, 
			{ "/address/@self", "http://localhost:PORT/addresses/1" }, 
			{ "/address/library", null }, 
			{ "/address/location", "Location_A" }, 
			{ "/books/0/@self", "http://localhost:PORT/books/1" }, 
			{ "/books/0/authors/0/@self", "http://localhost:PORT/authors/1" }, 
			{ "/books/0/authors/0/books/0", null }, 
			{ "/books/0/authors/0/books/@self", "http://localhost:PORT/authors/1/books" }, 
			{ "/books/0/authors/0/name", "authorA1" }, 
			{ "/books/0/authors/1/@self", "http://localhost:PORT/authors/2" }, 
			{ "/books/0/authors/1/books/0", null }, 
			{ "/books/0/authors/1/books/@self", "http://localhost:PORT/authors/2/books" }, 
			{ "/books/0/authors/1/name", "authorA2" }, 
			{ "/books/0/authors/@self", "http://localhost:PORT/books/1/authors" }, 
			{ "/books/0/isbn", null }, 
			{ "/books/0/library", null }, 
			{ "/books/0/title", "Book_A1" }, 
			{ "/books/@self", "http://localhost:PORT/libraries/1/books" }, 
			{ "/name", "Library_A" }
		},
		{
			{ "/@self", "http://localhost:PORT/libraries/2" }, 
			{ "/address/@self", "http://localhost:PORT/addresses/2" }, 
			{ "/address/library", null }, 
			{ "/address/location", "Location_B" }, 
			{ "/books/0/@self", "http://localhost:PORT/books/2" }, 
			{ "/books/0/authors/0/@self", "http://localhost:PORT/authors/3" }, 
			{ "/books/0/authors/0/books/0", null }, 
			{ "/books/0/authors/0/books/@self", "http://localhost:PORT/authors/3/books" }, 
			{ "/books/0/authors/0/name", "authorB1" }, 
			{ "/books/0/authors/1/@self", "http://localhost:PORT/authors/4" }, 
			{ "/books/0/authors/1/books/0", null }, 
			{ "/books/0/authors/1/books/@self", "http://localhost:PORT/authors/4/books" }, 
			{ "/books/0/authors/1/name", "authorB2" }, 
			{ "/books/0/authors/@self", "http://localhost:PORT/books/2/authors" }, 
			{ "/books/0/isbn", null }, 
			{ "/books/0/library", null }, 
			{ "/books/0/title", "Book_B1" }, 
			{ "/books/@self", "http://localhost:PORT/libraries/2/books" }, 
			{ "/name", "Library_B" }
		}
	};
	// @formatter:on
	// @formatter:off
	private static final String[][][] LIBRARY_RECURSIVE_IMPORT_CHILDREN_KEEP_SELF_HREFS_KEEP_DANGLING_HREFS = {
		{
			{ "/@self", "http://localhost:PORT/libraries/1" }, 
			{ "/address/@self", "http://localhost:PORT/addresses/1" }, 
			{ "/address/library", null }, 
			{ "/address/library/@self", "http://localhost:PORT/libraries/1" }, 
			{ "/address/location", "Location_A" }, 
			{ "/books/0/@self", "http://localhost:PORT/books/1" }, 
			{ "/books/0/authors/0/@self", "http://localhost:PORT/authors/1" }, 
			{ "/books/0/authors/0/books/0", null }, 
			{ "/books/0/authors/0/books/0/@self", "http://localhost:PORT/books/1" }, 
			{ "/books/0/authors/0/books/@self", "http://localhost:PORT/authors/1/books" }, 
			{ "/books/0/authors/0/name", "authorA1" }, 
			{ "/books/0/authors/1/@self", "http://localhost:PORT/authors/2" }, 
			{ "/books/0/authors/1/books/0", null }, 
			{ "/books/0/authors/1/books/0/@self", "http://localhost:PORT/books/1" }, 
			{ "/books/0/authors/1/books/@self", "http://localhost:PORT/authors/2/books" }, 
			{ "/books/0/authors/1/name", "authorA2" }, 
			{ "/books/0/authors/@self", "http://localhost:PORT/books/1/authors" }, 
			{ "/books/0/isbn", null }, 
			{ "/books/0/library", null }, 
			{ "/books/0/library/@self", "http://localhost:PORT/libraries/1" }, 
			{ "/books/0/title", "Book_A1" }, 
			{ "/books/@self", "http://localhost:PORT/libraries/1/books" }, 
			{ "/name", "Library_A" }
		},
		{
			{ "/@self", "http://localhost:PORT/libraries/2" }, 
			{ "/address/@self", "http://localhost:PORT/addresses/2" }, 
			{ "/address/library", null }, 
			{ "/address/library/@self", "http://localhost:PORT/libraries/2" }, 
			{ "/address/location", "Location_B" }, 
			{ "/books/0/@self", "http://localhost:PORT/books/2" }, 
			{ "/books/0/authors/0/@self", "http://localhost:PORT/authors/3" }, 
			{ "/books/0/authors/0/books/0", null }, 
			{ "/books/0/authors/0/books/0/@self", "http://localhost:PORT/books/2" }, 
			{ "/books/0/authors/0/books/@self", "http://localhost:PORT/authors/3/books" }, 
			{ "/books/0/authors/0/name", "authorB1" }, 
			{ "/books/0/authors/1/@self", "http://localhost:PORT/authors/4" }, 
			{ "/books/0/authors/1/books/0", null }, 
			{ "/books/0/authors/1/books/0/@self", "http://localhost:PORT/books/2" }, 
			{ "/books/0/authors/1/books/@self", "http://localhost:PORT/authors/4/books" }, 
			{ "/books/0/authors/1/name", "authorB2" }, 
			{ "/books/0/authors/@self", "http://localhost:PORT/books/2/authors" }, 
			{ "/books/0/isbn", null }, 
			{ "/books/0/library", null }, 
			{ "/books/0/library/@self", "http://localhost:PORT/libraries/2" }, 
			{ "/books/0/title", "Book_B1" }, 
			{ "/books/@self", "http://localhost:PORT/libraries/2/books" }, 
			{ "/name", "Library_B" }
		}
	};
	// @formatter:on
	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	static Map<TraversalMode, String[][][]> LIBARARIES = new HashMap<>();

	@BeforeAll
	public static void beforeAll() {
		LIBARARIES.put( TraversalMode.NONE, LIBRARY_NONE );
		LIBARARIES.put( TraversalMode.NONE_KEEP_DANGLING_HREFS, LIBRARY_NONE_KEEP_DANGLING_HREFS );
		LIBARARIES.put( TraversalMode.NONE_KEEP_SELF_HREFS, LIBRARY_NONE_KEEP_SELF_HREFS );
		LIBARARIES.put( TraversalMode.NONE_KEEP_SELF_HREFS_KEEP_DANGLING_HREFS, LIBRARY_NONE_KEEP_SELF_HREFS_KEEP_DANGLING_HREFS );
		LIBARARIES.put( TraversalMode.IMPORT_CHILDREN, LIBRARY_IMPORT_CHILDREN );
		LIBARARIES.put( TraversalMode.IMPORT_CHILDREN_KEEP_DANGLING_HREFS, LIBRARY_IMPORT_CHILDREN_KEEP_DANGLING_HREFS );
		LIBARARIES.put( TraversalMode.IMPORT_CHILDREN_KEEP_SELF_HREFS, LIBRARY_IMPORT_CHILDREN_KEEP_SELF_HREFS );
		LIBARARIES.put( TraversalMode.IMPORT_CHILDREN_KEEP_SELF_HREFS_KEEP_DANGLING_HREFS, LIBRARY_IMPORT_CHILDREN_KEEP_SELF_HREFS_KEEP_DANGLING_HREFS );
		LIBARARIES.put( TraversalMode.RECURSIVE_IMPORT_CHILDREN, LIBRARY_RECURSIVE_IMPORT_CHILDREN );
		LIBARARIES.put( TraversalMode.RECURSIVE_IMPORT_CHILDREN_KEEP_DANGLING_HREFS, LIBRARY_RECURSIVE_IMPORT_CHILDREN_KEEP_DANGLING_HREFS );
		LIBARARIES.put( TraversalMode.RECURSIVE_IMPORT_CHILDREN_KEEP_SELF_HREFS, LIBRARY_RECURSIVE_IMPORT_CHILDREN_KEEP_SELF_HREFS );
		LIBARARIES.put( TraversalMode.RECURSIVE_IMPORT_CHILDREN_KEEP_SELF_HREFS_KEEP_DANGLING_HREFS, LIBRARY_RECURSIVE_IMPORT_CHILDREN_KEEP_SELF_HREFS_KEEP_DANGLING_HREFS );
	}

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	protected String toNormalizedValue( Integer aHalPort, String aValue ) {
		if ( aValue != null ) {
			aValue = aValue.replaceAll( Pattern.quote( aHalPort.toString() ), "PORT" );
		}
		return aValue;
	}

	protected void createTestFixures( HalServer aHalServer ) {
		Address addressA = new Address( "Location_A" );
		Address addressB = new Address( "Location_B" );
		addressA = aHalServer.getAddressRepository().save( addressA );
		addressB = aHalServer.getAddressRepository().save( addressB );
		Library libraryA = new Library( "Library_A" );
		Library libraryB = new Library( "Library_B" );
		libraryA.setAddress( addressA );
		libraryB.setAddress( addressB );
		libraryA = aHalServer.getLibraryRepository().save( libraryA );
		libraryB = aHalServer.getLibraryRepository().save( libraryB );
		Book bookA1 = new Book( "Book_A1" );
		Book bookB1 = new Book( "Book_B1" );
		final List<Book> booksA = new ArrayList<>( Collections.singletonList( bookA1 ) );
		final List<Book> booksB = new ArrayList<>( Collections.singletonList( bookB1 ) );
		bookA1 = aHalServer.getBookRepository().save( bookA1 );
		bookB1 = aHalServer.getBookRepository().save( bookB1 );
		Author authorA1 = new Author( "authorA1", booksA );
		Author authorA2 = new Author( "authorA2", booksA );
		Author authorB1 = new Author( "authorB1", booksB );
		Author authorB2 = new Author( "authorB2", booksB );
		authorA1 = aHalServer.getAuthorRepository().save( authorA1 );
		authorA2 = aHalServer.getAuthorRepository().save( authorA2 );
		authorB1 = aHalServer.getAuthorRepository().save( authorB1 );
		authorB2 = aHalServer.getAuthorRepository().save( authorB2 );
		final List<Author> authorsA = new ArrayList<>( Arrays.asList( authorA1, authorA2 ) );
		final List<Author> authorsB = new ArrayList<>( Arrays.asList( authorB1, authorB2 ) );
		bookA1.setAuthors( authorsA );
		bookB1.setAuthors( authorsB );
		bookA1.setLibrary( libraryA );
		bookB1.setLibrary( libraryB );
		bookA1 = aHalServer.getBookRepository().save( bookA1 );
		bookB1 = aHalServer.getBookRepository().save( bookB1 );
		final Location locationA = new Location( "addressLine1_A", "addressLine2_A", "city_A", "state_A", "country_A", "zipCode_A" );
		final Location locationB = new Location( "addressLine1_B", "addressLine2_B", "city_B", "state_B", "country_B", "zipCode_B" );
		final Name nameA = new Name( "firstName_A", "middleName_A", "lastName_A" );
		final Name nameB = new Name( "firstName_B", "middleName_B", "lastName_B" );
		Person personA = new Person( nameA, "personA@aaa.com", locationA );
		Person personB = new Person( nameB, "personB@bbb.com", locationB );
		personA = aHalServer.getPersonRepository().save( personA );
		personB = aHalServer.getPersonRepository().save( personB );
		User userA = new User( "userA", "userA@aaa.com" );
		User userB = new User( "userB", "userB@bbb.com" );
		userA = aHalServer.getUserRepository().save( userA );
		userB = aHalServer.getUserRepository().save( userB );
	}

	protected Person createPerson( String aSuffix ) {
		final Location location = new Location( "addressLine1_" + aSuffix, "addressLine2_" + aSuffix, "city_" + aSuffix, "state_" + aSuffix, "country_" + aSuffix, "zipCode_" + aSuffix );
		final Name name = new Name( "firstName_" + aSuffix, "middleName_" + aSuffix, "lastName_" + aSuffix );
		final Person person = new Person( name, "person" + aSuffix + "@aaa.com", location );
		return person;
	}
}
