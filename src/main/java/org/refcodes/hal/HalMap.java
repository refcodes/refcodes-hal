// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.hal;

import java.util.Collection;
import java.util.regex.Pattern;

import org.refcodes.data.Delimiter;
import org.refcodes.struct.CanonicalMapBuilderImpl;
import org.refcodes.struct.PathMap;
import org.refcodes.struct.Property;
import org.refcodes.struct.Relation;

/**
 * Common type for HAL related data. The {@link HalMap} extends the
 * {@link CanonicalMapBuilderImpl} class. The path delimiter as of
 * {@link #getDelimiter()} is set to be the {@link Delimiter#PATH} character.
 */
public class HalMap extends CanonicalMapBuilderImpl {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	public static final String HAL_ALPS = "alps";

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	public static final String HAL_DESCRIPTOR = "descriptor";
	public static final String HAL_DESCRIPTORS = "descriptors";
	public static final String HAL_DOC = "doc";
	public static final String HAL_EMBEDDED = "_embedded";
	public static final String HAL_ENUM = "enum";
	public static final String HAL_FORMAT = "format";
	public static final String HAL_HREF = "href";
	public static final String HAL_LINKS = "_links";
	public static final String HAL_NAME = "name";
	public static final String HAL_SELF = "self";
	public static final String[] HAL_PATH_SELF_HREF = { HAL_LINKS, HAL_SELF, HAL_HREF };
	public static final String HAL_PROFILE = "profile";
	public static final String HAL_PROPERTIES = "properties";
	public static final String HAL_READ_ONLY = "readOnly";
	public static final String HAL_REF = "$ref";
	public static final String HAL_RELATION = "rt";
	public static final String HAL_TEMPLATED = "templated";
	public static final String HAL_TITLE = "title";
	public static final String HAL_TYPE = "type";
	public static final String HAL_VALUE = "value";
	public static final String META_DATA_ARRAY = ANNOTATOR + "array";
	public static final String META_DATA_ENUM = ANNOTATOR + "enum";
	public static final String META_DATA_FORMAT = ANNOTATOR + "format";
	public static final String META_DATA_HREF = ANNOTATOR + "href";
	public static final String META_DATA_READ_ONLY = ANNOTATOR + "readOnly";
	public static final String META_DATA_SELF = ANNOTATOR + "self";
	public static final String META_DATA_TYPE = ANNOTATOR + "type";
	public static final String META_DATA_VALUE = ANNOTATOR + "value";
	public static final String ROOT_PATH = "/";
	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Create an empty {@link HalData} instance using the public path delimiter
	 * "/" ({@link Delimiter#PATH}) for the path declarations.
	 */
	public HalMap() {}

	/**
	 * Create a {@link HalData} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)} using the public path delimiter "/"
	 * ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aObj The object from which the elements are to be added.
	 */
	public HalMap( Object aObj ) {
		super( aObj );
	}

	/**
	 * Creates a {@link HalData} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)} using the public path delimiter "/"
	 * ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aObj The object from which the elements are to be added.
	 * @param aFromPath The path from where to start adding elements of the
	 *        provided object.
	 */
	public HalMap( Object aObj, String aFromPath ) {
		super( aObj, aFromPath );
	}

	/**
	 * Create a {@link HalData} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)} using the public path delimiter "/"
	 * ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aToPath The sub-path where to insert the object's introspected
	 *        values to.
	 * @param aObj The object from which the elements are to be added.
	 */
	public HalMap( String aToPath, Object aObj ) {
		super( aToPath, aObj );
	}

	/**
	 * Creates a {@link HalData} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)} using the public path delimiter "/"
	 * ({@link Delimiter#PATH} for the path declarations.
	 *
	 * @param aToPath The sub-path where to insert the object's introspected
	 *        values to.
	 * @param aObj The object from which the elements are to be added.
	 * @param aFromPath The path from where to start adding elements of the
	 *        provided object.
	 */
	public HalMap( String aToPath, Object aObj, String aFromPath ) {
		super( aToPath, aObj, aFromPath );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap getDirAt( int aIndex ) {
		return getDirAt( getRootPath(), aIndex );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap getDirAt( String aPath, int aIndex ) {
		return retrieveFrom( toPath( aPath, Integer.toString( aIndex ) ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap putDirAt( Collection<?> aPathElements, int aIndex, Object aDir ) {
		return putDirAt( toPath( aPathElements ), aIndex, aDir );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap putDirAt( Collection<?> aPathElements, int aIndex, PathMap<String> aDir ) {
		return putDirAt( toPath( aPathElements ), aIndex, aDir );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap putDirAt( int aIndex, Object aDir ) {
		return putDirAt( getRootPath(), aIndex, aDir );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap putDirAt( int aIndex, PathMap<String> aDir ) {
		return putDirAt( getRootPath(), aIndex, aDir );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap putDirAt( Object aPath, int aIndex, Object aDir ) {
		return putDirAt( toPath( aPath ), aIndex, aDir );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap putDirAt( Object aPath, int aIndex, PathMap<String> aDir ) {
		return putDirAt( toPath( aPath ), aIndex, aDir );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap putDirAt( Object[] aPathElements, int aIndex, Object aDir ) {
		return putDirAt( toPath( aPathElements ), aIndex, aDir );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap putDirAt( Object[] aPathElements, int aIndex, PathMap<String> aDir ) {
		return putDirAt( toPath( aPathElements ), aIndex, aDir );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap putDirAt( String aPath, int aIndex, Object aDir ) {
		final HalMap theReplaced = removeDirAt( aPath, aIndex );
		insertTo( toPath( aPath, aIndex ), aDir );
		return theReplaced;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap putDirAt( String aPath, int aIndex, PathMap<String> aDir ) {
		final HalMap theReplaced = removeDirAt( aPath, aIndex );
		insertTo( toPath( aPath, aIndex ), aDir );
		return theReplaced;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap putDirAt( String[] aPathElements, int aIndex, Object aDir ) {
		return putDirAt( toPath( aPathElements ), aIndex, aDir );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap putDirAt( String[] aPathElements, int aIndex, PathMap<String> aDir ) {
		return putDirAt( toPath( aPathElements ), aIndex, aDir );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap query( Collection<?> aQueryElements ) {
		return query( toPath( aQueryElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap query( Object... aQueryElements ) {
		return query( toPath( aQueryElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap query( Pattern aRegExp ) {
		return new HalMap( super.query( aRegExp ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap query( String aPathQuery ) {
		return new HalMap( super.query( aPathQuery ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap query( String... aQueryElements ) {
		return query( toPath( aQueryElements ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HalMap queryBetween( Collection<?> aFromPath, Collection<?> aPathQuery, Collection<?> aToPath ) {
		return queryBetween( toPath( aFromPath ), toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HalMap queryBetween( Object aFromPath, Object aPathQuery, Object aToPath ) {
		return queryBetween( toPath( aFromPath ), toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HalMap queryBetween( Object[] aFromPath, Object[] aPathQuery, Object[] aToPath ) {
		return queryBetween( toPath( aFromPath ), toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap queryBetween( String aFromPath, Pattern aRegExp, String aToPath ) {
		return new HalMap( super.queryBetween( aFromPath, aRegExp, aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap queryBetween( String aFromPath, String aPathQuery, String aToPath ) {
		return new HalMap( super.queryBetween( aFromPath, aPathQuery, aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HalMap queryBetween( String[] aFromPath, String[] aPathQuery, String[] aToPath ) {
		return queryBetween( toPath( aFromPath ), toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HalMap queryFrom( Collection<?> aPathQuery, Collection<?> aFromPath ) {
		return queryFrom( toPath( aPathQuery ), toPath( aFromPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HalMap queryFrom( Object aPathQuery, Object aFromPath ) {
		return queryFrom( toPath( aPathQuery ), toPath( aFromPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HalMap queryFrom( Object[] aPathQuery, Object[] aFromPath ) {
		return queryFrom( toPath( aPathQuery ), toPath( aFromPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap queryFrom( Pattern aRegExp, String aFromPath ) {
		return new HalMap( super.queryFrom( aRegExp, aFromPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap queryFrom( String aPathQuery, String aFromPath ) {
		return new HalMap( super.queryFrom( aPathQuery, aFromPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HalMap queryFrom( String[] aPathQuery, String[] aFromPath ) {
		return queryFrom( toPath( aPathQuery ), toPath( aFromPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HalMap queryTo( Collection<?> aPathQuery, String aToPath ) {
		return queryTo( toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HalMap queryTo( Object aPathQuery, String aToPath ) {
		return queryTo( toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HalMap queryTo( Object[] aPathQuery, String aToPath ) {
		return queryTo( toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap queryTo( Pattern aRegExp, String aToPath ) {
		return new HalMap( super.queryTo( aRegExp, aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap queryTo( String aPathQuery, String aToPath ) {
		return new HalMap( super.queryTo( aPathQuery, aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HalMap queryTo( String[] aPathQuery, String aToPath ) {
		return queryTo( toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap removeAll( Collection<?> aPathQueryElements ) {
		return new HalMap( super.removeAll( aPathQueryElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap removeAll( Object... aPathQueryElements ) {
		return new HalMap( super.removeAll( aPathQueryElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap removeAll( Object aPathQuery ) {
		return new HalMap( super.removeAll( aPathQuery ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap removeAll( Pattern aRegExp ) {
		return new HalMap( super.removeAll( aRegExp ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap removeAll( String... aPathQueryElements ) {
		return new HalMap( super.removeAll( aPathQueryElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap removeAll( String aPathQuery ) {
		return new HalMap( super.removeAll( aPathQuery ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap removeDirAt( int aIndex ) {
		return new HalMap( super.removeDirAt( aIndex ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap removeDirAt( Object aPath, int aIndex ) {
		return new HalMap( super.removeDirAt( aPath, aIndex ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap removeDirAt( Object[] aPathElements, int aIndex ) {
		return new HalMap( super.removeDirAt( aPathElements, aIndex ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap removeDirAt( String aPath, int aIndex ) {
		return new HalMap( super.removeDirAt( aPath, aIndex ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap removeDirAt( String[] aPathElements, int aIndex ) {
		return new HalMap( super.removeDirAt( aPathElements, aIndex ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap removeFrom( Object... aPathElements ) {
		return new HalMap( super.removeFrom( aPathElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap removeFrom( Object aPath ) {
		return new HalMap( super.removeFrom( aPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap removeFrom( String aPath ) {
		return new HalMap( super.removeFrom( aPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap removeFrom( String... aPathElements ) {
		return new HalMap( super.removeFrom( aPathElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap removePaths( Collection<?> aPaths ) {
		return new HalMap( super.removePaths( aPaths ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap removePaths( String... aPaths ) {
		return new HalMap( super.removePaths( aPaths ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HalMap retrieveBetween( Collection<?> aFromPath, Collection<?> aToPath ) {
		return retrieveBetween( toPath( aFromPath ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HalMap retrieveBetween( Object aFromPath, Object aToPath ) {
		return retrieveBetween( toPath( aFromPath ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HalMap retrieveBetween( Object[] aFromPath, Object[] aToPath ) {
		return retrieveBetween( toPath( aFromPath ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap retrieveBetween( String aFromPath, String aToPath ) {
		return new HalMap( super.retrieveBetween( aFromPath, aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HalMap retrieveBetween( String[] aFromPath, String[] aToPath ) {
		return retrieveBetween( toPath( aFromPath ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HalMap retrieveFrom( Collection<?> aPathElements ) {
		return retrieveFrom( toPath( aPathElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap retrieveFrom( Object aParentPath ) {
		return retrieveFrom( toPath( aParentPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap retrieveFrom( Object... aPathElements ) {
		return retrieveFrom( toPath( aPathElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap retrieveFrom( String aFromPath ) {
		return new HalMap( super.retrieveFrom( aFromPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap retrieveFrom( String... aPathElements ) {
		return retrieveFrom( toPath( aPathElements ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HalMap retrieveTo( Collection<?> aToPathElements ) {
		return retrieveTo( toPath( aToPathElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap retrieveTo( Object aToPath ) {
		return retrieveTo( toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap retrieveTo( Object... aToPathElements ) {
		return retrieveTo( toPath( aToPathElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap retrieveTo( String aToPath ) {
		return new HalMap( super.retrieveTo( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap retrieveTo( String... aToPathElements ) {
		return retrieveTo( toPath( aToPathElements ) );
	}

	/**
	 * Creates a vanilla plain {@link HalData} instance from this {@link HalMap}
	 * (e.g. {@link HalData} or {@link HalStruct}) excluding all Meta-Data and
	 * only including the actual payload.
	 * 
	 * @return The accordingly {@link HalData} instance.
	 */
	public CanonicalMapBuilder toPayload() {
		final CanonicalMapBuilder thePayload = new CanonicalMapBuilderImpl( this );
		thePayload.removeAll( "**", ANNOTATOR + "*" );
		thePayload.removeAll( "**", ANNOTATOR + "*", "**" );
		thePayload.removeAll( ANNOTATOR + "*", "**" );
		return thePayload;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <T> T toType( Class<T> aType ) {
		return toPayload().toType( getRootPath(), aType );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <T> T toType( String aFromPath, Class<T> aType ) {
		return toPayload().toType( aFromPath, aType );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withInsert( Object aObj ) {
		insert( aObj );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withInsert( PathMap<String> aFrom ) {
		insert( aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withInsertBetween( Collection<?> aToPathElements, Object aFrom, Collection<?> aFromPathElements ) {
		insertBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withInsertBetween( Collection<?> aToPathElements, PathMap<String> aFrom, Collection<?> aFromPathElements ) {
		insertBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withInsertBetween( Object aToPath, Object aFrom, Object aFromPath ) {
		insertBetween( aToPath, aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withInsertBetween( Object aToPath, PathMap<String> aFrom, Object aFromPath ) {
		insertBetween( aToPath, aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withInsertBetween( Object[] aToPathElements, Object aFrom, Object[] aFromPathElements ) {
		insertBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withInsertBetween( Object[] aToPathElements, PathMap<String> aFrom, Object[] aFromPathElements ) {
		insertBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withInsertBetween( String aToPath, Object aFrom, String aFromPath ) {
		insertBetween( aToPath, aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withInsertBetween( String aToPath, PathMap<String> aFrom, String aFromPath ) {
		insertBetween( aToPath, aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withInsertBetween( String[] aToPathElements, Object aFrom, String[] aFromPathElements ) {
		insertBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withInsertBetween( String[] aToPathElements, PathMap<String> aFrom, String[] aFromPathElements ) {
		insertBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withInsertFrom( Object aFrom, Collection<?> aFromPathElements ) {
		insertFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withInsertFrom( Object aFrom, Object aFromPath ) {
		insertFrom( aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withInsertFrom( Object aFrom, Object... aFromPathElements ) {
		withInsertFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withInsertFrom( Object aFrom, String aFromPath ) {
		insertFrom( aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withInsertFrom( Object aFrom, String... aFromPathElements ) {
		insertFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withInsertFrom( PathMap<String> aFrom, Collection<?> aFromPathElements ) {
		insertFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withInsertFrom( PathMap<String> aFrom, Object aFromPath ) {
		insertFrom( aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withInsertFrom( PathMap<String> aFrom, Object... aFromPathElements ) {
		withInsertFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withInsertFrom( PathMap<String> aFrom, String aFromPath ) {
		insertFrom( aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withInsertFrom( PathMap<String> aFrom, String... aFromPathElements ) {
		insertFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withInsertTo( Collection<?> aToPathElements, Object aFrom ) {
		insertTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withInsertTo( Collection<?> aToPathElements, PathMap<String> aFrom ) {
		insertTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withInsertTo( Object aToPath, Object aFrom ) {
		insertTo( aToPath, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withInsertTo( Object aToPath, PathMap<String> aFrom ) {
		insertTo( aToPath, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withInsertTo( Object[] aToPathElements, Object aFrom ) {
		insertTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withInsertTo( Object[] aToPathElements, PathMap<String> aFrom ) {
		insertTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withInsertTo( String aToPath, Object aFrom ) {
		insertTo( aToPath, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withInsertTo( String aToPath, PathMap<String> aFrom ) {
		insertTo( aToPath, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withInsertTo( String[] aToPathElements, Object aFrom ) {
		insertTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withInsertTo( String[] aToPathElements, PathMap<String> aFrom ) {
		insertTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withMerge( Object aObj ) {
		merge( aObj );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withMerge( PathMap<String> aFrom ) {
		merge( aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withMergeBetween( Collection<?> aToPathElements, Object aFrom, Collection<?> aFromPathElements ) {
		mergeBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withMergeBetween( Collection<?> aToPathElements, PathMap<String> aFrom, Collection<?> aFromPathElements ) {
		mergeBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withMergeBetween( Object aToPath, Object aFrom, Object aFromPath ) {
		mergeBetween( aToPath, aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withMergeBetween( Object aToPath, PathMap<String> aFrom, Object aFromPath ) {
		mergeBetween( aToPath, aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withMergeBetween( Object[] aToPathElements, Object aFrom, Object[] aFromPathElements ) {
		mergeBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withMergeBetween( Object[] aToPathElements, PathMap<String> aFrom, Object[] aFromPathElements ) {
		mergeBetween( aToPathElements, aFromPathElements, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withMergeBetween( String aToPath, Object aFrom, String aFromPath ) {
		mergeBetween( aToPath, aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withMergeBetween( String aToPath, PathMap<String> aFrom, String aFromPath ) {
		mergeBetween( aToPath, aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withMergeBetween( String[] aToPathElements, Object aFrom, String[] aFromPathElements ) {
		mergeBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withMergeBetween( String[] aToPathElements, PathMap<String> aFrom, String[] aFromPathElements ) {
		mergeBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withMergeFrom( Object aFrom, Collection<?> aFromPathElements ) {
		mergeFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withMergeFrom( Object aFrom, Object aFromPath ) {
		mergeFrom( aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withMergeFrom( Object aFrom, Object... aFromPathElements ) {
		mergeFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withMergeFrom( Object aFrom, String aFromPath ) {
		mergeFrom( aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withMergeFrom( Object aFrom, String... aFromPathElements ) {
		mergeFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withMergeFrom( PathMap<String> aFrom, Collection<?> aFromPathElements ) {
		mergeFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withMergeFrom( PathMap<String> aFrom, Object aFromPath ) {
		mergeFrom( aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withMergeFrom( PathMap<String> aFrom, Object... aFromPathElements ) {
		mergeFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withMergeFrom( PathMap<String> aFrom, String aFromPath ) {
		mergeFrom( aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withMergeFrom( PathMap<String> aFrom, String... aFromPathElements ) {
		mergeFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withMergeTo( Collection<?> aToPathElements, Object aFrom ) {
		mergeTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withMergeTo( Collection<?> aToPathElements, PathMap<String> aFrom ) {
		mergeTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withMergeTo( Object aToPath, Object aFrom ) {
		mergeTo( aToPath, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withMergeTo( Object aToPath, PathMap<String> aFrom ) {
		mergeTo( aToPath, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withMergeTo( Object[] aToPathElements, Object aFrom ) {
		mergeTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withMergeTo( Object[] aToPathElements, PathMap<String> aFrom ) {
		mergeTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withMergeTo( String aToPath, Object aFrom ) {
		mergeTo( aToPath, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withMergeTo( String aToPath, PathMap<String> aFrom ) {
		mergeTo( aToPath, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withMergeTo( String[] aToPathElements, Object aFrom ) {
		mergeTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withMergeTo( String[] aToPathElements, PathMap<String> aFrom ) {
		mergeTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPut( Collection<?> aPathElements, String aValue ) {
		put( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPut( Object[] aPathElements, String aValue ) {
		put( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPut( Property aProperty ) {
		put( aProperty );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPut( Relation<String, String> aProperty ) {
		put( aProperty );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPut( String aKey, String aValue ) {
		put( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPut( String[] aKey, String aValue ) {
		put( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPutBoolean( Collection<?> aPathElements, Boolean aValue ) {
		putBoolean( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPutBoolean( Object aKey, Boolean aValue ) {
		putBoolean( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPutBoolean( Object[] aPathElements, Boolean aValue ) {
		putBoolean( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPutBoolean( String aKey, Boolean aValue ) {
		putBoolean( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPutBoolean( String[] aPathElements, Boolean aValue ) {
		putBoolean( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPutByte( Collection<?> aPathElements, Byte aValue ) {
		putByte( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPutByte( Object aKey, Byte aValue ) {
		putByte( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPutByte( Object[] aPathElements, Byte aValue ) {
		putByte( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPutByte( String aKey, Byte aValue ) {
		putByte( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPutByte( String[] aPathElements, Byte aValue ) {
		putByte( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPutChar( Collection<?> aPathElements, Character aValue ) {
		putChar( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPutChar( Object aKey, Character aValue ) {
		putChar( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPutChar( Object[] aPathElements, Character aValue ) {
		putChar( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPutChar( String aKey, Character aValue ) {
		putChar( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPutChar( String[] aPathElements, Character aValue ) {
		putChar( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <C> HalMap withPutClass( Collection<?> aPathElements, Class<C> aValue ) {
		putClass( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <C> HalMap withPutClass( Object aKey, Class<C> aValue ) {
		putClass( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <C> HalMap withPutClass( Object[] aPathElements, Class<C> aValue ) {
		putClass( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <C> HalMap withPutClass( String aKey, Class<C> aValue ) {
		putClass( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <C> HalMap withPutClass( String[] aPathElements, Class<C> aValue ) {
		putClass( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPutDirAt( Collection<?> aPathElements, int aIndex, Object aDir ) {
		putDirAt( aPathElements, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPutDirAt( Collection<?> aPathElements, int aIndex, PathMap<String> aDir ) {
		putDirAt( aPathElements, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPutDirAt( int aIndex, Object aDir ) {
		putDirAt( aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPutDirAt( int aIndex, PathMap<String> aDir ) {
		putDirAt( aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPutDirAt( Object aPath, int aIndex, Object aDir ) {
		putDirAt( aPath, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPutDirAt( Object aPath, int aIndex, PathMap<String> aDir ) {
		putDirAt( aPath, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPutDirAt( Object[] aPathElements, int aIndex, Object aDir ) {
		putDirAt( aPathElements, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPutDirAt( Object[] aPathElements, int aIndex, PathMap<String> aDir ) {
		putDirAt( aPathElements, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPutDirAt( String aPath, int aIndex, Object aDir ) {
		putDirAt( aPath, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPutDirAt( String aPath, int aIndex, PathMap<String> aDir ) {
		putDirAt( aPath, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPutDirAt( String[] aPathElements, int aIndex, Object aDir ) {
		putDirAt( aPathElements, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPutDirAt( String[] aPathElements, int aIndex, PathMap<String> aDir ) {
		putDirAt( aPathElements, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPutDouble( Collection<?> aPathElements, Double aValue ) {
		putDouble( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPutDouble( Object aKey, Double aValue ) {
		putDouble( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPutDouble( Object[] aPathElements, Double aValue ) {
		putDouble( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPutDouble( String aKey, Double aValue ) {
		putDouble( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPutDouble( String[] aPathElements, Double aValue ) {
		putDouble( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <E extends Enum<E>> HalMap withPutEnum( Collection<?> aPathElements, E aValue ) {
		putEnum( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <E extends Enum<E>> HalMap withPutEnum( Object aKey, E aValue ) {
		putEnum( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <E extends Enum<E>> HalMap withPutEnum( Object[] aPathElements, E aValue ) {
		putEnum( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <E extends Enum<E>> HalMap withPutEnum( String aKey, E aValue ) {
		putEnum( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <E extends Enum<E>> HalMap withPutEnum( String[] aPathElements, E aValue ) {
		putEnum( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPutFloat( Collection<?> aPathElements, Float aValue ) {
		putFloat( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPutFloat( Object aKey, Float aValue ) {
		putFloat( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPutFloat( Object[] aPathElements, Float aValue ) {
		putFloat( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPutFloat( String aKey, Float aValue ) {
		putFloat( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPutFloat( String[] aPathElements, Float aValue ) {
		putFloat( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPutInt( Collection<?> aPathElements, Integer aValue ) {
		putInt( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPutInt( Object aKey, Integer aValue ) {
		putInt( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPutInt( Object[] aPathElements, Integer aValue ) {
		putInt( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPutInt( String aKey, Integer aValue ) {
		putInt( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPutInt( String[] aPathElements, Integer aValue ) {
		putInt( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPutLong( Collection<?> aPathElements, Long aValue ) {
		putLong( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPutLong( Object aKey, Long aValue ) {
		putLong( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPutLong( Object[] aPathElements, Long aValue ) {
		putLong( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPutLong( String aKey, Long aValue ) {
		putLong( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPutLong( String[] aPathElements, Long aValue ) {
		putLong( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPutShort( Collection<?> aPathElements, Short aValue ) {
		putShort( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPutShort( Object aKey, Short aValue ) {
		putShort( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPutShort( Object[] aPathElements, Short aValue ) {
		putShort( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPutShort( String aKey, Short aValue ) {
		putShort( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPutShort( String[] aPathElements, Short aValue ) {
		putShort( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPutString( Collection<?> aPathElements, String aValue ) {
		putString( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPutString( Object aKey, String aValue ) {
		putString( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPutString( Object[] aPathElements, String aValue ) {
		putString( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPutString( String aKey, String aValue ) {
		putString( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withPutString( String[] aPathElements, String aValue ) {
		putString( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withRemoveFrom( Collection<?> aPathElements ) {
		removeFrom( aPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withRemoveFrom( Object aPath ) {
		removeFrom( aPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withRemoveFrom( Object... aPathElements ) {
		removeFrom( aPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withRemoveFrom( String aPath ) {
		removeFrom( aPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withRemoveFrom( String... aPathElements ) {
		removeFrom( aPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalMap withRemovePaths( String... aPathElements ) {
		removeFrom( aPathElements );
		return this;
	}
}
