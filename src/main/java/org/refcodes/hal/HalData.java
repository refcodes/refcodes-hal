// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.hal;

import java.util.Collection;
import java.util.regex.Pattern;

import org.refcodes.data.Delimiter;
import org.refcodes.struct.CanonicalMap.CanonicalMapBuilder;
import org.refcodes.struct.CanonicalMapBuilderImpl;
import org.refcodes.struct.PathMap;
import org.refcodes.struct.PathMap.MutablePathMap;
import org.refcodes.struct.Property;
import org.refcodes.struct.Relation;
import org.refcodes.web.FormFields;
import org.refcodes.web.HttpStatusException;

/**
 * /** The {@link HalData} interface defines a {@link CanonicalMapBuilder}
 * https://www.metacodes.proized for working with HAL-Browser data.
 * {@link HalData} instances represent the actual payload ("rows") of a
 * HAL-Resource. In comparison, {@link HalStruct} instances represent the
 * structure including Meta-Data if a {@link HalData} instance.
 * The{@link HalData} extends the {@link CanonicalMapBuilderImpl} class. The
 * path delimiter as of {@link #getDelimiter()} is set to be the
 * {@link Delimiter#PATH} character.
 */
public class HalData extends HalMap {

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Create an empty {@link HalData} instance using the public path delimiter
	 * "/" ({@link Delimiter#PATH}) for the path declarations.
	 */
	public HalData() {}

	/**
	 * Creates a {@link HalData} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)} using the public path delimiter "/"
	 * ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aObj The object from which the elements are to be added.
	 * @param aFromPath The path from where to start adding elements of the
	 *        provided object.
	 */
	public HalData( Object aObj, String aFromPath ) {
		super( aObj, aFromPath );
	}

	/**
	 * Create a {@link HalData} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)} using the public path delimiter "/"
	 * ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aObj The object from which the elements are to be added.
	 */
	public HalData( Object aObj ) {
		super( aObj );
	}

	/**
	 * Creates a {@link HalData} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)} using the public path delimiter "/"
	 * ({@link Delimiter#PATH} for the path declarations.
	 *
	 * @param aToPath The sub-path where to insert the object's introspected
	 *        values to.
	 * @param aObj The object from which the elements are to be added.
	 * @param aFromPath The path from where to start adding elements of the
	 *        provided object.
	 */
	public HalData( String aToPath, Object aObj, String aFromPath ) {
		super( aToPath, aObj, aFromPath );
	}

	/**
	 * Create a {@link HalData} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)} using the public path delimiter "/"
	 * ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aToPath The sub-path where to insert the object's introspected
	 *        values to.
	 * @param aObj The object from which the elements are to be added.
	 */
	public HalData( String aToPath, Object aObj ) {
		super( aToPath, aObj );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Imports the data of the HAL-Resource to which value points referenced by
	 * the path.
	 * 
	 * @param aPath The path to the attribute which is to be imported. The HREF
	 *        being pointed to is resolved and the referenced data from the
	 *        HAL-Resource is inserted at the given path.
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public void importHref( String aPath ) throws HttpStatusException {
		importHref( aPath, (FormFields) null );
	}

	/**
	 * Imports the data of the HAL-Resource to which value points referenced by
	 * the path.
	 * 
	 * @param aPath The path to the attribute which is to be imported. The HREF
	 *        being pointed to is resolved and the referenced data from the
	 *        HAL-Resource is inserted at the given path.
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public void importHref( Object aPath ) throws HttpStatusException {
		importHref( aPath, (FormFields) null );
	}

	/**
	 * Imports the data of the HAL-Resource to which value points referenced by
	 * the path.
	 * 
	 * @param aPathElements The path elements representing the path to the
	 *        attribute which is to be imported. The HREF being pointed to is
	 *        resolved and the referenced data from the HAL-Resource is inserted
	 *        at the given path.
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public void importHref( Object... aPathElements ) throws HttpStatusException {
		importHref( aPathElements, (FormFields) null );
	}

	/**
	 * Imports the data of the HAL-Resource to which value points referenced by
	 * the path.
	 * 
	 * @param aPathElements The path elements representing the path to the
	 *        attribute which is to be imported. The HREF being pointed to is
	 *        resolved and the referenced data from the HAL-Resource is inserted
	 *        at the given path.
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public void importHref( String... aPathElements ) throws HttpStatusException {
		importHref( aPathElements, (FormFields) null );
	}

	/**
	 * Imports the data of the HAL-Resource to which value points referenced by
	 * the path.
	 * 
	 * @param aPathElements The path elements representing the path to the
	 *        attribute which is to be imported. The HREF being pointed to is
	 *        resolved and the referenced data from the HAL-Resource is inserted
	 *        at the given path.
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public void importHref( Collection<?> aPathElements ) throws HttpStatusException {
		importHref( aPathElements, (FormFields) null );
	}

	/**
	 * Imports the data of the HAL-Resource to which value points referenced by
	 * the path.
	 * 
	 * @param aMode The mode for importing the data.
	 * @param aPath The path to the attribute which is to be imported. The HREF
	 *        being pointed to is resolved and the referenced data from the
	 *        HAL-Resource is inserted at the given path.
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public void importHref( TraversalMode aMode, String aPath ) throws HttpStatusException {
		importHref( aMode, aPath, (FormFields) null );
	}

	/**
	 * Imports the data of the HAL-Resource to which value points referenced by
	 * the path.
	 * 
	 * @param aMode The mode for importing the data.
	 * @param aPath The path to the attribute which is to be imported. The HREF
	 *        being pointed to is resolved and the referenced data from the
	 *        HAL-Resource is inserted at the given path.
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public void importHref( TraversalMode aMode, Object aPath ) throws HttpStatusException {
		importHref( aMode, aPath, (FormFields) null );
	}

	/**
	 * Imports the data of the HAL-Resource to which value points referenced by
	 * the path.
	 * 
	 * @param aMode The mode for importing the data.
	 * @param aPathElements The path elements representing the path to the
	 *        attribute which is to be imported. The HREF being pointed to is
	 *        resolved and the referenced data from the HAL-Resource is inserted
	 *        at the given path.
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public void importHref( TraversalMode aMode, Object... aPathElements ) throws HttpStatusException {
		importHref( aMode, aPathElements, (FormFields) null );
	}

	/**
	 * Imports the data of the HAL-Resource to which value points referenced by
	 * the path.
	 * 
	 * @param aMode The mode for importing the data.
	 * @param aPathElements The path elements representing the path to the
	 *        attribute which is to be imported. The HREF being pointed to is
	 *        resolved and the referenced data from the HAL-Resource is inserted
	 *        at the given path.
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public void importHref( TraversalMode aMode, String... aPathElements ) throws HttpStatusException {
		importHref( aMode, aPathElements, (FormFields) null );
	}

	/**
	 * Imports the data of the HAL-Resource to which value points referenced by
	 * the path.
	 * 
	 * @param aMode The mode for importing the data.
	 * @param aPathElements The path elements representing the path to the
	 *        attribute which is to be imported. The HREF being pointed to is
	 *        resolved and the referenced data from the HAL-Resource is inserted
	 *        at the given path.
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public void importHref( TraversalMode aMode, Collection<?> aPathElements ) throws HttpStatusException {
		importHref( aMode, aPathElements, (FormFields) null );
	}

	/**
	 * Imports the data of the HAL-Resource to which value points referenced by
	 * the path.
	 * 
	 * @param aPath The path to the attribute which is to be imported. The HREF
	 *        being pointed to is resolved and the referenced data from the
	 *        HAL-Resource is inserted at the given path.
	 * @param aQueryFields The query fields to be appended to the requesting
	 *        URL.
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public void importHref( String aPath, FormFields aQueryFields ) throws HttpStatusException {
		importHref( TraversalMode.RECURSIVE_IMPORT_CHILDREN, aPath, aQueryFields );
	}

	/**
	 * Imports the data of the HAL-Resource to which value points referenced by
	 * the path.
	 * 
	 * @param aPath The path to the attribute which is to be imported. The HREF
	 *        being pointed to is resolved and the referenced data from the
	 *        HAL-Resource is inserted at the given path.
	 * @param aQueryFields The query fields to be appended to the requesting
	 *        URL.
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public void importHref( Object aPath, FormFields aQueryFields ) throws HttpStatusException {
		importHref( toPath( aPath ), aQueryFields );
	}

	/**
	 * Imports the data of the HAL-Resource to which value points referenced by
	 * the path.
	 * 
	 * @param aPathElements The path elements representing the path to the
	 *        attribute which is to be imported. The HREF being pointed to is
	 *        resolved and the referenced data from the HAL-Resource is inserted
	 *        at the given path.
	 * @param aQueryFields The query fields to be appended to the requesting
	 *        URL.
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public void importHref( Object[] aPathElements, FormFields aQueryFields ) throws HttpStatusException {
		importHref( toPath( aPathElements ), aQueryFields );
	}

	/**
	 * Imports the data of the HAL-Resource to which value points referenced by
	 * the path.
	 * 
	 * @param aPathElements The path elements representing the path to the
	 *        attribute which is to be imported. The HREF being pointed to is
	 *        resolved and the referenced data from the HAL-Resource is inserted
	 *        at the given path.
	 * @param aQueryFields The query fields to be appended to the requesting
	 *        URL.
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public void importHref( String[] aPathElements, FormFields aQueryFields ) throws HttpStatusException {
		importHref( toPath( aPathElements ), aQueryFields );
	}

	/**
	 * Imports the data of the HAL-Resource to which value points referenced by
	 * the path.
	 * 
	 * @param aPathElements The path elements representing the path to the
	 *        attribute which is to be imported. The HREF being pointed to is
	 *        resolved and the referenced data from the HAL-Resource is inserted
	 *        at the given path.
	 * @param aQueryFields The query fields to be appended to the requesting
	 *        URL.
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public void importHref( Collection<?> aPathElements, FormFields aQueryFields ) throws HttpStatusException {
		importHref( toPath( aPathElements ), aQueryFields );
	}

	/**
	 * Imports the data of the HAL-Resource to which value points referenced by
	 * the path.
	 * 
	 * @param aMode The mode for importing the data.
	 * @param aPath The path to the attribute which is to be imported. The HREF
	 *        being pointed to is resolved and the referenced data from the
	 *        HAL-Resource is inserted at the given path.
	 * @param aQueryFields The query fields to be appended to the requesting
	 *        URL.
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public void importHref( TraversalMode aMode, Object aPath, FormFields aQueryFields ) throws HttpStatusException {
		importHref( aMode, toPath( aPath ), aQueryFields );
	}

	/**
	 * Imports the data of the HAL-Resource to which value points referenced by
	 * the path.
	 * 
	 * @param aMode The mode for importing the data.
	 * @param aPathElements The path elements representing the path to the
	 *        attribute which is to be imported. The HREF being pointed to is
	 *        resolved and the referenced data from the HAL-Resource is inserted
	 *        at the given path.
	 * @param aQueryFields The query fields to be appended to the requesting
	 *        URL.
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public void importHref( TraversalMode aMode, Object[] aPathElements, FormFields aQueryFields ) throws HttpStatusException {
		importHref( aMode, toPath( aPathElements ), aQueryFields );
	}

	/**
	 * Imports the data of the HAL-Resource to which value points referenced by
	 * the path.
	 * 
	 * @param aMode The mode for importing the data.
	 * @param aPathElements The path elements representing the path to the
	 *        attribute which is to be imported. The HREF being pointed to is
	 *        resolved and the referenced data from the HAL-Resource is inserted
	 *        at the given path.
	 * @param aQueryFields The query fields to be appended to the requesting
	 *        URL.
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public void importHref( TraversalMode aMode, String[] aPathElements, FormFields aQueryFields ) throws HttpStatusException {
		importHref( aMode, toPath( aPathElements ), aQueryFields );
	}

	/**
	 * Imports the data of the HAL-Resource to which value points referenced by
	 * the path.
	 * 
	 * @param aMode The mode for importing the data.
	 * @param aPathElements The path elements representing the path to the
	 *        attribute which is to be imported. The HREF being pointed to is
	 *        resolved and the referenced data from the HAL-Resource is inserted
	 *        at the given path.
	 * @param aQueryFields The query fields to be appended to the requesting
	 *        URL.
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public void importHref( TraversalMode aMode, Collection<?> aPathElements, FormFields aQueryFields ) throws HttpStatusException {
		importHref( aMode, toPath( aPathElements ), aQueryFields );
	}

	/**
	 * Imports the data of the HAL-Resource to which value points referenced by
	 * the path.
	 * 
	 * @param aMode The mode for importing the data.
	 * @param aPath The path to the attribute which is to be imported. The HREF
	 *        being pointed to is resolved and the referenced data from the
	 *        HAL-Resource is inserted at the given path.
	 * @param aQueryFields The query fields to be appended to the requesting
	 *        URL.
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public void importHref( TraversalMode aMode, String aPath, FormFields aQueryFields ) throws HttpStatusException {
		throw new UnsupportedOperationException( "The implementing class does not provide any support for this functionality which required access something like a <" + HalClient.class.getSimpleName() + "> implementation." );
	}

	/**
	 * Exports the data of the HAL-Resource to which value points referenced by
	 * the path.
	 * 
	 * @param aPath The path to the attribute which is to be exported. The HREF
	 *        being pointed to is resolved and the referenced data from the
	 *        HAL-Resource is is returned.
	 * 
	 * @return The accordingly loaded data.
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public HalData exportHref( String aPath ) throws HttpStatusException {
		return exportHref( aPath, (FormFields) null );
	}

	/**
	 * Exports the data of the HAL-Resource to which value points referenced by
	 * the path.
	 * 
	 * @param aPath The path to the attribute which is to be exported. The HREF
	 *        being pointed to is resolved and the referenced data from the
	 *        HAL-Resource is is returned.
	 * 
	 * @return The accordingly loaded data.
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public HalData exportHref( Object aPath ) throws HttpStatusException {
		return exportHref( aPath, (FormFields) null );
	}

	/**
	 * Exports the data of the HAL-Resource to which value points referenced by
	 * the path.
	 * 
	 * @param aPathElements The path elements representing the path to the
	 *        attribute which is to be exported. The HREF being pointed to is
	 *        resolved and the referenced data from the HAL-Resource is inserted
	 *        at the given path.
	 * 
	 * @return The accordingly loaded data.
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public HalData exportHref( Object... aPathElements ) throws HttpStatusException {
		return exportHref( aPathElements, (FormFields) null );
	}

	/**
	 * Exports the data of the HAL-Resource to which value points referenced by
	 * the path.
	 * 
	 * @param aPathElements The path elements representing the path to the
	 *        attribute which is to be exported. The HREF being pointed to is
	 *        resolved and the referenced data from the HAL-Resource is inserted
	 *        at the given path.
	 * 
	 * @return The accordingly loaded data.
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public HalData exportHref( String... aPathElements ) throws HttpStatusException {
		return exportHref( aPathElements, (FormFields) null );
	}

	/**
	 * Exports the data of the HAL-Resource to which value points referenced by
	 * the path.
	 * 
	 * @param aPathElements The path elements representing the path to the
	 *        attribute which is to be exported. The HREF being pointed to is
	 *        resolved and the referenced data from the HAL-Resource is inserted
	 *        at the given path.
	 * 
	 * @return The accordingly loaded data.
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public HalData exportHref( Collection<?> aPathElements ) throws HttpStatusException {
		return exportHref( aPathElements, (FormFields) null );
	}

	/**
	 * Exports the data of the HAL-Resource to which value points referenced by
	 * the path.
	 * 
	 * @param aMode The mode for exporting the data.
	 * @param aPath The path to the attribute which is to be exported. The HREF
	 *        being pointed to is resolved and the referenced data from the
	 *        HAL-Resource is is returned.
	 * 
	 * @return The accordingly loaded data.
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public HalData exportHref( TraversalMode aMode, String aPath ) throws HttpStatusException {
		return exportHref( aMode, aPath, (FormFields) null );
	}

	/**
	 * Exports the data of the HAL-Resource to which value points referenced by
	 * the path.
	 * 
	 * @param aMode The mode for exporting the data.
	 * @param aPath The path to the attribute which is to be exported. The HREF
	 *        being pointed to is resolved and the referenced data from the
	 *        HAL-Resource is is returned.
	 * 
	 * @return The accordingly loaded data.
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public HalData exportHref( TraversalMode aMode, Object aPath ) throws HttpStatusException {
		return exportHref( aMode, aPath, (FormFields) null );
	}

	/**
	 * Exports the data of the HAL-Resource to which value points referenced by
	 * the path.
	 * 
	 * @param aMode The mode for exporting the data.
	 * @param aPathElements The path elements representing the path to the
	 *        attribute which is to be exported. The HREF being pointed to is
	 *        resolved and the referenced data from the HAL-Resource is is
	 *        returned.
	 * 
	 * @return The accordingly loaded data.
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public HalData exportHref( TraversalMode aMode, Object... aPathElements ) throws HttpStatusException {
		return exportHref( aMode, aPathElements, (FormFields) null );
	}

	/**
	 * Exports the data of the HAL-Resource to which value points referenced by
	 * the path.
	 * 
	 * @param aMode The mode for exporting the data.
	 * @param aPathElements The path elements representing the path to the
	 *        attribute which is to be exported. The HREF being pointed to is
	 *        resolved and the referenced data from the HAL-Resource is is
	 *        returned.
	 * 
	 * @return The accordingly loaded data.
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public HalData exportHref( TraversalMode aMode, String... aPathElements ) throws HttpStatusException {
		return exportHref( aMode, aPathElements, (FormFields) null );
	}

	/**
	 * Exports the data of the HAL-Resource to which value points referenced by
	 * the path.
	 * 
	 * @param aMode The mode for exporting the data.
	 * @param aPathElements The path elements representing the path to the
	 *        attribute which is to be exported. The HREF being pointed to is
	 *        resolved and the referenced data from the HAL-Resource is is
	 *        returned.
	 * 
	 * @return The accordingly loaded data.
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public HalData exportHref( TraversalMode aMode, Collection<?> aPathElements ) throws HttpStatusException {
		return exportHref( aMode, aPathElements, (FormFields) null );
	}

	/**
	 * Exports the data of the HAL-Resource to which value points referenced by
	 * the path.
	 * 
	 * @param aPath The path to the attribute which is to be exported. The HREF
	 *        being pointed to is resolved and the referenced data from the
	 *        HAL-Resource is is returned.
	 * @param aQueryFields The query fields to be appended to the requesting
	 *        URL.
	 * 
	 * @return The accordingly loaded data.
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public HalData exportHref( String aPath, FormFields aQueryFields ) throws HttpStatusException {
		return exportHref( TraversalMode.RECURSIVE_IMPORT_CHILDREN, aPath, aQueryFields );
	}

	/**
	 * Exports the data of the HAL-Resource to which value points referenced by
	 * the path.
	 * 
	 * @param aPath The path to the attribute which is to be exported. The HREF
	 *        being pointed to is resolved and the referenced data from the
	 *        HAL-Resource is is returned.
	 * @param aQueryFields The query fields to be appended to the requesting
	 *        URL.
	 * 
	 * @return The accordingly loaded data.
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public HalData exportHref( Object aPath, FormFields aQueryFields ) throws HttpStatusException {
		return exportHref( toPath( aPath ), aQueryFields );
	}

	/**
	 * Exports the data of the HAL-Resource to which value points referenced by
	 * the path.
	 * 
	 * @param aPathElements The path elements representing the path to the
	 *        attribute which is to be exported. The HREF being pointed to is
	 *        resolved and the referenced data from the HAL-Resource is inserted
	 *        at the given path.
	 * @param aQueryFields The query fields to be appended to the requesting
	 *        URL.
	 * 
	 * @return The accordingly loaded data.
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public HalData exportHref( Object[] aPathElements, FormFields aQueryFields ) throws HttpStatusException {
		return exportHref( toPath( aPathElements ), aQueryFields );
	}

	/**
	 * Exports the data of the HAL-Resource to which value points referenced by
	 * the path.
	 * 
	 * @param aPathElements The path elements representing the path to the
	 *        attribute which is to be exported. The HREF being pointed to is
	 *        resolved and the referenced data from the HAL-Resource is inserted
	 *        at the given path.
	 * @param aQueryFields The query fields to be appended to the requesting
	 *        URL.
	 * 
	 * @return The accordingly loaded data.
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public HalData exportHref( String[] aPathElements, FormFields aQueryFields ) throws HttpStatusException {
		return exportHref( toPath( aPathElements ), aQueryFields );
	}

	/**
	 * Exports the data of the HAL-Resource to which value points referenced by
	 * the path.
	 * 
	 * @param aPathElements The path elements representing the path to the
	 *        attribute which is to be exported. The HREF being pointed to is
	 *        resolved and the referenced data from the HAL-Resource is inserted
	 *        at the given path.
	 * @param aQueryFields The query fields to be appended to the requesting
	 *        URL.
	 * 
	 * @return The accordingly loaded data.
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public HalData exportHref( Collection<?> aPathElements, FormFields aQueryFields ) throws HttpStatusException {
		return exportHref( toPath( aPathElements ), aQueryFields );
	}

	/**
	 * Exports the data of the HAL-Resource to which value points referenced by
	 * the path.
	 * 
	 * @param aMode The mode for exporting the data.
	 * @param aPath The path to the attribute which is to be exported. The HREF
	 *        being pointed to is resolved and the referenced data from the
	 *        HAL-Resource is is returned.
	 * @param aQueryFields The query fields to be appended to the requesting
	 *        URL.
	 * 
	 * @return The accordingly loaded data.
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public HalData exportHref( TraversalMode aMode, String aPath, FormFields aQueryFields ) throws HttpStatusException {
		throw new UnsupportedOperationException( "The implementing class does not provide any support for this functionality which required access something like a <" + HalClient.class.getSimpleName() + "> implementation." );
	}

	/**
	 * Exports the data of the HAL-Resource to which value points referenced by
	 * the path.
	 * 
	 * @param aMode The mode for exporting the data.
	 * @param aPath The path to the attribute which is to be exported. The HREF
	 *        being pointed to is resolved and the referenced data from the
	 *        HAL-Resource is is returned.
	 * @param aQueryFields The query fields to be appended to the requesting
	 *        URL.
	 * 
	 * @return The accordingly loaded data.
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public HalData exportHref( TraversalMode aMode, Object aPath, FormFields aQueryFields ) throws HttpStatusException {
		return exportHref( aMode, toPath( aPath ), aQueryFields );
	}

	/**
	 * Exports the data of the HAL-Resource to which value points referenced by
	 * the path.
	 * 
	 * @param aMode The mode for exporting the data.
	 * @param aPathElements The path elements representing the path to the
	 *        attribute which is to be exported. The HREF being pointed to is
	 *        resolved and the referenced data from the HAL-Resource is is
	 *        returned.
	 * @param aQueryFields The query fields to be appended to the requesting
	 *        URL.
	 * 
	 * @return The accordingly loaded data.
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public HalData exportHref( TraversalMode aMode, Object[] aPathElements, FormFields aQueryFields ) throws HttpStatusException {
		return exportHref( aMode, toPath( aPathElements ), aQueryFields );
	}

	/**
	 * Exports the data of the HAL-Resource to which value points referenced by
	 * the path.
	 * 
	 * @param aMode The mode for exporting the data.
	 * @param aPathElements The path elements representing the path to the
	 *        attribute which is to be exported. The HREF being pointed to is
	 *        resolved and the referenced data from the HAL-Resource is is
	 *        returned.
	 * @param aQueryFields The query fields to be appended to the requesting
	 *        URL.
	 * 
	 * @return The accordingly loaded data.
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public HalData exportHref( TraversalMode aMode, String[] aPathElements, FormFields aQueryFields ) throws HttpStatusException {
		return exportHref( aMode, toPath( aPathElements ), aQueryFields );
	}

	/**
	 * Exports the data of the HAL-Resource to which value points referenced by
	 * the path.
	 * 
	 * @param aMode The mode for exporting the data.
	 * @param aPathElements The path elements representing the path to the
	 *        attribute which is to be exported. The HREF being pointed to is
	 *        resolved and the referenced data from the HAL-Resource is is
	 *        returned.
	 * @param aQueryFields The query fields to be appended to the requesting
	 *        URL.
	 * 
	 * @return The accordingly loaded data.
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public HalData exportHref( TraversalMode aMode, Collection<?> aPathElements, FormFields aQueryFields ) throws HttpStatusException {
		return exportHref( aMode, toPath( aPathElements ), aQueryFields );
	}

	// -------------------------------------------------------------------------
	// HAL:
	// -------------------------------------------------------------------------

	/**
	 * Extracts the entite's TID from the provided HTTP-Body: We assume a SELF
	 * reference and take the last number if the according HREF.
	 * 
	 * @return The TID being extracted.
	 */
	public Long getId() {
		final String theSelfHref = get( META_DATA_SELF );
		if ( theSelfHref != null && theSelfHref.length() != 0 ) {
			final int index = theSelfHref.lastIndexOf( getDelimiter() );
			if ( index != -1 ) {
				final String theId = theSelfHref.substring( index + 1 );
				if ( theId.length() != 0 ) {
					try {
						return Long.valueOf( theId );
					}
					catch ( Exception ignore ) {}
				}
			}
		}
		return null;
	}

	/**
	 * Extracts the entite's TID from the provided HTTP-Body: We assume a SELF
	 * reference and take the last number if the according HREF.
	 * 
	 * @return The TID being extracted.
	 */
	public String getSelfHref() {
		return get( META_DATA_SELF );
	}

	// -------------------------------------------------------------------------
	// INHERITANCE:
	// -------------------------------------------------------------------------

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData getDirAt( int aIndex ) {
		return getDirAt( getRootPath(), aIndex );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData getDirAt( String aPath, int aIndex ) {
		return retrieveFrom( toPath( aPath, Integer.toString( aIndex ) ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData putDirAt( Collection<?> aPathElements, int aIndex, Object aDir ) {
		return putDirAt( toPath( aPathElements ), aIndex, aDir );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData putDirAt( Collection<?> aPathElements, int aIndex, PathMap<String> aDir ) {
		return putDirAt( toPath( aPathElements ), aIndex, aDir );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData putDirAt( int aIndex, Object aDir ) {
		return putDirAt( getRootPath(), aIndex, aDir );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData putDirAt( int aIndex, PathMap<String> aDir ) {
		return putDirAt( getRootPath(), aIndex, aDir );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData putDirAt( Object aPath, int aIndex, Object aDir ) {
		return putDirAt( toPath( aPath ), aIndex, aDir );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData putDirAt( Object aPath, int aIndex, PathMap<String> aDir ) {
		return putDirAt( toPath( aPath ), aIndex, aDir );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData putDirAt( Object[] aPathElements, int aIndex, Object aDir ) {
		return putDirAt( toPath( aPathElements ), aIndex, aDir );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData putDirAt( Object[] aPathElements, int aIndex, PathMap<String> aDir ) {
		return putDirAt( toPath( aPathElements ), aIndex, aDir );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData putDirAt( String aPath, int aIndex, Object aDir ) {
		final HalData theReplaced = removeDirAt( aPath, aIndex );
		insertTo( toPath( aPath, aIndex ), aDir );
		return theReplaced;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData putDirAt( String aPath, int aIndex, PathMap<String> aDir ) {
		final HalData theReplaced = removeDirAt( aPath, aIndex );
		insertTo( toPath( aPath, aIndex ), aDir );
		return theReplaced;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData putDirAt( String[] aPathElements, int aIndex, Object aDir ) {
		return putDirAt( toPath( aPathElements ), aIndex, aDir );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData putDirAt( String[] aPathElements, int aIndex, PathMap<String> aDir ) {
		return putDirAt( toPath( aPathElements ), aIndex, aDir );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData query( Collection<?> aQueryElements ) {
		return query( toPath( aQueryElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData query( Object... aQueryElements ) {
		return query( toPath( aQueryElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData query( String aPathQuery ) {
		return new HalData( super.query( aPathQuery ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData query( Pattern aRegExp ) {
		return new HalData( super.query( aRegExp ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData query( String... aQueryElements ) {
		return query( toPath( aQueryElements ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HalData queryBetween( Collection<?> aFromPath, Collection<?> aPathQuery, Collection<?> aToPath ) {
		return queryBetween( toPath( aFromPath ), toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HalData queryBetween( Object aFromPath, Object aPathQuery, Object aToPath ) {
		return queryBetween( toPath( aFromPath ), toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HalData queryBetween( Object[] aFromPath, Object[] aPathQuery, Object[] aToPath ) {
		return queryBetween( toPath( aFromPath ), toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData queryBetween( String aFromPath, String aPathQuery, String aToPath ) {
		return new HalData( super.queryBetween( aFromPath, aPathQuery, aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData queryBetween( String aFromPath, Pattern aRegExp, String aToPath ) {
		return new HalData( super.queryBetween( aFromPath, aRegExp, aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HalData queryBetween( String[] aFromPath, String[] aPathQuery, String[] aToPath ) {
		return queryBetween( toPath( aFromPath ), toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HalData queryFrom( Collection<?> aPathQuery, Collection<?> aFromPath ) {
		return queryFrom( toPath( aPathQuery ), toPath( aFromPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HalData queryFrom( Object aPathQuery, Object aFromPath ) {
		return queryFrom( toPath( aPathQuery ), toPath( aFromPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HalData queryFrom( Object[] aPathQuery, Object[] aFromPath ) {
		return queryFrom( toPath( aPathQuery ), toPath( aFromPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData queryFrom( String aPathQuery, String aFromPath ) {
		return new HalData( super.queryFrom( aPathQuery, aFromPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData queryFrom( Pattern aRegExp, String aFromPath ) {
		return new HalData( super.queryFrom( aRegExp, aFromPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HalData queryFrom( String[] aPathQuery, String[] aFromPath ) {
		return queryFrom( toPath( aPathQuery ), toPath( aFromPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HalData queryTo( Collection<?> aPathQuery, String aToPath ) {
		return queryTo( toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HalData queryTo( Object aPathQuery, String aToPath ) {
		return queryTo( toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HalData queryTo( Object[] aPathQuery, String aToPath ) {
		return queryTo( toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData queryTo( String aPathQuery, String aToPath ) {
		return new HalData( super.queryTo( aPathQuery, aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData queryTo( Pattern aRegExp, String aToPath ) {
		return new HalData( super.queryTo( aRegExp, aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HalData queryTo( String[] aPathQuery, String aToPath ) {
		return queryTo( toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData removePaths( Collection<?> aPaths ) {
		return new HalData( super.removePaths( aPaths ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData removePaths( String... aPaths ) {
		return new HalData( super.removePaths( aPaths ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData removeAll( String... aPathQueryElements ) {
		return new HalData( super.removeAll( aPathQueryElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData removeAll( Object... aPathQueryElements ) {
		return new HalData( super.removeAll( aPathQueryElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData removeAll( Collection<?> aPathQueryElements ) {
		return new HalData( super.removeAll( aPathQueryElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData removeAll( String aPathQuery ) {
		return new HalData( super.removeAll( aPathQuery ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData removeAll( Pattern aRegExp ) {
		return new HalData( super.removeAll( aRegExp ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData removeAll( Object aPathQuery ) {
		return new HalData( super.removeAll( aPathQuery ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData removeDirAt( int aIndex ) {
		return new HalData( super.removeDirAt( aIndex ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData removeDirAt( Object aPath, int aIndex ) {
		return new HalData( super.removeDirAt( aPath, aIndex ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData removeDirAt( Object[] aPathElements, int aIndex ) {
		return new HalData( super.removeDirAt( aPathElements, aIndex ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData removeDirAt( String aPath, int aIndex ) {
		return new HalData( super.removeDirAt( aPath, aIndex ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData removeDirAt( String[] aPathElements, int aIndex ) {
		return new HalData( super.removeDirAt( aPathElements, aIndex ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData removeFrom( Object... aPathElements ) {
		return new HalData( super.removeFrom( aPathElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData removeFrom( Object aPath ) {
		return new HalData( super.removeFrom( aPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData removeFrom( String aPath ) {
		return new HalData( super.removeFrom( aPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData removeFrom( String... aPathElements ) {
		return new HalData( super.removeFrom( aPathElements ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HalData retrieveBetween( Collection<?> aFromPath, Collection<?> aToPath ) {
		return retrieveBetween( toPath( aFromPath ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HalData retrieveBetween( Object aFromPath, Object aToPath ) {
		return retrieveBetween( toPath( aFromPath ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HalData retrieveBetween( Object[] aFromPath, Object[] aToPath ) {
		return retrieveBetween( toPath( aFromPath ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HalData retrieveBetween( String[] aFromPath, String[] aToPath ) {
		return retrieveBetween( toPath( aFromPath ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HalData retrieveFrom( Collection<?> aPathElements ) {
		return retrieveFrom( toPath( aPathElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData retrieveFrom( Object aParentPath ) {
		return retrieveFrom( toPath( aParentPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData retrieveFrom( Object... aPathElements ) {
		return retrieveFrom( toPath( aPathElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData retrieveFrom( String... aPathElements ) {
		return retrieveFrom( toPath( aPathElements ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HalData retrieveTo( Collection<?> aToPathElements ) {
		return retrieveTo( toPath( aToPathElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData retrieveTo( Object aToPath ) {
		return retrieveTo( toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData retrieveTo( Object... aToPathElements ) {
		return retrieveTo( toPath( aToPathElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData retrieveTo( String... aToPathElements ) {
		return retrieveTo( toPath( aToPathElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPut( Collection<?> aPathElements, String aValue ) {
		put( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPut( Object[] aPathElements, String aValue ) {
		put( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPut( Relation<String, String> aProperty ) {
		put( aProperty );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPut( Property aProperty ) {
		put( aProperty );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPut( String[] aKey, String aValue ) {
		put( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPutBoolean( Collection<?> aPathElements, Boolean aValue ) {
		putBoolean( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPutBoolean( Object aKey, Boolean aValue ) {
		putBoolean( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPutBoolean( Object[] aPathElements, Boolean aValue ) {
		putBoolean( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPutBoolean( String aKey, Boolean aValue ) {
		putBoolean( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPutBoolean( String[] aPathElements, Boolean aValue ) {
		putBoolean( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPutByte( Collection<?> aPathElements, Byte aValue ) {
		putByte( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPutByte( Object aKey, Byte aValue ) {
		putByte( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPutByte( Object[] aPathElements, Byte aValue ) {
		putByte( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPutByte( String aKey, Byte aValue ) {
		putByte( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPutByte( String[] aPathElements, Byte aValue ) {
		putByte( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPutChar( Collection<?> aPathElements, Character aValue ) {
		putChar( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPutChar( Object aKey, Character aValue ) {
		putChar( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPutChar( Object[] aPathElements, Character aValue ) {
		putChar( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPutChar( String aKey, Character aValue ) {
		putChar( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPutChar( String[] aPathElements, Character aValue ) {
		putChar( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <C> HalData withPutClass( Collection<?> aPathElements, Class<C> aValue ) {
		putClass( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <C> HalData withPutClass( Object aKey, Class<C> aValue ) {
		putClass( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <C> HalData withPutClass( Object[] aPathElements, Class<C> aValue ) {
		putClass( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <C> HalData withPutClass( String aKey, Class<C> aValue ) {
		putClass( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <C> HalData withPutClass( String[] aPathElements, Class<C> aValue ) {
		putClass( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPutDouble( Collection<?> aPathElements, Double aValue ) {
		putDouble( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPutDouble( Object aKey, Double aValue ) {
		putDouble( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPutDouble( Object[] aPathElements, Double aValue ) {
		putDouble( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPutDouble( String aKey, Double aValue ) {
		putDouble( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPutDouble( String[] aPathElements, Double aValue ) {
		putDouble( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <E extends Enum<E>> HalData withPutEnum( Collection<?> aPathElements, E aValue ) {
		putEnum( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <E extends Enum<E>> HalData withPutEnum( Object aKey, E aValue ) {
		putEnum( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <E extends Enum<E>> HalData withPutEnum( Object[] aPathElements, E aValue ) {
		putEnum( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <E extends Enum<E>> HalData withPutEnum( String aKey, E aValue ) {
		putEnum( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <E extends Enum<E>> HalData withPutEnum( String[] aPathElements, E aValue ) {
		putEnum( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPutFloat( Collection<?> aPathElements, Float aValue ) {
		putFloat( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPutFloat( Object aKey, Float aValue ) {
		putFloat( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPutFloat( Object[] aPathElements, Float aValue ) {
		putFloat( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPutFloat( String aKey, Float aValue ) {
		putFloat( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPutFloat( String[] aPathElements, Float aValue ) {
		putFloat( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPutInt( Collection<?> aPathElements, Integer aValue ) {
		putInt( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPutInt( Object aKey, Integer aValue ) {
		putInt( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPutInt( Object[] aPathElements, Integer aValue ) {
		putInt( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPutInt( String aKey, Integer aValue ) {
		putInt( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPutInt( String[] aPathElements, Integer aValue ) {
		putInt( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPutLong( Collection<?> aPathElements, Long aValue ) {
		putLong( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPutLong( Object aKey, Long aValue ) {
		putLong( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPutLong( Object[] aPathElements, Long aValue ) {
		putLong( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPutLong( String aKey, Long aValue ) {
		putLong( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPutLong( String[] aPathElements, Long aValue ) {
		putLong( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPutShort( Collection<?> aPathElements, Short aValue ) {
		putShort( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPutShort( Object aKey, Short aValue ) {
		putShort( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPutShort( Object[] aPathElements, Short aValue ) {
		putShort( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPutShort( String aKey, Short aValue ) {
		putShort( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPutShort( String[] aPathElements, Short aValue ) {
		putShort( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPutString( Collection<?> aPathElements, String aValue ) {
		putString( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPutString( Object aKey, String aValue ) {
		putString( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPutString( Object[] aPathElements, String aValue ) {
		putString( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPutString( String aKey, String aValue ) {
		putString( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPutString( String[] aPathElements, String aValue ) {
		putString( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withInsert( Object aObj ) {
		insert( aObj );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withInsert( PathMap<String> aFrom ) {
		insert( aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withInsertBetween( Collection<?> aToPathElements, Object aFrom, Collection<?> aFromPathElements ) {
		insertBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withInsertBetween( Collection<?> aToPathElements, PathMap<String> aFrom, Collection<?> aFromPathElements ) {
		insertBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withInsertBetween( Object aToPath, Object aFrom, Object aFromPath ) {
		insertBetween( aToPath, aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withInsertBetween( Object aToPath, PathMap<String> aFrom, Object aFromPath ) {
		insertBetween( aToPath, aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withInsertBetween( Object[] aToPathElements, Object aFrom, Object[] aFromPathElements ) {
		insertBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withInsertBetween( Object[] aToPathElements, PathMap<String> aFrom, Object[] aFromPathElements ) {
		insertBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withInsertBetween( String aToPath, Object aFrom, String aFromPath ) {
		insertBetween( aToPath, aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withInsertBetween( String aToPath, PathMap<String> aFrom, String aFromPath ) {
		insertBetween( aToPath, aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withInsertBetween( String[] aToPathElements, Object aFrom, String[] aFromPathElements ) {
		insertBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withInsertBetween( String[] aToPathElements, PathMap<String> aFrom, String[] aFromPathElements ) {
		insertBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withInsertFrom( Object aFrom, Collection<?> aFromPathElements ) {
		insertFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withInsertFrom( Object aFrom, Object aFromPath ) {
		insertFrom( aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withInsertFrom( Object aFrom, Object... aFromPathElements ) {
		withInsertFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withInsertFrom( Object aFrom, String aFromPath ) {
		insertFrom( aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withInsertFrom( Object aFrom, String... aFromPathElements ) {
		insertFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withInsertFrom( PathMap<String> aFrom, Collection<?> aFromPathElements ) {
		insertFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withInsertFrom( PathMap<String> aFrom, Object aFromPath ) {
		insertFrom( aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withInsertFrom( PathMap<String> aFrom, Object... aFromPathElements ) {
		withInsertFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withInsertFrom( PathMap<String> aFrom, String aFromPath ) {
		insertFrom( aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withInsertFrom( PathMap<String> aFrom, String... aFromPathElements ) {
		insertFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withInsertTo( Collection<?> aToPathElements, Object aFrom ) {
		insertTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withInsertTo( Collection<?> aToPathElements, PathMap<String> aFrom ) {
		insertTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withInsertTo( Object aToPath, Object aFrom ) {
		insertTo( aToPath, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withInsertTo( Object aToPath, PathMap<String> aFrom ) {
		insertTo( aToPath, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withInsertTo( Object[] aToPathElements, Object aFrom ) {
		insertTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withInsertTo( Object[] aToPathElements, PathMap<String> aFrom ) {
		insertTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withInsertTo( String aToPath, Object aFrom ) {
		insertTo( aToPath, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withInsertTo( String aToPath, PathMap<String> aFrom ) {
		insertTo( aToPath, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withInsertTo( String[] aToPathElements, Object aFrom ) {
		insertTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withInsertTo( String[] aToPathElements, PathMap<String> aFrom ) {
		insertTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withMerge( Object aObj ) {
		merge( aObj );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withMerge( PathMap<String> aFrom ) {
		merge( aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withMergeBetween( Collection<?> aToPathElements, Object aFrom, Collection<?> aFromPathElements ) {
		mergeBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withMergeBetween( Collection<?> aToPathElements, PathMap<String> aFrom, Collection<?> aFromPathElements ) {
		mergeBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withMergeBetween( Object aToPath, Object aFrom, Object aFromPath ) {
		mergeBetween( aToPath, aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withMergeBetween( Object aToPath, PathMap<String> aFrom, Object aFromPath ) {
		mergeBetween( aToPath, aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withMergeBetween( Object[] aToPathElements, Object aFrom, Object[] aFromPathElements ) {
		mergeBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withMergeBetween( Object[] aToPathElements, PathMap<String> aFrom, Object[] aFromPathElements ) {
		mergeBetween( aToPathElements, aFromPathElements, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withMergeBetween( String aToPath, Object aFrom, String aFromPath ) {
		mergeBetween( aToPath, aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withMergeBetween( String aToPath, PathMap<String> aFrom, String aFromPath ) {
		mergeBetween( aToPath, aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withMergeBetween( String[] aToPathElements, Object aFrom, String[] aFromPathElements ) {
		mergeBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withMergeBetween( String[] aToPathElements, PathMap<String> aFrom, String[] aFromPathElements ) {
		mergeBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withMergeFrom( Object aFrom, Collection<?> aFromPathElements ) {
		mergeFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withMergeFrom( Object aFrom, Object aFromPath ) {
		mergeFrom( aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withMergeFrom( Object aFrom, Object... aFromPathElements ) {
		mergeFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withMergeFrom( Object aFrom, String aFromPath ) {
		mergeFrom( aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withMergeFrom( Object aFrom, String... aFromPathElements ) {
		mergeFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withMergeFrom( PathMap<String> aFrom, Collection<?> aFromPathElements ) {
		mergeFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withMergeFrom( PathMap<String> aFrom, Object aFromPath ) {
		mergeFrom( aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withMergeFrom( PathMap<String> aFrom, Object... aFromPathElements ) {
		mergeFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withMergeFrom( PathMap<String> aFrom, String aFromPath ) {
		mergeFrom( aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withMergeFrom( PathMap<String> aFrom, String... aFromPathElements ) {
		mergeFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withMergeTo( Collection<?> aToPathElements, Object aFrom ) {
		mergeTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withMergeTo( Collection<?> aToPathElements, PathMap<String> aFrom ) {
		mergeTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withMergeTo( Object aToPath, Object aFrom ) {
		mergeTo( aToPath, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withMergeTo( Object aToPath, PathMap<String> aFrom ) {
		mergeTo( aToPath, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withMergeTo( Object[] aToPathElements, Object aFrom ) {
		mergeTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withMergeTo( Object[] aToPathElements, PathMap<String> aFrom ) {
		mergeTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withMergeTo( String aToPath, Object aFrom ) {
		mergeTo( aToPath, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withMergeTo( String aToPath, PathMap<String> aFrom ) {
		mergeTo( aToPath, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withMergeTo( String[] aToPathElements, Object aFrom ) {
		mergeTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withMergeTo( String[] aToPathElements, PathMap<String> aFrom ) {
		mergeTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPutDirAt( Collection<?> aPathElements, int aIndex, Object aDir ) {
		putDirAt( aPathElements, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPutDirAt( Collection<?> aPathElements, int aIndex, PathMap<String> aDir ) {
		putDirAt( aPathElements, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPutDirAt( int aIndex, Object aDir ) {
		putDirAt( aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPutDirAt( int aIndex, PathMap<String> aDir ) {
		putDirAt( aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPutDirAt( Object aPath, int aIndex, Object aDir ) {
		putDirAt( aPath, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPutDirAt( Object aPath, int aIndex, PathMap<String> aDir ) {
		putDirAt( aPath, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPutDirAt( Object[] aPathElements, int aIndex, Object aDir ) {
		putDirAt( aPathElements, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPutDirAt( Object[] aPathElements, int aIndex, PathMap<String> aDir ) {
		putDirAt( aPathElements, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPutDirAt( String aPath, int aIndex, Object aDir ) {
		putDirAt( aPath, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPutDirAt( String aPath, int aIndex, PathMap<String> aDir ) {
		putDirAt( aPath, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPutDirAt( String[] aPathElements, int aIndex, Object aDir ) {
		putDirAt( aPathElements, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPutDirAt( String[] aPathElements, int aIndex, PathMap<String> aDir ) {
		putDirAt( aPathElements, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withRemoveFrom( Collection<?> aPathElements ) {
		removeFrom( aPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withRemoveFrom( Object aPath ) {
		removeFrom( aPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withRemoveFrom( Object... aPathElements ) {
		removeFrom( aPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withRemoveFrom( String aPath ) {
		removeFrom( aPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withRemoveFrom( String... aPathElements ) {
		removeFrom( aPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withRemovePaths( String... aPathElements ) {
		removeFrom( aPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData retrieveFrom( String aFromPath ) {
		return new HalData( super.retrieveFrom( aFromPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData retrieveTo( String aToPath ) {
		return new HalData( super.retrieveTo( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData retrieveBetween( String aFromPath, String aToPath ) {
		return new HalData( super.retrieveBetween( aFromPath, aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalData withPut( String aKey, String aValue ) {
		put( aKey, aValue );
		return this;
	}
}
