// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.hal;

import java.util.ArrayList;

import org.refcodes.web.HttpBodyMap;

/**
 * In case the a paginated result set is expected when querying the
 * {@link HalClient}, then a {@link HalDataPage} will be returned containing the
 * the {@link HalData} instances correlating to the requested page as well as
 * additional information about the number of total pages available. Paginated
 * result sets may be expected when calling methods such as
 * {@link HalClient#readPage(String, int, int)} or
 * {@link HalClient#readAll(String, org.refcodes.web.FormFields)} (or similar)
 * with HTTP-Query-Fields querying a paginated result.
 */
public class HalDataPage extends ArrayList<HalData> {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * The path to the page's number value (e.g. in an {@link HttpBodyMap}).
	 */
	public static final String PAGE_NUMBER = "/page/number";

	/**
	 * The path to the page's size value (e.g. in an {@link HttpBodyMap}).
	 */
	public static final String PAGE_SIZE = "/page/size";

	/**
	 * The path to the total page count value (e.g. in an {@link HttpBodyMap}).
	 */
	public static final String PAGE_COUNT = "/page/totalPages";

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private int _pageNumber = 0;
	private int _pageSize = -1;
	private int _pageCount = 1;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Default constructor for usage by sub-classes.
	 */
	protected HalDataPage() {}

	/**
	 * Constructs the {@link HalDataPage} instances with the given pagination
	 * metrics.
	 * 
	 * @param aPageNumber The page's number as of pagination.
	 * @param aPageSize The page's size as of pagination.
	 * @param aPageCount The total page count of the overall result set.
	 */
	public HalDataPage( int aPageNumber, int aPageSize, int aPageCount ) {
		_pageNumber = aPageNumber;
		_pageSize = aPageSize;
		_pageCount = aPageCount;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Returns the number of the page which this {@link HalDataPage} instances
	 * represents.
	 * 
	 * @return The {@link HalDataPage} result set's page number as of
	 *         pagination.
	 */
	public int getPageNumber() {
		return _pageNumber;
	}

	/**
	 * Returns the maximum size of the page being requested for the
	 * {@link HalDataPage} result set. Returns -1 in case no page size has been
	 * specified.
	 * 
	 * @return The {@link HalDataPage} result set's page size as of pagination.
	 */
	public int getPageSize() {
		return _pageSize;
	}

	/**
	 * Returns the total number of the pages which are available for retrieval.
	 * 
	 * @return The total number of the pages representing the overall result
	 *         set..
	 */
	public int getPageCount() {
		return _pageCount;
	}
}
