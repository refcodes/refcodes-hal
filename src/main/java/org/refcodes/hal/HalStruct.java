// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.hal;

import java.util.Collection;
import java.util.regex.Pattern;

import org.refcodes.data.Delimiter;
import org.refcodes.struct.CanonicalMapBuilderImpl;
import org.refcodes.struct.PathMap;
import org.refcodes.struct.Property;
import org.refcodes.struct.Relation;

/**
 * The {@link HalStruct} interface defines a {@link CanonicalMapBuilder}
 * https://www.metacodes.proized for representing the structure including
 * Meta-Data of a {@link HalStruct} instance . In comparison, {@link HalData}
 * instances represent the actual structures ("data definition") of a
 * HAL-Resource The {@link HalStruct} extends the
 * {@link CanonicalMapBuilderImpl} class. The path delimiter as of
 * {@link #getDelimiter()} is set to be the {@link Delimiter#PATH} character.
 */
public class HalStruct extends HalMap {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final long serialVersionUID = 1L;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Create an empty {@link HalStruct} instance using the public path
	 * delimiter "/" ({@link Delimiter#PATH}) for the path declarations.
	 */
	public HalStruct() {}

	/**
	 * Create a {@link HalStruct} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)} using the public path delimiter "/"
	 * ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aObj The object from which the elements are to be added.
	 */
	public HalStruct( Object aObj ) {
		super( aObj );
	}

	/**
	 * Creates a {@link HalStruct} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)} using the public path delimiter "/"
	 * ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aObj The object from which the elements are to be added.
	 * @param aFromPath The path from where to start adding elements of the
	 *        provided object.
	 */
	public HalStruct( Object aObj, String aFromPath ) {
		super( aObj, aFromPath );
	}

	/**
	 * Create a {@link HalStruct} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)} using the public path delimiter "/"
	 * ({@link Delimiter#PATH}) for the path declarations.
	 *
	 * @param aToPath The sub-path where to insert the object's introspected
	 *        values to.
	 * @param aObj The object from which the elements are to be added.
	 */
	public HalStruct( String aToPath, Object aObj ) {
		super( aToPath, aObj );
	}

	/**
	 * Creates a {@link HalStruct} instance containing the elements as of
	 * {@link MutablePathMap#insert(Object)} using the public path delimiter "/"
	 * ({@link Delimiter#PATH} for the path declarations.
	 *
	 * @param aToPath The sub-path where to insert the object's introspected
	 *        values to.
	 * @param aObj The object from which the elements are to be added.
	 * @param aFromPath The path from where to start adding elements of the
	 *        provided object.
	 */
	public HalStruct( String aToPath, Object aObj, String aFromPath ) {
		super( aToPath, aObj, aFromPath );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct getDirAt( int aIndex ) {
		return getDirAt( getRootPath(), aIndex );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct getDirAt( String aPath, int aIndex ) {
		return retrieveFrom( toPath( aPath, Integer.toString( aIndex ) ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct putDirAt( Collection<?> aPathElements, int aIndex, Object aDir ) {
		return putDirAt( toPath( aPathElements ), aIndex, aDir );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct putDirAt( Collection<?> aPathElements, int aIndex, PathMap<String> aDir ) {
		return putDirAt( toPath( aPathElements ), aIndex, aDir );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct putDirAt( int aIndex, Object aDir ) {
		return putDirAt( getRootPath(), aIndex, aDir );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct putDirAt( int aIndex, PathMap<String> aDir ) {
		return putDirAt( getRootPath(), aIndex, aDir );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct putDirAt( Object aPath, int aIndex, Object aDir ) {
		return putDirAt( toPath( aPath ), aIndex, aDir );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct putDirAt( Object aPath, int aIndex, PathMap<String> aDir ) {
		return putDirAt( toPath( aPath ), aIndex, aDir );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct putDirAt( Object[] aPathElements, int aIndex, Object aDir ) {
		return putDirAt( toPath( aPathElements ), aIndex, aDir );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct putDirAt( Object[] aPathElements, int aIndex, PathMap<String> aDir ) {
		return putDirAt( toPath( aPathElements ), aIndex, aDir );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct putDirAt( String aPath, int aIndex, Object aDir ) {
		final HalStruct theReplaced = removeDirAt( aPath, aIndex );
		insertTo( toPath( aPath, aIndex ), aDir );
		return theReplaced;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct putDirAt( String aPath, int aIndex, PathMap<String> aDir ) {
		final HalStruct theReplaced = removeDirAt( aPath, aIndex );
		insertTo( toPath( aPath, aIndex ), aDir );
		return theReplaced;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct putDirAt( String[] aPathElements, int aIndex, Object aDir ) {
		return putDirAt( toPath( aPathElements ), aIndex, aDir );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct putDirAt( String[] aPathElements, int aIndex, PathMap<String> aDir ) {
		return putDirAt( toPath( aPathElements ), aIndex, aDir );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct query( Collection<?> aQueryElements ) {
		return query( toPath( aQueryElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct query( Object... aQueryElements ) {
		return query( toPath( aQueryElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct query( Pattern aRegExp ) {
		return new HalStruct( super.query( aRegExp ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct query( String aPathQuery ) {
		return new HalStruct( super.query( aPathQuery ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct query( String... aQueryElements ) {
		return query( toPath( aQueryElements ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HalStruct queryBetween( Collection<?> aFromPath, Collection<?> aPathQuery, Collection<?> aToPath ) {
		return queryBetween( toPath( aFromPath ), toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HalStruct queryBetween( Object aFromPath, Object aPathQuery, Object aToPath ) {
		return queryBetween( toPath( aFromPath ), toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HalStruct queryBetween( Object[] aFromPath, Object[] aPathQuery, Object[] aToPath ) {
		return queryBetween( toPath( aFromPath ), toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct queryBetween( String aFromPath, Pattern aRegExp, String aToPath ) {
		return new HalStruct( super.queryBetween( aFromPath, aRegExp, aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct queryBetween( String aFromPath, String aPathQuery, String aToPath ) {
		return new HalStruct( super.queryBetween( aFromPath, aPathQuery, aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HalStruct queryBetween( String[] aFromPath, String[] aPathQuery, String[] aToPath ) {
		return queryBetween( toPath( aFromPath ), toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HalStruct queryFrom( Collection<?> aPathQuery, Collection<?> aFromPath ) {
		return queryFrom( toPath( aPathQuery ), toPath( aFromPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HalStruct queryFrom( Object aPathQuery, Object aFromPath ) {
		return queryFrom( toPath( aPathQuery ), toPath( aFromPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HalStruct queryFrom( Object[] aPathQuery, Object[] aFromPath ) {
		return queryFrom( toPath( aPathQuery ), toPath( aFromPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct queryFrom( Pattern aRegExp, String aFromPath ) {
		return new HalStruct( super.queryFrom( aRegExp, aFromPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct queryFrom( String aPathQuery, String aFromPath ) {
		return new HalStruct( super.queryFrom( aPathQuery, aFromPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HalStruct queryFrom( String[] aPathQuery, String[] aFromPath ) {
		return queryFrom( toPath( aPathQuery ), toPath( aFromPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HalStruct queryTo( Collection<?> aPathQuery, String aToPath ) {
		return queryTo( toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HalStruct queryTo( Object aPathQuery, String aToPath ) {
		return queryTo( toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HalStruct queryTo( Object[] aPathQuery, String aToPath ) {
		return queryTo( toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct queryTo( Pattern aRegExp, String aToPath ) {
		return new HalStruct( super.queryTo( aRegExp, aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct queryTo( String aPathQuery, String aToPath ) {
		return new HalStruct( super.queryTo( aPathQuery, aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HalStruct queryTo( String[] aPathQuery, String aToPath ) {
		return queryTo( toPath( aPathQuery ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct removeAll( Collection<?> aPathQueryElements ) {
		return new HalStruct( super.removeAll( aPathQueryElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct removeAll( Object... aPathQueryElements ) {
		return new HalStruct( super.removeAll( aPathQueryElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct removeAll( Object aPathQuery ) {
		return new HalStruct( super.removeAll( aPathQuery ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct removeAll( Pattern aRegExp ) {
		return new HalStruct( super.removeAll( aRegExp ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct removeAll( String... aPathQueryElements ) {
		return new HalStruct( super.removeAll( aPathQueryElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct removeAll( String aPathQuery ) {
		return new HalStruct( super.removeAll( aPathQuery ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct removeDirAt( int aIndex ) {
		return new HalStruct( super.removeDirAt( aIndex ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct removeDirAt( Object aPath, int aIndex ) {
		return new HalStruct( super.removeDirAt( aPath, aIndex ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct removeDirAt( Object[] aPathElements, int aIndex ) {
		return new HalStruct( super.removeDirAt( aPathElements, aIndex ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct removeDirAt( String aPath, int aIndex ) {
		return new HalStruct( super.removeDirAt( aPath, aIndex ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct removeDirAt( String[] aPathElements, int aIndex ) {
		return new HalStruct( super.removeDirAt( aPathElements, aIndex ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct removeFrom( Object... aPathElements ) {
		return new HalStruct( super.removeFrom( aPathElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct removeFrom( Object aPath ) {
		return new HalStruct( super.removeFrom( aPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct removeFrom( String aPath ) {
		return new HalStruct( super.removeFrom( aPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct removeFrom( String... aPathElements ) {
		return new HalStruct( super.removeFrom( aPathElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct removePaths( Collection<?> aPaths ) {
		return new HalStruct( super.removePaths( aPaths ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct removePaths( String... aPaths ) {
		return new HalStruct( super.removePaths( aPaths ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HalStruct retrieveBetween( Collection<?> aFromPath, Collection<?> aToPath ) {
		return retrieveBetween( toPath( aFromPath ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HalStruct retrieveBetween( Object aFromPath, Object aToPath ) {
		return retrieveBetween( toPath( aFromPath ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HalStruct retrieveBetween( Object[] aFromPath, Object[] aToPath ) {
		return retrieveBetween( toPath( aFromPath ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct retrieveBetween( String aFromPath, String aToPath ) {
		return new HalStruct( super.retrieveBetween( aFromPath, aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HalStruct retrieveBetween( String[] aFromPath, String[] aToPath ) {
		return retrieveBetween( toPath( aFromPath ), toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HalStruct retrieveFrom( Collection<?> aPathElements ) {
		return retrieveFrom( toPath( aPathElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct retrieveFrom( Object aParentPath ) {
		return retrieveFrom( toPath( aParentPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct retrieveFrom( Object... aPathElements ) {
		return retrieveFrom( toPath( aPathElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct retrieveFrom( String... aPathElements ) {
		return retrieveFrom( toPath( aPathElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct retrieveFrom( String aFromPath ) {
		return new HalStruct( super.retrieveFrom( aFromPath ) );
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public HalStruct retrieveTo( Collection<?> aToPathElements ) {
		return retrieveTo( toPath( aToPathElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct retrieveTo( Object aToPath ) {
		return retrieveTo( toPath( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct retrieveTo( Object... aToPathElements ) {
		return retrieveTo( toPath( aToPathElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct retrieveTo( String... aToPathElements ) {
		return retrieveTo( toPath( aToPathElements ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct retrieveTo( String aToPath ) {
		return new HalStruct( super.retrieveTo( aToPath ) );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withInsert( Object aObj ) {
		insert( aObj );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withInsert( PathMap<String> aFrom ) {
		insert( aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withInsertBetween( Collection<?> aToPathElements, Object aFrom, Collection<?> aFromPathElements ) {
		insertBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withInsertBetween( Collection<?> aToPathElements, PathMap<String> aFrom, Collection<?> aFromPathElements ) {
		insertBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withInsertBetween( Object aToPath, Object aFrom, Object aFromPath ) {
		insertBetween( aToPath, aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withInsertBetween( Object aToPath, PathMap<String> aFrom, Object aFromPath ) {
		insertBetween( aToPath, aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withInsertBetween( Object[] aToPathElements, Object aFrom, Object[] aFromPathElements ) {
		insertBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withInsertBetween( Object[] aToPathElements, PathMap<String> aFrom, Object[] aFromPathElements ) {
		insertBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withInsertBetween( String aToPath, Object aFrom, String aFromPath ) {
		insertBetween( aToPath, aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withInsertBetween( String aToPath, PathMap<String> aFrom, String aFromPath ) {
		insertBetween( aToPath, aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withInsertBetween( String[] aToPathElements, Object aFrom, String[] aFromPathElements ) {
		insertBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withInsertBetween( String[] aToPathElements, PathMap<String> aFrom, String[] aFromPathElements ) {
		insertBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withInsertFrom( Object aFrom, Collection<?> aFromPathElements ) {
		insertFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withInsertFrom( Object aFrom, Object aFromPath ) {
		insertFrom( aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withInsertFrom( Object aFrom, Object... aFromPathElements ) {
		withInsertFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withInsertFrom( Object aFrom, String aFromPath ) {
		insertFrom( aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withInsertFrom( Object aFrom, String... aFromPathElements ) {
		insertFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withInsertFrom( PathMap<String> aFrom, Collection<?> aFromPathElements ) {
		insertFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withInsertFrom( PathMap<String> aFrom, Object aFromPath ) {
		insertFrom( aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withInsertFrom( PathMap<String> aFrom, Object... aFromPathElements ) {
		withInsertFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withInsertFrom( PathMap<String> aFrom, String aFromPath ) {
		insertFrom( aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withInsertFrom( PathMap<String> aFrom, String... aFromPathElements ) {
		insertFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withInsertTo( Collection<?> aToPathElements, Object aFrom ) {
		insertTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withInsertTo( Collection<?> aToPathElements, PathMap<String> aFrom ) {
		insertTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withInsertTo( Object aToPath, Object aFrom ) {
		insertTo( aToPath, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withInsertTo( Object aToPath, PathMap<String> aFrom ) {
		insertTo( aToPath, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withInsertTo( Object[] aToPathElements, Object aFrom ) {
		insertTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withInsertTo( Object[] aToPathElements, PathMap<String> aFrom ) {
		insertTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withInsertTo( String aToPath, Object aFrom ) {
		insertTo( aToPath, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withInsertTo( String aToPath, PathMap<String> aFrom ) {
		insertTo( aToPath, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withInsertTo( String[] aToPathElements, Object aFrom ) {
		insertTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withInsertTo( String[] aToPathElements, PathMap<String> aFrom ) {
		insertTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withMerge( Object aObj ) {
		merge( aObj );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withMerge( PathMap<String> aFrom ) {
		merge( aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withMergeBetween( Collection<?> aToPathElements, Object aFrom, Collection<?> aFromPathElements ) {
		mergeBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withMergeBetween( Collection<?> aToPathElements, PathMap<String> aFrom, Collection<?> aFromPathElements ) {
		mergeBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withMergeBetween( Object aToPath, Object aFrom, Object aFromPath ) {
		mergeBetween( aToPath, aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withMergeBetween( Object aToPath, PathMap<String> aFrom, Object aFromPath ) {
		mergeBetween( aToPath, aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withMergeBetween( Object[] aToPathElements, Object aFrom, Object[] aFromPathElements ) {
		mergeBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withMergeBetween( Object[] aToPathElements, PathMap<String> aFrom, Object[] aFromPathElements ) {
		mergeBetween( aToPathElements, aFromPathElements, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withMergeBetween( String aToPath, Object aFrom, String aFromPath ) {
		mergeBetween( aToPath, aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withMergeBetween( String aToPath, PathMap<String> aFrom, String aFromPath ) {
		mergeBetween( aToPath, aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withMergeBetween( String[] aToPathElements, Object aFrom, String[] aFromPathElements ) {
		mergeBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withMergeBetween( String[] aToPathElements, PathMap<String> aFrom, String[] aFromPathElements ) {
		mergeBetween( aToPathElements, aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withMergeFrom( Object aFrom, Collection<?> aFromPathElements ) {
		mergeFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withMergeFrom( Object aFrom, Object aFromPath ) {
		mergeFrom( aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withMergeFrom( Object aFrom, Object... aFromPathElements ) {
		mergeFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withMergeFrom( Object aFrom, String aFromPath ) {
		mergeFrom( aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withMergeFrom( Object aFrom, String... aFromPathElements ) {
		mergeFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withMergeFrom( PathMap<String> aFrom, Collection<?> aFromPathElements ) {
		mergeFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withMergeFrom( PathMap<String> aFrom, Object aFromPath ) {
		mergeFrom( aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withMergeFrom( PathMap<String> aFrom, Object... aFromPathElements ) {
		mergeFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withMergeFrom( PathMap<String> aFrom, String aFromPath ) {
		mergeFrom( aFrom, aFromPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withMergeFrom( PathMap<String> aFrom, String... aFromPathElements ) {
		mergeFrom( aFrom, aFromPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withMergeTo( Collection<?> aToPathElements, Object aFrom ) {
		mergeTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withMergeTo( Collection<?> aToPathElements, PathMap<String> aFrom ) {
		mergeTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withMergeTo( Object aToPath, Object aFrom ) {
		mergeTo( aToPath, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withMergeTo( Object aToPath, PathMap<String> aFrom ) {
		mergeTo( aToPath, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withMergeTo( Object[] aToPathElements, Object aFrom ) {
		mergeTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withMergeTo( Object[] aToPathElements, PathMap<String> aFrom ) {
		mergeTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withMergeTo( String aToPath, Object aFrom ) {
		mergeTo( aToPath, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withMergeTo( String aToPath, PathMap<String> aFrom ) {
		mergeTo( aToPath, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withMergeTo( String[] aToPathElements, Object aFrom ) {
		mergeTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withMergeTo( String[] aToPathElements, PathMap<String> aFrom ) {
		mergeTo( aToPathElements, aFrom );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPut( Collection<?> aPathElements, String aValue ) {
		put( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPut( Object[] aPathElements, String aValue ) {
		put( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPut( Property aProperty ) {
		put( aProperty );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPut( Relation<String, String> aProperty ) {
		put( aProperty );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPut( String aKey, String aValue ) {
		put( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPut( String[] aKey, String aValue ) {
		put( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPutBoolean( Collection<?> aPathElements, Boolean aValue ) {
		putBoolean( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPutBoolean( Object aKey, Boolean aValue ) {
		putBoolean( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPutBoolean( Object[] aPathElements, Boolean aValue ) {
		putBoolean( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPutBoolean( String aKey, Boolean aValue ) {
		putBoolean( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPutBoolean( String[] aPathElements, Boolean aValue ) {
		putBoolean( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPutByte( Collection<?> aPathElements, Byte aValue ) {
		putByte( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPutByte( Object aKey, Byte aValue ) {
		putByte( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPutByte( Object[] aPathElements, Byte aValue ) {
		putByte( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPutByte( String aKey, Byte aValue ) {
		putByte( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPutByte( String[] aPathElements, Byte aValue ) {
		putByte( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPutChar( Collection<?> aPathElements, Character aValue ) {
		putChar( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPutChar( Object aKey, Character aValue ) {
		putChar( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPutChar( Object[] aPathElements, Character aValue ) {
		putChar( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPutChar( String aKey, Character aValue ) {
		putChar( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPutChar( String[] aPathElements, Character aValue ) {
		putChar( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <C> HalStruct withPutClass( Collection<?> aPathElements, Class<C> aValue ) {
		putClass( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <C> HalStruct withPutClass( Object aKey, Class<C> aValue ) {
		putClass( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <C> HalStruct withPutClass( Object[] aPathElements, Class<C> aValue ) {
		putClass( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <C> HalStruct withPutClass( String aKey, Class<C> aValue ) {
		putClass( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <C> HalStruct withPutClass( String[] aPathElements, Class<C> aValue ) {
		putClass( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPutDirAt( Collection<?> aPathElements, int aIndex, Object aDir ) {
		putDirAt( aPathElements, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPutDirAt( Collection<?> aPathElements, int aIndex, PathMap<String> aDir ) {
		putDirAt( aPathElements, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPutDirAt( int aIndex, Object aDir ) {
		putDirAt( aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPutDirAt( int aIndex, PathMap<String> aDir ) {
		putDirAt( aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPutDirAt( Object aPath, int aIndex, Object aDir ) {
		putDirAt( aPath, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPutDirAt( Object aPath, int aIndex, PathMap<String> aDir ) {
		putDirAt( aPath, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPutDirAt( Object[] aPathElements, int aIndex, Object aDir ) {
		putDirAt( aPathElements, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPutDirAt( Object[] aPathElements, int aIndex, PathMap<String> aDir ) {
		putDirAt( aPathElements, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPutDirAt( String aPath, int aIndex, Object aDir ) {
		putDirAt( aPath, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPutDirAt( String aPath, int aIndex, PathMap<String> aDir ) {
		putDirAt( aPath, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPutDirAt( String[] aPathElements, int aIndex, Object aDir ) {
		putDirAt( aPathElements, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPutDirAt( String[] aPathElements, int aIndex, PathMap<String> aDir ) {
		putDirAt( aPathElements, aIndex, aDir );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPutDouble( Collection<?> aPathElements, Double aValue ) {
		putDouble( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPutDouble( Object aKey, Double aValue ) {
		putDouble( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPutDouble( Object[] aPathElements, Double aValue ) {
		putDouble( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPutDouble( String aKey, Double aValue ) {
		putDouble( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPutDouble( String[] aPathElements, Double aValue ) {
		putDouble( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <E extends Enum<E>> HalStruct withPutEnum( Collection<?> aPathElements, E aValue ) {
		putEnum( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <E extends Enum<E>> HalStruct withPutEnum( Object aKey, E aValue ) {
		putEnum( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <E extends Enum<E>> HalStruct withPutEnum( Object[] aPathElements, E aValue ) {
		putEnum( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <E extends Enum<E>> HalStruct withPutEnum( String aKey, E aValue ) {
		putEnum( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public <E extends Enum<E>> HalStruct withPutEnum( String[] aPathElements, E aValue ) {
		putEnum( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPutFloat( Collection<?> aPathElements, Float aValue ) {
		putFloat( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPutFloat( Object aKey, Float aValue ) {
		putFloat( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPutFloat( Object[] aPathElements, Float aValue ) {
		putFloat( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPutFloat( String aKey, Float aValue ) {
		putFloat( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPutFloat( String[] aPathElements, Float aValue ) {
		putFloat( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPutInt( Collection<?> aPathElements, Integer aValue ) {
		putInt( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPutInt( Object aKey, Integer aValue ) {
		putInt( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPutInt( Object[] aPathElements, Integer aValue ) {
		putInt( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPutInt( String aKey, Integer aValue ) {
		putInt( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPutInt( String[] aPathElements, Integer aValue ) {
		putInt( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPutLong( Collection<?> aPathElements, Long aValue ) {
		putLong( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPutLong( Object aKey, Long aValue ) {
		putLong( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPutLong( Object[] aPathElements, Long aValue ) {
		putLong( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPutLong( String aKey, Long aValue ) {
		putLong( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPutLong( String[] aPathElements, Long aValue ) {
		putLong( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPutShort( Collection<?> aPathElements, Short aValue ) {
		putShort( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPutShort( Object aKey, Short aValue ) {
		putShort( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPutShort( Object[] aPathElements, Short aValue ) {
		putShort( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPutShort( String aKey, Short aValue ) {
		putShort( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPutShort( String[] aPathElements, Short aValue ) {
		putShort( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPutString( Collection<?> aPathElements, String aValue ) {
		putString( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPutString( Object aKey, String aValue ) {
		putString( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPutString( Object[] aPathElements, String aValue ) {
		putString( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPutString( String aKey, String aValue ) {
		putString( aKey, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withPutString( String[] aPathElements, String aValue ) {
		putString( aPathElements, aValue );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withRemoveFrom( Collection<?> aPathElements ) {
		removeFrom( aPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withRemoveFrom( Object aPath ) {
		removeFrom( aPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withRemoveFrom( Object... aPathElements ) {
		removeFrom( aPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withRemoveFrom( String aPath ) {
		removeFrom( aPath );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withRemoveFrom( String... aPathElements ) {
		removeFrom( aPathElements );
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalStruct withRemovePaths( String... aPathElements ) {
		removeFrom( aPathElements );
		return this;
	}
}
