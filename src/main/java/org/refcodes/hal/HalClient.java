// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.hal;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.refcodes.data.Delimiter;
import org.refcodes.data.IoRetryCount;
import org.refcodes.data.IoSleepLoopTime;
import org.refcodes.rest.HttpRestClient;
import org.refcodes.rest.HttpRestServer;
import org.refcodes.rest.OauthTokenHandler;
import org.refcodes.rest.RestRequest;
import org.refcodes.rest.RestRequestBuilder;
import org.refcodes.rest.RestResponse;
import org.refcodes.rest.RestfulHttpClient;
import org.refcodes.rest.RestfulHttpServer;
import org.refcodes.struct.CanonicalMap;
import org.refcodes.struct.CanonicalMap.CanonicalMapBuilder;
import org.refcodes.struct.CanonicalMapBuilderImpl;
import org.refcodes.struct.PathMap;
import org.refcodes.web.BadInvocationException;
import org.refcodes.web.BadResponseException;
import org.refcodes.web.FormFields;
import org.refcodes.web.HttpBodyMap;
import org.refcodes.web.HttpResponseException;
import org.refcodes.web.HttpStatusCode;
import org.refcodes.web.HttpStatusException;
import org.refcodes.web.InternalClientErrorException;
import org.refcodes.web.MediaType;
import org.refcodes.web.OauthToken;
import org.refcodes.web.OauthTokenAccessor.OauthTokenBuilder;
import org.refcodes.web.OauthTokenAccessor.OauthTokenProperty;
import org.refcodes.web.Url;

/**
 * The {@link HalClient} lets you comfortably introspect and manage (in terms of
 * CRUD) HAL resources. You may use it reflect the data structures of entities
 * accessible by remote HAL endpoints as well as apply CRUD operations on the
 * introspected entities. This means that you interact completely dynamically
 * with the HAL resource at runtime instead of having to provide static data
 * structures at compile time. Still you may go wit static data types as we use
 * the {@link CanonicalMap} (as well as the {@link CanonicalMapBuilder}) to
 * operate upon the dynamic data structures. Implementations may use the
 * {@link RestfulHttpServer} and it's implementations such as the
 * {@link HttpRestServer}. See also
 * "https://en.wikipedia.org/wiki/Hypertext_Application_Language".
 */
public class HalClient implements OauthTokenProperty, OauthTokenBuilder<HalClient> {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final Logger LOGGER = Logger.getLogger( HalClient.class.getName() );

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final boolean IS_RELATION_ALWAYS_AS_ARRRAY = true;
	private static final String ARRAY_IDENTIFIER = "array";
	private static final int DEFAULT_REDIRECT_DEPTH = -1;
	private static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd";
	private static final String DEFAULT_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";
	private static final String DEFAULT_TIME_FORMAT = "HH:mm:ss";
	private static final String DATE = "date";
	private static final String DATE_TIME = "date-time";
	private static final String TIME = "time";
	private static final String PREFIX_REFERENCE = "#";
	private static final String URI_IDENTIFIER = "uri";
	private static final String[] RESERVED_ENTITES = { HalMap.HAL_SELF };

	// protected static String toAttributePath( String path ) {
	//	int index = path.indexOf( HalMap.META_DATA );
	//	if ( index != -1 ) {
	//		path = path.substring( 0, index );
	//	}
	//	return path;
	// }

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private String[] _excludeEntities;
	private String _halUrl;
	private String[] _includeEntities;
	private final RestfulHttpClient _restClient = new HttpRestClient();
	private String _profileLink;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates the {@link HalClient} with no authorization credentials
	 * passed.
	 * 
	 * @param aHalUrl The URL pointing to the HAL-Endpoint.
	 * 
	 * @throws MalformedURLException Thrown to indicate that a malformed OAuth
	 *         URL has occurred.
	 */
	public HalClient( String aHalUrl ) throws MalformedURLException {
		_halUrl = toValidUrl( aHalUrl );
	}

	/**
	 * Instantiates the {@link HalClient} with a valid {@link OauthToken}
	 * passed.
	 * 
	 * @param aHalUrl The URL pointing to the HAL-Endpoint.
	 * @param aOauthToken The OAuth-Token to be used when authorizing against
	 *        the HAL-Endpoint.
	 * 
	 * @throws MalformedURLException Thrown to indicate that a malformed OAuth
	 *         URL has occurred.
	 */
	public HalClient( String aHalUrl, OauthToken aOauthToken ) throws MalformedURLException {
		_halUrl = toValidUrl( aHalUrl );
		_restClient.setOauthToken( aOauthToken );
	}

	/**
	 * Instantiates the {@link HalClient} with no authorization credentials
	 * passed.
	 * 
	 * @param aHalUrl The URL pointing to the HAL-Endpoint.
	 * @param aOauthUrl The URL pointing to the OAuth authentication endpoint.
	 * @param aOauthClientId The assigned OAuth client TID.
	 * @param aOauthClientSecret The client's OAuth secret.
	 * @param aOauthUserName The OAuth authorization user name.
	 * @param aOauthUserSecret The OAuth authorization user secret.
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status when requesting the OAuth URL.
	 * @throws MalformedURLException Thrown to indicate that a malformed OAuth
	 *         URL has occurred.
	 */
	public HalClient( String aHalUrl, String aOauthUrl, String aOauthClientId, String aOauthClientSecret, String aOauthUserName, String aOauthUserSecret ) throws HttpStatusException, MalformedURLException {
		_halUrl = toValidUrl( aHalUrl );
		final OauthToken theOauthToken = toOauthTokenHandler( aOauthUrl, aOauthClientId, aOauthClientSecret, aOauthUserName, aOauthUserSecret );
		_restClient.setOauthToken( theOauthToken );
	}

	/**
	 * Instantiates the {@link HalClient} with no authorization credentials
	 * passed.
	 * 
	 * @param aHalUrl The URL pointing to the HAL-Endpoint.
	 * 
	 * @throws MalformedURLException Thrown to indicate that a malformed OAuth
	 *         URL has occurred.
	 */
	public HalClient( Url aHalUrl ) throws MalformedURLException {
		_halUrl = toValidUrl( aHalUrl.toHttpUrl() );
	}

	/**
	 * Instantiates the {@link HalClient} with a valid {@link OauthToken}
	 * passed.
	 * 
	 * @param aHalUrl The URL pointing to the HAL-Endpoint.
	 * @param aOauthToken The OAuth-Token to be used when authorizing against
	 *        the HAL-Endpoint.
	 * 
	 * @throws MalformedURLException Thrown to indicate that a malformed OAuth
	 *         URL has occurred.
	 */
	public HalClient( Url aHalUrl, OauthToken aOauthToken ) throws MalformedURLException {
		_halUrl = toValidUrl( aHalUrl.toHttpUrl() );
		_restClient.setOauthToken( aOauthToken );
	}

	/**
	 * Instantiates the {@link HalClient} with no authorization credentials
	 * passed.
	 * 
	 * @param aHalUrl The URL pointing to the HAL-Endpoint.
	 * @param aOauthUrl The URL pointing to the OAuth authentication endpoint.
	 * @param aOauthClientId The assigned OAuth client TID.
	 * @param aOauthClientSecret The client's OAuth secret.
	 * @param aOauthUserName The OAuth authorization user name.
	 * @param aOauthUserSecret The OAuth authorization user secret.
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status when requesting the OAuth URL.
	 * @throws MalformedURLException Thrown to indicate that a malformed OAuth
	 *         URL has occurred.
	 */
	public HalClient( Url aHalUrl, Url aOauthUrl, String aOauthClientId, String aOauthClientSecret, String aOauthUserName, String aOauthUserSecret ) throws HttpStatusException, MalformedURLException {
		_halUrl = toValidUrl( aHalUrl.toHttpUrl() );
		final OauthToken theOauthToken = toOauthTokenHandler( aOauthUrl.toHttpUrl(), aOauthClientId, aOauthClientSecret, aOauthUserName, aOauthUserSecret );
		_restClient.setOauthToken( theOauthToken );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Introspects the provided entity at the HAL resource and returns an
	 * instance of the {@link CanonicalMap} representing the reflected data
	 * structure. By default, Meta-Data is included in the result. If otherwise
	 * required, use {@link #introspect(String, TraversalMode)}.
	 * 
	 * @param aEntity The entity to be introspected.
	 * 
	 * @return A {@link CanonicalMap} instance representing the structure of the
	 *         entity, e.g. providing the attribute names and the data types as
	 *         well as other constraints representing the layout of the entity
	 *         (think of a class defining the layout of its instances).
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public HalStruct introspect( String aEntity ) throws HttpStatusException {
		return introspect( aEntity, TraversalMode.RECURSIVE_IMPORT_CHILDREN_KEEP_SELF_HREFS_KEEP_DANGLING_HREFS );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public HalClient withOAuthToken( OauthToken aOauthToken ) {
		setOauthToken( aOauthToken );
		return this;
	}

	/**
	 * Sets the names of the entities to be excluded by the {@link HalClient}.
	 *
	 * @param entities The entities to be excluded.
	 * 
	 * @return This instance ass of the builder pattern.
	 */
	public HalClient withExcludeEntities( String[] entities ) {
		setExcludeEntities( entities );
		return this;
	}

	/**
	 * Sets the names of the entities to be included by the {@link HalClient}.
	 *
	 * @param entities The entities to be included.
	 * 
	 * @return This instance ass of the builder pattern.
	 */
	public HalClient withIncludeEntities( String[] entities ) {
		setIncludeEntities( entities );
		return this;
	}

	// /////////////////////////////////////////////////////////////////////////
	// CRUD:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Creates a new entitie's element (row) at the HAL resource consisting of
	 * the properties as of the provided {@link CanonicalMap}.
	 * 
	 * @param aEntity The entity for which to create the element (row).
	 * @param aProperties The {@link CanonicalMap} containing the properties of
	 *        the entity be created.
	 * 
	 * @return A {@link HalData} with the newly created element (row).
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public HalData create( String aEntity, HalData aProperties ) throws HttpStatusException {
		RestRequestBuilder theRequest = null;
		try {
			final String theHref = toEntityLink( aEntity );
			theRequest = _restClient.buildPost( theHref, aProperties.toPayload() ).withRedirectDepth( DEFAULT_REDIRECT_DEPTH );
			theRequest.getHeaderFields().putContentType( MediaType.APPLICATION_JSON );
			final RestResponse theResponse = theRequest.toRestResponse();
			if ( theResponse.getHttpStatusCode().isErrorStatus() ) {
				throw theResponse.getHttpStatusCode().toHttpStatusException( "Cannot create entity <" + aEntity + "> as of HTTP-Status-Code <" + theResponse.getHttpStatusCode() + "> (" + theResponse.getHttpStatusCode().getStatusCode() + "): " + theResponse.getHttpBody() );
			}
			return toHalData( theResponse.getResponse(), TraversalMode.NONE_KEEP_SELF_HREFS_KEEP_DANGLING_HREFS );
		}
		catch ( MalformedURLException e ) {
			throw new InternalClientErrorException( "Internal client error as of a malformed" + ( theRequest != null ? " <" + theRequest.getUrl() + ">" : "" ) + " URL!", e );
		}
	}

	/**
	 * Creates a new entitie's element (row) at the HAL resource consisting of
	 * the properties as of the provided {@link CanonicalMap}.
	 * 
	 * @param aEntity The entity for which to create the element (row).
	 * @param aObj The {@link Object} from which the properties are retrieved by
	 *        means of reflection for the entity be created.
	 * 
	 * @return A {@link HalData} with the newly created element (row).
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public HalData create( String aEntity, Object aObj ) throws HttpStatusException {
		return create( aEntity, new HalData( aObj ) );
	}

	/**
	 * Retrieves the properties with the according values for the given entity
	 * with the given TID from the HAL resource. Any HREF resources are ignored
	 * put the link is preserved, e.g. as of
	 * {@link TraversalMode#IMPORT_CHILDREN_KEEP_DANGLING_HREFS}.
	 * 
	 * @param aEntity The entity for which to retrieve the properties.
	 * @param aId The TID of the entity for which to retrieve the properties.
	 * 
	 * @return A {@link CanonicalMap} with the property names assigned to the
	 *         according values.
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public HalData read( String aEntity, Long aId ) throws HttpStatusException {
		return read( aEntity, aId, (FormFields) null );
	}

	/**
	 * Retrieves the properties with the according values for the given entity
	 * with the given TID from the HAL resource. Any HREF resources are ignored
	 * put the link is preserved, e.g. as of
	 * {@link TraversalMode#IMPORT_CHILDREN_KEEP_DANGLING_HREFS}.
	 * 
	 * @param aEntity The entity for which to retrieve the properties.
	 * @param aId The TID of the entity for which to retrieve the properties.
	 * @param aQueryFields The query fields to be appended to the requesting
	 *        URL.
	 * 
	 * @return A {@link CanonicalMap} with the property names assigned to the
	 *         according values.
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public HalData read( String aEntity, Long aId, FormFields aQueryFields ) throws HttpStatusException {
		return read( aEntity, aId, TraversalMode.IMPORT_CHILDREN_KEEP_DANGLING_HREFS, aQueryFields );
	}

	/**
	 * Retrieves the properties with the according values for the given entity
	 * with the given TID from the HAL resource.
	 * 
	 * @param aEntity The entity for which to retrieve the properties.
	 * @param aId The TID of the entity for which to retrieve the properties.
	 * @param aMode The mode for handling HREF resources, e.g. load them as well
	 *        or ignore them.
	 * 
	 * @return A {@link CanonicalMap} with the property names assigned to the
	 *         according values.
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public HalData read( String aEntity, Long aId, TraversalMode aMode ) throws HttpStatusException {
		return read( aEntity, aId, aMode, (FormFields) null );
	}

	/**
	 * Retrieves the properties with the according values for the given entity
	 * with the given TID from the HAL resource.
	 * 
	 * @param aEntity The entity for which to retrieve the properties.
	 * @param aId The TID of the entity for which to retrieve the properties.
	 * @param aMode The mode for handling HREF resources, e.g. load them as well
	 *        or ignore them.
	 * @param aQueryFields The query fields to be appended to the requesting
	 *        URL.
	 * 
	 * @return A {@link CanonicalMap} with the property names assigned to the
	 *         according values.
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public HalData read( String aEntity, Long aId, TraversalMode aMode, FormFields aQueryFields ) throws HttpStatusException {
		String theHref = null;
		try {
			theHref = toEntityLink( aEntity );
			theHref = theHref + "/" + aId;
			return read( theHref, aMode, aQueryFields );

		}
		catch ( MalformedURLException e ) {
			throw new InternalClientErrorException( "Internal client error as of a malformed" + ( theHref != null ? " <" + theHref + ">" : "" ) + " URL!", e );
		}
	}

	/**
	 * Retrieves the properties with the according values of all elements (rows)
	 * for the given entity from the HAL resource. Any HREF resources are
	 * ignored put the link is preserved, e.g. as of
	 * {@link TraversalMode#IMPORT_CHILDREN_KEEP_DANGLING_HREFS}.
	 * 
	 * @param aEntity The entity for which to retrieve the properties of all
	 *        elements (rows).
	 * 
	 * @return A {@link HalData} list of type {@link HalDataPage} containing one
	 *         {@link HalData} instance per element (row).
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public HalDataPage readAll( String aEntity ) throws HttpStatusException {
		return readAll( aEntity, TraversalMode.IMPORT_CHILDREN_KEEP_DANGLING_HREFS, (FormFields) null );
	}

	/**
	 * Retrieves the properties with the according values of all elements (rows)
	 * for the given entity from the HAL resource. Any HREF resources are
	 * ignored put the link is preserved, e.g. as of
	 * {@link TraversalMode#IMPORT_CHILDREN_KEEP_DANGLING_HREFS}.
	 * 
	 * @param aEntity The entity for which to retrieve the properties of all
	 *        elements (rows).
	 * @param aQueryFields The query fields to be appended to the requesting
	 *        URL.
	 * 
	 * @return A {@link HalData} list of type {@link HalDataPage} containing one
	 *         {@link HalData} instance per element (row).
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public HalDataPage readAll( String aEntity, FormFields aQueryFields ) throws HttpStatusException {
		return readAll( aEntity, TraversalMode.IMPORT_CHILDREN_KEEP_DANGLING_HREFS, aQueryFields );
	}

	/**
	 * Retrieves the properties with the according values of all elements (rows)
	 * for the given entity from the HAL resource.
	 * 
	 * @param aEntity The entity for which to retrieve the properties of all
	 *        elements (rows).
	 * @param aMode The mode for handling HREF resources, e.g. load them as well
	 *        or ignore them.
	 * 
	 * @return A {@link HalData} list of type {@link HalDataPage} containing one
	 *         {@link HalData} instance per element (row).
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public HalDataPage readAll( String aEntity, TraversalMode aMode ) throws HttpStatusException {
		return readAll( aEntity, aMode, (FormFields) null );
	}

	/**
	 * Retrieves the properties with the according values of all elements (rows)
	 * for the given entity from the HAL resource.
	 * 
	 * @param aEntity The entity for which to retrieve the properties of all
	 *        elements (rows).
	 * @param aMode The mode for handling HREF resources, e.g. load them as well
	 *        or ignore them.
	 * @param aQueryFields The query fields to be appended to the requesting
	 *        URL.
	 * 
	 * @return A {@link HalData} list of type {@link HalDataPage} containing one
	 *         {@link HalData} instance per element (row).
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public HalDataPage readAll( String aEntity, TraversalMode aMode, FormFields aQueryFields ) throws HttpStatusException {
		try {
			final String theHref = toEntityLink( aEntity );
			final RestRequestBuilder theRequest = _restClient.buildGet( theHref, aQueryFields ).withRedirectDepth( DEFAULT_REDIRECT_DEPTH );
			final RestResponse theResponse = theRequest.toRestResponse();
			handleHttpStatusException( "Cannot read HAL data for entity '" + aEntity + "'", theRequest, theResponse );
			final HttpBodyMap theHttpBody = theResponse.getResponse();

			int thePageNumber = 0;
			int thePageSize = -1;
			int thePageCount = 1;

			if ( theHttpBody.containsKey( HalDataPage.PAGE_NUMBER ) ) {
				try {
					thePageNumber = theHttpBody.getInt( HalDataPage.PAGE_NUMBER );
				}
				catch ( NumberFormatException ignore ) { /* ignore */ }
			}
			if ( theHttpBody.containsKey( HalDataPage.PAGE_SIZE ) ) {
				try {
					thePageSize = theHttpBody.getInt( HalDataPage.PAGE_SIZE );
				}
				catch ( NumberFormatException ignore ) { /* ignore */ }
			}
			if ( theHttpBody.containsKey( HalDataPage.PAGE_COUNT ) ) {
				try {
					thePageCount = theHttpBody.getInt( HalDataPage.PAGE_COUNT );
				}
				catch ( NumberFormatException ignore ) { /* ignore */ }
			}

			final HalDataPage theHalDatas = new HalDataPage( thePageNumber, thePageSize, thePageCount );

			String theEntity = toEmbeddedEntity( theHttpBody );
			if ( theEntity == null ) {
				theEntity = aEntity;
			}
			final int[] theIndexes;
			HalData eHalData;
			HttpBodyMap eHttpBody;
			final Map<String, HttpBodyMap> theCachedHrefs = new HashMap<>();
			final String thePath = theHttpBody.toPath( HalMap.HAL_EMBEDDED, theHttpBody.getDelimiter(), theEntity );
			if ( theHttpBody.isIndexDir( thePath ) && ( theIndexes = theHttpBody.getDirIndexes( thePath ) ).length > 0 ) {
				Set<String> eVisitedHrefs;
				for ( int theIndexe : theIndexes ) {
					eHttpBody = theHttpBody.getDirAt( thePath, theIndexe );
					eVisitedHrefs = new HashSet<>();
					eHalData = toHalData( eHttpBody, aMode, eVisitedHrefs, theCachedHrefs );
					theHalDatas.add( eHalData );
				}
			}
			return theHalDatas;
		}
		catch ( HttpStatusException | MalformedURLException e ) {
			throw new InternalClientErrorException( "Cannot read data as of an internal client error!", e );
		}
	}

	/**
	 * Retrieves the properties with the according values of all elements (rows)
	 * for the given entity from the HAL resource. Any HREF resources are
	 * ignored put the link is preserved, e.g. as of
	 * {@link TraversalMode#IMPORT_CHILDREN_KEEP_DANGLING_HREFS}. The
	 * HAL-Browser must support pagination for the requested result set's page
	 * number and a page's size to function as intended. In Spring Boot you
	 * usually use a repository of type <code>PagingAndSortingRepository</code>.
	 * 
	 * @param aEntity The entity for which to retrieve the properties of all
	 *        elements (rows).
	 * @param aPage The page as of pagination to retrieve.
	 * @param aPageSize The page's size as of pagination for the page to
	 *        retrieve.
	 * 
	 * @return A {@link HalData} list of type {@link HalDataPage} containing one
	 *         {@link HalData} instance per element (row).
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public HalDataPage readPage( String aEntity, int aPage, int aPageSize ) throws HttpStatusException {
		final FormFields theFields = new FormFields().withAddTo( "page", Integer.toString( aPage ) ).withAddTo( "size", Integer.toString( aPageSize ) );
		return readAll( aEntity, TraversalMode.IMPORT_CHILDREN_KEEP_DANGLING_HREFS, theFields );
	}

	/**
	 * Retrieves the properties with the according values of all elements (rows)
	 * for the given entity from the HAL resource. The HAL-Browser must support
	 * pagination for the requested result set's page number and a page's size
	 * to function as intended. In Spring Boot you usually use a repository of
	 * type <code>PagingAndSortingRepository</code>.
	 * 
	 * @param aEntity The entity for which to retrieve the properties of all
	 *        elements (rows).
	 * @param aPage The page as of pagination to retrieve.
	 * @param aPageSize The page's size as of pagination for the page to
	 *        retrieve.
	 * @param aMode The mode for handling HREF resources, e.g. load them as well
	 *        or ignore them.
	 * 
	 * @return A {@link HalData} list of type {@link HalDataPage} containing one
	 *         {@link HalData} instance per element (row).
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public HalDataPage readPage( String aEntity, int aPage, int aPageSize, TraversalMode aMode ) throws HttpStatusException {
		final FormFields theFields = new FormFields().withAddTo( "page", Integer.toString( aPage ) ).withAddTo( "size", Integer.toString( aPageSize ) );
		return readAll( aEntity, aMode, theFields );
	}

	/**
	 * Retrieves the properties with the according values of all elements (rows)
	 * for the given entity from the HAL resource. Any HREF resources are
	 * ignored put the link is preserved, e.g. as of
	 * {@link TraversalMode#IMPORT_CHILDREN_KEEP_DANGLING_HREFS}. The
	 * HAL-Browser must support pagination for the requested result set's page
	 * number and a page's size to function as intended. In Spring Boot you
	 * usually use a repository of type <code>PagingAndSortingRepository</code>.
	 * 
	 * @param aEntity The entity for which to retrieve the properties of all
	 *        elements (rows).
	 * @param aPage The page as of pagination to retrieve.
	 * @param aPageSize The page's size as of pagination for the page to
	 *        retrieve.
	 * @param aQueryFields The query fields to be appended to the requesting
	 *        URL.
	 * 
	 * @return A {@link HalData} list of type {@link HalDataPage} containing one
	 *         {@link HalData} instance per element (row).
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public HalDataPage readPage( String aEntity, int aPage, int aPageSize, FormFields aQueryFields ) throws HttpStatusException {
		if ( aQueryFields == null ) {
			aQueryFields = new FormFields();
		}
		aQueryFields.withAddTo( "page", Integer.toString( aPage ) ).withAddTo( "size", Integer.toString( aPageSize ) );
		return readAll( aEntity, TraversalMode.IMPORT_CHILDREN_KEEP_DANGLING_HREFS, aQueryFields );
	}

	/**
	 * Retrieves the properties with the according values of all elements (rows)
	 * for the given entity from the HAL resource. The HAL-Browser must support
	 * pagination for the requested result set's page number and a page's size
	 * to function as intended. In Spring Boot you usually use a repository of
	 * type <code>PagingAndSortingRepository</code>.
	 * 
	 * @param aEntity The entity for which to retrieve the properties of all
	 *        elements (rows).
	 * @param aPage The page as of pagination to retrieve.
	 * @param aPageSize The page's size as of pagination for the page to
	 *        retrieve.
	 * @param aMode The mode for handling HREF resources, e.g. load them as well
	 *        or ignore them.
	 * @param aQueryFields The query fields to be appended to the requesting
	 *        URL.
	 * 
	 * @return A {@link HalData} list of type {@link HalDataPage} containing one
	 *         {@link HalData} instance per element (row).
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public HalDataPage readPage( String aEntity, int aPage, int aPageSize, TraversalMode aMode, FormFields aQueryFields ) throws HttpStatusException {
		if ( aQueryFields == null ) {
			aQueryFields = new FormFields();
		}
		aQueryFields.withAddTo( "page", Integer.toString( aPage ) ).withAddTo( "size", Integer.toString( aPageSize ) );
		return readAll( aEntity, aMode, aQueryFields );
	}

	/**
	 * Creates a new entitie's element (row) at the HAL resource consisting of
	 * the properties as of the provided {@link CanonicalMap}.
	 * 
	 * @param aEntity The entity for which to create the element (row).
	 * @param aObj The {@link Object} from which the properties are retrieved by
	 *        means of reflection for the entity be created.
	 * 
	 * @return A {@link HalData} with the newly created element (row).
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public HalData update( String aEntity, Object aObj ) throws HttpStatusException {
		return update( aEntity, new HalData( aObj ) );
	}

	/**
	 * Updates an existing entitie's element (row) at the HAL resource using the
	 * properties as of the provided {@link CanonicalMap}.
	 * 
	 * @param aEntity The entity for which to update the element (row).
	 * @param aId The TID of the element (row) of the entity to be updated.
	 * @param aProperties The {@link CanonicalMap} containing the properties of
	 *        the entity to be updated.
	 * 
	 * @return The updated representation of the entitie's element (row).
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public HalData update( String aEntity, String aId, HalData aProperties ) throws HttpStatusException {
		String theHref = null;
		try {
			theHref = toEntityLink( aEntity );
			theHref = theHref + "/" + aId;
			final RestRequestBuilder theRequest = _restClient.buildPut( theHref, aProperties.toPayload() ).withRedirectDepth( DEFAULT_REDIRECT_DEPTH );
			theRequest.getHeaderFields().putContentType( MediaType.APPLICATION_JSON );
			final RestResponse theResponse = theRequest.toRestResponse();
			if ( theResponse.getHttpStatusCode().isErrorStatus() ) {
				throw theResponse.getHttpStatusCode().toHttpStatusException( "Cannot update entity <" + aEntity + "> as of HTTP-Status-Code <" + theResponse.getHttpStatusCode() + "> (" + theResponse.getHttpStatusCode().getStatusCode() + "): " + theResponse.getHttpBody() );
			}
			return toHalData( theResponse.getResponse(), TraversalMode.NONE_KEEP_SELF_HREFS_KEEP_DANGLING_HREFS );
		}
		catch ( MalformedURLException e ) {
			throw new InternalClientErrorException( "Internal client error as of a malformed" + ( theHref != null ? " <" + theHref + ">" : "" ) + " URL!", e );
		}
	}

	/**
	 * Deletes an entitie's element (row) from the HAL resource.
	 * 
	 * @param aEntity The entity for which to delete the element (row).
	 * @param aId The TID of the element (row) of the entity to be deleted.
	 * 
	 * @return True in case the element (row) has been deleted, false if there
	 *         was none such element (row) at the HAL resource (the expectation
	 *         is satisfied now as there is no element (row) with the given TID
	 *         in the HAL resource after the operation).
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public boolean delete( String aEntity, String aId ) throws HttpStatusException {
		String theHref = null;
		try {
			theHref = toEntityLink( aEntity );
			theHref = theHref + "/" + aId;
			final RestRequestBuilder theRequest = _restClient.buildDelete( theHref ).withRedirectDepth( DEFAULT_REDIRECT_DEPTH );
			theRequest.getHeaderFields().putContentType( MediaType.APPLICATION_JSON );
			final RestResponse theResponse = theRequest.toRestResponse();
			if ( theResponse.getHttpStatusCode().isSuccessStatus() ) {
				return true;
			}

			if ( theResponse.getHttpStatusCode() == HttpStatusCode.NOT_FOUND ) {
				return false;
			}

			throw toHttpStatusExcpetion( "Cannot delete entity <" + aEntity + ">", theRequest, theResponse );

		}
		catch ( MalformedURLException e ) {
			throw new InternalClientErrorException( "Internal client error as of a malformed" + ( theHref != null ? " <" + theHref + ">" : "" ) + " URL!", e );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// INTROSPECTION:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Determines the entities provided by the HAL resource.
	 * 
	 * @return A {@link String} array with the according entity names.
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public String[] entities() throws HttpStatusException {
		final List<String> theEntities = new ArrayList<>();
		RestResponse theResponse = null;
		try {
			final String theProfileLink = toProfileLink();
			final RestRequestBuilder theRequest = _restClient.buildGet( theProfileLink ).withRedirectDepth( DEFAULT_REDIRECT_DEPTH );
			theRequest.getHeaderFields().withContentType( MediaType.APPLICATION_JSON ).withAcceptTypes( MediaType.APPLICATION_JSON );
			theResponse = theRequest.toRestResponse();
			handleHttpStatusException( "Cannot read all HAL entities", theRequest, theResponse );
			final HttpBodyMap theBody = theResponse.getResponse();
			final List<String> theNames = new ArrayList<>( theBody.children( HalMap.HAL_LINKS ) );
			Collections.sort( theNames );
			String eLink;
			for ( String eKey : theNames ) {
				if ( isIncludeEntity( eKey ) && !isReservedEntity( eKey ) ) {
					eLink = theBody.get( HalMap.HAL_LINKS, eKey, HalMap.HAL_HREF );
					if ( eLink != null ) {
						theEntities.add( eKey );
					}
				}
			}
		}
		catch ( MalformedURLException e ) {
			throw new InternalClientErrorException( ( theResponse != null ? ( " :" + theResponse.getHttpBody() ) : "" ), e );
		}
		return theEntities.toArray( new String[theEntities.size()] );
	}

	/**
	 * Introspects the provided entity at the HAL resource and returns an
	 * instance of the {@link CanonicalMap} representing the reflected data
	 * structure.
	 * 
	 * @param aEntity The entity to be introspected.
	 * @param aMode The mode of operation whilst introspecting an entity: You
	 *        may include MetaData as of
	 *        {@link TraversalMode#IMPORT_CHILDREN_KEEP_DANGLING_HREFS} or just
	 *        retrieve the plain data structure without Meta-Data as of
	 *        {@link TraversalMode#IMPORT_CHILDREN}.
	 * 
	 * @return A {@link CanonicalMap} instance representing the structure of the
	 *         entity, e.g. providing the attribute names and the data types as
	 *         well as other constraints representing the layout of the entity
	 *         (think of a class defining the layout of its instances).
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status.
	 */
	public HalStruct introspect( String aEntity, TraversalMode aMode ) throws HttpStatusException {
		final HalStruct theTemplate = new HalStruct();
		String theHref = null;
		try {
			final Map<String, HttpBodyMap> theCachedHrefs = new HashMap<>();
			theHref = toEntityProfileLink( aEntity, theCachedHrefs );
			introspect( theTemplate, aEntity, theHref, aMode, new HashSet<>(), theCachedHrefs );
			theCachedHrefs.clear();
		}
		catch ( MalformedURLException e ) {
			throw new BadInvocationException( "Internal client error as of a malformed" + ( theHref != null ? " <" + theHref + ">" : "" ) + " URL!", e );
		}
		// Handle mode of operation |-->
		if ( aMode != null && aMode == TraversalMode.NONE ) {
			theTemplate.removeAll( "**", PathMap.ANNOTATOR + "*", "**" );
		}
		// Handle mode of operation <--| 
		return theTemplate;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Adds a HREF's HTTP-Body to the invocation's cache. This method is
	 * introduced as of debugging purposes.
	 * 
	 * @param aHref The HREF to add to the cache.
	 * @param aMediaType The Media-Type being used for the request.
	 * @param aCachedHrefs The already cached HREFs
	 * @param aHttpBody The HTTP-Body to add to the cache.
	 */
	private void addCachedHref( String aHref, MediaType aMediaType, Map<String, HttpBodyMap> aCachedHrefs, HttpBodyMap aHttpBody ) {
		if ( aHref != null ) {
			aCachedHrefs.put( aHref + aMediaType, aHttpBody );
		}
	}

	/**
	 * Examines the HTTP-Body and in case we have more than one entity stored in
	 * the {@link HttpBodyMap} (e.g. we have an array of entities), then the
	 * according entities are processed one by one and added to the resulting
	 * {@link HalData} instance. Else the single entity is processed
	 * accordingly.
	 * 
	 * @param aHttpBody The {@link HttpBodyMap} to be inspected.
	 * @param aMode The {@link TraversalMode} to evaluate.
	 * @param aVisitedHrefs The HREFs which have already been visited for the
	 *        given trait.
	 * @param aCachedHrefs The HREFs which have been visited already, to prevent
	 *        endless recursion.
	 * 
	 * @return The {@link HalData} instance retrieved from the given
	 *         {@link HttpBodyMap}.
	 * 
	 * @throws HttpStatusException
	 * @throws MalformedURLException
	 */
	private HalData fromHttpBody( HttpBodyMap aHttpBody, TraversalMode aMode, Set<String> aVisitedHrefs, Map<String, HttpBodyMap> aCachedHrefs ) throws HttpStatusException, MalformedURLException {
		HalData theHalData = null;
		final String theEntity = toEmbeddedEntity( aHttpBody );
		boolean isArray = false;
		if ( theEntity != null ) {
			final int[] theIndexes;
			final String thePath = aHttpBody.toPath( HalMap.HAL_EMBEDDED, theEntity );
			// Array? -->
			isArray = aHttpBody.isIndexDir( thePath );
			if ( isArray ) {
				theHalData = new HalDataClient();
				theIndexes = aHttpBody.getDirIndexes( thePath );
				if ( theIndexes.length > 0 ) {
					HttpBodyMap eHttpBody;
					HalData eHalData;
					for ( int i = 0; i < theIndexes.length; i++ ) {
						eHttpBody = aHttpBody.getDirAt( thePath, theIndexes[i] );
						eHalData = toHalData( eHttpBody, aMode, aVisitedHrefs, aCachedHrefs );
						theHalData.putDirAt( i, eHalData );
					}
				}
			}
			if ( aMode.isKeepSelfHrefs() ) {
				theHalData.put( HalMap.META_DATA_SELF, toSelfHref( aHttpBody ) );
			}
		}
		// Array? <--|
		if ( !isArray ) {
			theHalData = toHalData( aHttpBody, aMode, aVisitedHrefs, aCachedHrefs );
		}
		return theHalData != null && theHalData.size() != 0 ? theHalData : null;
	}

	/**
	 * Gets a HREF's HTTP-Body from the invocation's cache. This method is
	 * introduced as of debugging purposes.
	 * 
	 * @param aHref The HREF to retrieve from the cache.
	 * @param aMediaType The Media-Type being used for the request.
	 * @param aCachedHrefs The already cached HREFs
	 * 
	 * @return aHttpBody The according HTTP-Body from the cache or null if there
	 *         was none.
	 */
	private HttpBodyMap getCachedHref( String aHref, MediaType aMediaType, Map<String, HttpBodyMap> aCachedHrefs ) {
		return aCachedHrefs.get( aHref + aMediaType );
	}

	/**
	 * Gets the names of the entities to be excluded by the {@link HalClient}.
	 * 
	 * @return The entities to be excluded.
	 */
	public String[] getExcludeEntities() {
		return _excludeEntities;
	}

	/**
	 * Gets the names of the entities to be included by the {@link HalClient}.
	 * 
	 * @return The entities to be included.
	 */
	public String[] getIncludeEntities() {
		return _includeEntities;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public OauthToken getOauthToken() {
		return _restClient.getOauthToken();
	}

	/**
	 * Did we visit this HREF already? I use stack terminology as this seems
	 * semantically fit. This method is introduced as of debugging purposes.
	 * 
	 * @param aHref The HREF to test.
	 * @param aVisitedHrefs The already visited HREFs.
	 * 
	 * @return True in case we already visited this one.
	 */
	private boolean hasVisitedHref( String aHref, Set<String> aVisitedHrefs ) {
		return aVisitedHrefs.contains( aHref );
	}

	/**
	 * Introspects the entity.
	 * 
	 * @param aTemplate The template to be filled up with introspection
	 *        findings.
	 * @param aEntity The entity to be introspected.
	 * @param aHref The link to the HAL profile of the entity.
	 * @param aMode The mode of operation whilst introspecting an entity: You
	 *        may include MetaData as of {@link TraversalMode#INCLUDE_META_DATA}
	 *        or just retrieve the plain data structure without Meta-Data as of
	 *        {@link TraversalMode#NO_META_DATA}.
	 * @param aCachedHrefs The HREFs which have been visited already, to prevent
	 *        endless recursion.
	 * 
	 * @throws MalformedURLException Thrown to indicate that a malformed URL has
	 *         occurred.
	 * @throws HttpResponseException Thrown by a HTTP-Response handling system
	 *         in case of some unexpected response.
	 */
	private void introspect( CanonicalMapBuilder aTemplate, String aEntity, String aHref, TraversalMode aMode, Set<String> aVisitedHrefs, Map<String, HttpBodyMap> aCachedHrefs ) throws MalformedURLException, HttpStatusException {
		introspectSchemaJson( aTemplate, aEntity, aHref, aMode, aCachedHrefs );
		introspectHalJson( aTemplate, aEntity, aHref, aMode, aVisitedHrefs, aCachedHrefs );
	}

	// /////////////////////////////////////////////////////////////////////////
	// INTROSPECTION:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Introspects the entity by means of the
	 * {@link MediaType#APPLICATION_HAL_JSON}.
	 * 
	 * @param aTemplate The template to be filled up with introspection
	 *        findings.
	 * @param aEntity The entity to be introspected.
	 * @param aHref The link to the HAL profile of the entity.
	 * @param aMode The mode of operation whilst introspecting an entity: You
	 *        may include MetaData as of {@link TraversalMode#INCLUDE_META_DATA}
	 *        or just retrieve the plain data structure without Meta-Data as of
	 *        {@link TraversalMode#NO_META_DATA}.
	 * @param aCachedHrefs The HREFs which have been visited already, to prevent
	 *        endless recursion.
	 * 
	 * @throws MalformedURLException Thrown to indicate that a malformed URL has
	 *         occurred.
	 * @throws HttpResponseException Thrown by a HTTP-Response handling system
	 *         in case of some unexpected response.
	 */
	private void introspectHalJson( CanonicalMapBuilder aTemplate, String aEntity, String aHref, TraversalMode aMode, Set<String> aVisitedHrefs, Map<String, HttpBodyMap> aCachedHrefs ) throws MalformedURLException, HttpStatusException {

		HttpBodyMap theHttpBody = getCachedHref( aHref, MediaType.APPLICATION_HAL_JSON, aCachedHrefs );
		if ( theHttpBody == null ) {
			final RestRequestBuilder theRequest = _restClient.buildGet( aHref ).withRedirectDepth( DEFAULT_REDIRECT_DEPTH );
			theRequest.getHeaderFields().withContentType( MediaType.APPLICATION_HAL_JSON ).withAcceptTypes( MediaType.APPLICATION_HAL_JSON );
			final RestResponse theResponse = theRequest.toRestResponse();
			handleHttpStatusException( "Cannot introspect HAL entity '" + aEntity + '"', theRequest, theResponse );
			theHttpBody = theResponse.getResponse();
			if ( theHttpBody == null ) {
				throw new BadResponseException( "Cannot resolve relation for entity <" + aEntity + ">!" );
			}
			addCachedHref( aHref, MediaType.APPLICATION_HAL_JSON, aCachedHrefs, theHttpBody );
		}
		theHttpBody = theHttpBody.retrieveFrom( HalMap.HAL_ALPS, HalMap.HAL_DESCRIPTOR );
		if ( theHttpBody == null || theHttpBody.size() == 0 ) {
			theHttpBody = theHttpBody.retrieveFrom( HalMap.HAL_ALPS, HalMap.HAL_DESCRIPTORS );
		}

		if ( theHttpBody != null && theHttpBody.size() != 0 ) {
			introspectHalMetaData( aTemplate, aEntity, theHttpBody, "", aMode );
			introspectHalRelations( aTemplate, aEntity, theHttpBody, "", aMode, aVisitedHrefs, aCachedHrefs );
		}
	}

	/**
	 * Introspects the entity's Meta-Data by means of the
	 * {@link MediaType#APPLICATION_HAL_JSON}.
	 * 
	 * @param aTemplate The template to be filled up with introspection
	 *        findings.
	 * @param aEntity The entity to be introspected.
	 * @param aHttpBody The profile HTTP body for the entity.
	 * @param aCurrentPath The current path in the HTTP profile being processed
	 *        (needed for recursion).
	 * @param aMode The mode of operation whilst introspecting an entity: You
	 *        may include MetaData as of {@link TraversalMode#INCLUDE_META_DATA}
	 *        or just retrieve the plain data structure without Meta-Data as of
	 *        {@link TraversalMode#NO_META_DATA}.
	 */
	private void introspectHalMetaData( CanonicalMapBuilder aTemplate, String aEntity, HttpBodyMap aHttpBody, String aCurrentPath, TraversalMode aMode ) {
		if ( aHttpBody.isIndexDir() ) {
			int[] theDirIndexes = aHttpBody.getDirIndexes();
			String eName;
			String eValue;
			for ( int i = 0; i < theDirIndexes.length; i++ ) {
				if ( aHttpBody.get( theDirIndexes[i], HalMap.HAL_NAME ) == null ) {
					aHttpBody = aHttpBody.retrieveFrom( theDirIndexes[i], HalMap.HAL_DESCRIPTOR );
					if ( aHttpBody == null || aHttpBody.size() == 0 ) {
						aHttpBody = aHttpBody.retrieveFrom( theDirIndexes[i], HalMap.HAL_DESCRIPTORS );
					}
					if ( aHttpBody != null && aHttpBody.size() != 0 ) {
						theDirIndexes = aHttpBody.getDirIndexes();
						for ( int theDirIndexe : theDirIndexes ) {
							eName = aHttpBody.get( theDirIndexe, HalMap.HAL_NAME );
							eValue = aHttpBody.get( theDirIndexe, HalMap.HAL_DOC, HalMap.HAL_VALUE );
							if ( eValue != null && eValue.length() != 0 ) {
								if ( !aTemplate.isArray( eName, HalMap.META_DATA_ENUM ) ) {
									aTemplate.putArray( new String[] { aCurrentPath, eName, HalMap.META_DATA_ENUM }, toEnumArray( eValue ) );
								}
							}
						}
					}
				}
			}
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Introspects the entity's relations by means of the
	 * {@link MediaType#APPLICATION_HAL_JSON}.
	 * 
	 * @param aTemplate The template to be filled up with introspection
	 *        findings.
	 * @param aEntity The entity to be introspected.
	 * @param aHttpBody The profile HTTP body for the entity.
	 * @param aCurrentPath The current path in the HTTP profile being processed
	 *        (needed for recursion). * @param aCachedHrefs The HREFs which have
	 *        been visited already, to prevent endless recursion.
	 * @param aCachedHrefs The HREFs which have been visited already, to prevent
	 *        endless recursion.
	 * @param aMode The mode of operation whilst introspecting an entity: You
	 *        may include MetaData as of {@link TraversalMode#INCLUDE_META_DATA}
	 *        or just retrieve the plain data structure without Meta-Data as of
	 *        {@link TraversalMode#NO_META_DATA}.
	 */
	private void introspectHalRelations( CanonicalMapBuilder aTemplate, String aEntity, HttpBodyMap aHttpBody, String aCurrentPath, TraversalMode aMode, Set<String> aVisitedHrefs, Map<String, HttpBodyMap> aCachedHrefs ) throws HttpStatusException, MalformedURLException {
		if ( aHttpBody.isIndexDir() ) {
			int[] theDirIndexes = aHttpBody.getDirIndexes();
			String eEntity;
			String eHref;
			String tmp;
			for ( int i = 0; i < theDirIndexes.length; i++ ) {
				if ( aHttpBody.get( theDirIndexes[i], HalMap.HAL_NAME ) == null && aHttpBody.get( theDirIndexes[i], HalMap.HAL_HREF ) != null ) {
					aHttpBody = aHttpBody.retrieveFrom( theDirIndexes[i], HalMap.HAL_DESCRIPTOR );
					if ( aHttpBody == null || aHttpBody.size() == 0 ) {
						aHttpBody = aHttpBody.retrieveFrom( theDirIndexes[i], HalMap.HAL_DESCRIPTORS );
					}
					if ( aHttpBody != null && aHttpBody.size() != 0 ) {
						theDirIndexes = aHttpBody.getDirIndexes();
						for ( int theDirIndexe : theDirIndexes ) {
							eEntity = aHttpBody.get( theDirIndexe, HalMap.HAL_NAME );
							eHref = toRelation( aHttpBody.get( theDirIndexe, HalMap.HAL_RELATION ) );
							if ( eHref != null ) {
								tmp = aTemplate.get( aCurrentPath, eEntity );
								if ( tmp != null && tmp.length() != 0 && !tmp.equals( URI_IDENTIFIER ) ) {
									LOGGER.log( Level.WARNING, "Relation for <" + aEntity + ">  with path (attribute) <" + aHttpBody.toPath( aCurrentPath, eEntity ) + "> and value <" + tmp + "> is reassigned with relation <" + eHref + ">." );
								}
								if ( !aTemplate.containsKey( aHttpBody.toPath( aCurrentPath, eEntity ) ) ) {
									LOGGER.log( Level.WARNING, "Entity <" + aEntity + "> contains unidentified structure element <" + aHttpBody.toPath( aCurrentPath, eEntity ) + ">." );
								}

								try {
									eHref = toEntityProfileLink( eHref, aCachedHrefs );
								}
								catch ( Exception ignore ) {}

								if ( !hasVisitedHref( eHref, aVisitedHrefs ) ) {
									if ( aMode != null && aMode.isImportChildren() ) {
										final CanonicalMapBuilder eTemplate = new CanonicalMapBuilderImpl();
										pushVisitedHref( eHref, aVisitedHrefs );
										introspect( eTemplate, eEntity, eHref, aMode.nextMode(), aVisitedHrefs, aCachedHrefs );
										if ( eTemplate != null && eTemplate.size() != 0 ) {
											aTemplate.insertTo( eEntity, (CanonicalMap) eTemplate );
										}
										popVisitedHref( eHref, aVisitedHrefs );
										if ( IS_RELATION_ALWAYS_AS_ARRRAY ) {
											aTemplate.putBoolean( aTemplate.toPath( aCurrentPath, eEntity, HalMap.META_DATA_ARRAY ), true );
										}
									}
								}
								if ( aMode != null && aMode.isKeepDanglingHrefs() && ( !aMode.isImportChildren() || hasVisitedHref( eHref, aVisitedHrefs ) ) ) {
									aTemplate.put( aTemplate.toPath( aCurrentPath, eEntity, HalMap.META_DATA_SELF ), eHref );
									if ( IS_RELATION_ALWAYS_AS_ARRRAY ) {
										aTemplate.putBoolean( aTemplate.toPath( aCurrentPath, eEntity, HalMap.META_DATA_ARRAY ), true );
									}
								}
							}
						}
					}
					break;
				}
			}
		}
	}

	/**
	 * Further introspects the entity by means of the
	 * {@link MediaType#APPLICATION_SCHEMA_JSON}.
	 * 
	 * @param aTemplate The template to be filled up with introspection
	 *        findings.
	 * @param aEntity The entity to be introspected.
	 * @param aHttpBody The profile HTTP body for the entity.
	 * @param aCurrentPath The current path in the HTTP profile being processed
	 *        (needed for recursion).
	 * @param aMode The mode of operation whilst introspecting an entity: You
	 *        may include MetaData as of {@link TraversalMode#INCLUDE_META_DATA}
	 *        or just retrieve the plain data structure without Meta-Data as of
	 *        {@link TraversalMode#NO_META_DATA}.
	 */
	private void introspectSchemaJson( CanonicalMapBuilder aTemplate, String aEntity, HttpBodyMap aHalSection, HttpBodyMap aHalProfile, String aCurrentPath, TraversalMode aMode ) {
		final HttpBodyMap theProperties = aHalSection.retrieveFrom( HalMap.HAL_PROPERTIES );
		String eValue;
		String eParent;
		String[] eValues;
		HttpBodyMap theQueryMap;
		List<String> theKeys;
		final List<String> theEnums = new ArrayList<>();

		// Enum |-->
		theQueryMap = theProperties.query( "**", HalMap.HAL_ENUM, "*" );
		theKeys = new ArrayList<>( theQueryMap.keySet() );
		for ( String eKey : theKeys ) {
			eParent = theQueryMap.toParentPath( eKey );
			if ( !theEnums.contains( eParent ) ) {
				theEnums.add( eParent );
			}
		}
		Collections.sort( theEnums );
		String eParentTypePath;
		String eParentTypeValue;
		for ( String eKey : theEnums ) {
			if ( theQueryMap.isArray( eKey ) ) {
				eValues = theQueryMap.getArray( eKey );
				eKey = eKey.substring( 0, eKey.length() - HalMap.HAL_ENUM.length() - 1 );
				aTemplate.removeAll( aTemplate.toPath( aCurrentPath, eKey, "*" ) );
				aTemplate.putArray( new String[] { aCurrentPath, eKey, HalMap.META_DATA_ENUM }, eValues );
			}
		}
		// Enum <--|

		// Type |-->
		theQueryMap = theProperties.query( "**", HalMap.HAL_TYPE );
		theKeys = new ArrayList<>( theQueryMap.keySet() );
		Collections.sort( theKeys );
		// String eParentTypePath, eParentTypeValue;
		boolean isIgnoreEntry;
		for ( String eKey : theKeys ) {
			if ( !theQueryMap.isDir( eKey ) ) {
				isIgnoreEntry = false;
				eValue = theQueryMap.get( eKey );
				eKey = eKey.substring( 0, eKey.length() - HalMap.HAL_TYPE.length() - 1 );
				// Array type handling |-->
				if ( theQueryMap.hasParentPath( eKey ) ) {
					eParentTypePath = theQueryMap.toPath( theQueryMap.toParentPath( eKey ), HalMap.HAL_TYPE );
					eParentTypeValue = theQueryMap.get( eParentTypePath );
					if ( eParentTypeValue != null && ARRAY_IDENTIFIER.equals( eParentTypeValue ) ) {
						isIgnoreEntry = true;
					}
				}
				if ( !isIgnoreEntry ) {
					aTemplate.put( new String[] { aCurrentPath, eKey }, null );
					aTemplate.put( new String[] { aCurrentPath, eKey, HalMap.META_DATA_TYPE }, eValue );
					if ( ARRAY_IDENTIFIER.equals( eValue ) ) {
						aTemplate.put( new String[] { aCurrentPath, eKey, HalMap.META_DATA_ARRAY }, "true" );
					}
				}
			}
		}
		// Type <--|

		// Read only |-->
		theQueryMap = theProperties.query( "**", HalMap.HAL_READ_ONLY );
		theKeys = new ArrayList<>( theQueryMap.keySet() );
		Collections.sort( theKeys );
		// String eParentTypePath, eParentTypeValue;
		for ( String eKey : theKeys ) {
			if ( !theQueryMap.isDir( eKey ) ) {
				isIgnoreEntry = false;
				eValue = theQueryMap.get( eKey );
				eKey = eKey.substring( 0, eKey.length() - HalMap.HAL_READ_ONLY.length() - 1 );
				// Array type handling |-->
				if ( theQueryMap.hasParentPath( eKey ) ) {
					eParentTypePath = theQueryMap.toPath( theQueryMap.toParentPath( eKey ), HalMap.HAL_READ_ONLY );
					eParentTypeValue = theQueryMap.get( eParentTypePath );
					if ( eParentTypeValue != null && ARRAY_IDENTIFIER.equals( eParentTypeValue ) ) {
						isIgnoreEntry = true;
					}
				}
				if ( !isIgnoreEntry ) {
					aTemplate.put( new String[] { aCurrentPath, eKey }, null );
					aTemplate.put( new String[] { aCurrentPath, eKey, HalMap.META_DATA_READ_ONLY }, eValue );
				}
			}
		}
		// Read only <--|

		// Format |-->
		theQueryMap = theProperties.query( "**", HalMap.HAL_FORMAT );
		theKeys = new ArrayList<>( theQueryMap.keySet() );
		Collections.sort( theKeys );
		String eAttribute;
		for ( String eKey : theKeys ) {
			if ( !theQueryMap.isDir( eKey ) ) {
				eAttribute = eKey.substring( 0, eKey.length() - HalMap.HAL_FORMAT.length() - 1 );
				eValue = theQueryMap.get( eKey );
				switch ( eValue ) {
				case DATE_TIME -> eValue = DEFAULT_DATE_TIME_FORMAT;
				case TIME -> eValue = DEFAULT_TIME_FORMAT;
				case DATE -> eValue = DEFAULT_DATE_FORMAT;
				default -> {
				}
				}
				aTemplate.put( new String[] { aCurrentPath, eAttribute }, null );
				aTemplate.put( new String[] { aCurrentPath, eAttribute, HalMap.META_DATA_FORMAT }, eValue );
			}
		}
		// Format <--|

		// Relation |-->
		if ( aMode != null && aMode.isImportChildren() ) {
			String eEntity;
			String[] ePathElements;
			theQueryMap = theProperties.query( "**", HalMap.HAL_REF );
			theKeys = new ArrayList<>( theQueryMap.sortedKeys() );
			for ( String eKey : theKeys ) {
				eValue = theQueryMap.get( eKey );
				if ( eValue != null && eValue.startsWith( PREFIX_REFERENCE ) ) {
					ePathElements = theQueryMap.toPathElements( eKey );
					if ( ePathElements.length >= 2 ) {
						eEntity = ePathElements[ePathElements.length - 2];
						aTemplate.remove( aCurrentPath, eEntity );
						eValue = eValue.substring( 1 );
						eValue = aHalProfile.fromExternalPath( eValue, '/' );
						introspectSchemaJson( aTemplate, aEntity, aHalProfile.retrieveFrom( eValue ), aHalProfile, aHalSection.toPath( aCurrentPath, eEntity ), aMode.nextMode() );
					}
				}
			}
		}
		// Relation <--|
	}

	/**
	 * Introspects the entity by means of the
	 * {@link MediaType#APPLICATION_SCHEMA_JSON}.
	 * 
	 * @param aTemplate The template to be filled up with introspection
	 *        findings.
	 * @param aEntity The entity to be introspected.
	 * @param aHref The link to the HAL profile of the entity.
	 * @param aMode The mode of operation whilst introspecting an entity: You
	 *        may include MetaData as of {@link TraversalMode#INCLUDE_META_DATA}
	 *        or just retrieve the plain data structure without Meta-Data as of
	 *        {@link TraversalMode#NO_META_DATA}.
	 * @param aCachedHrefs The HREFs which have been visited already, to prevent
	 *        endless recursion.
	 * 
	 * @throws MalformedURLException Thrown to indicate that a malformed URL has
	 *         occurred.
	 * @throws HttpResponseException Thrown by a HTTP-Response handling system
	 *         in case of some unexpected response.
	 * @throws BadResponseException This code is used when a client signals a
	 *         bad response being received by a server, e.g. the response cannot
	 *         be processed by the client and does not meet the client's
	 *         expectations.
	 */
	private void introspectSchemaJson( CanonicalMapBuilder aTemplate, String aEntity, String aHref, TraversalMode aMode, Map<String, HttpBodyMap> aCachedHrefs ) throws MalformedURLException, HttpStatusException {
		HttpBodyMap theHttpBody = getCachedHref( aHref, MediaType.APPLICATION_SCHEMA_JSON, aCachedHrefs );
		if ( theHttpBody == null ) {
			final RestRequestBuilder theRequest = _restClient.buildGet( aHref );
			theRequest.getHeaderFields().withContentType( MediaType.APPLICATION_SCHEMA_JSON ).withAcceptTypes( MediaType.APPLICATION_SCHEMA_JSON );
			final RestResponse theResponse = theRequest.toRestResponse();
			handleHttpStatusException( "Cannot introspect HAL entity '" + aEntity + '"', theRequest, theResponse );
			theHttpBody = theResponse.getResponse();
			if ( theHttpBody == null ) {
				throw new BadResponseException( "Received an empty response when querying " );
			}
			addCachedHref( aHref, MediaType.APPLICATION_SCHEMA_JSON, aCachedHrefs, theHttpBody );
		}
		if ( aMode.isKeepSelfHrefs() ) {
			aTemplate.put( HalMap.META_DATA_SELF, aHref );
		}
		introspectSchemaJson( aTemplate, aEntity, theHttpBody, theHttpBody, "", aMode );
	}

	/**
	 * Determines whether the given entity is valid as of the
	 * {@link #getIncludeEntities()} and {@link #getExcludeEntities()}.
	 * 
	 * @param The entity for which to determine whether to include it.
	 * 
	 * @return True in case the entity should be included, else false.
	 */
	private boolean isIncludeEntity( String aEntity ) {
		// !include
		if ( getIncludeEntities() == null || getIncludeEntities().length == 0 ) {
			// !include + !exclude
			if ( getExcludeEntities() == null || getExcludeEntities().length == 0 ) {
				return true;
			}
			// !include + exclude
			else {
				for ( String eExclude : getExcludeEntities() ) {
					if ( eExclude.equals( aEntity ) ) {
						return false;
					}
				}
				return true;
			}
		}
		// include
		else {
			// include + !exclude
			if ( getExcludeEntities() == null || getExcludeEntities().length == 0 ) {
				for ( String eInclude : getIncludeEntities() ) {
					if ( eInclude.equals( aEntity ) ) {
						return true;
					}
				}
				return false;
			}
			// include + exclude
			else {
				for ( String eExclude : getExcludeEntities() ) {
					if ( eExclude.equals( aEntity ) ) {
						return false;
					}
				}
				for ( String eInclude : getIncludeEntities() ) {
					if ( eInclude.equals( aEntity ) ) {
						return true;
					}
				}
				return false;
			}
		}
	}

	/**
	 * Determines whether the provided entity's name is a reserved HAL "entity"
	 * (keyword).
	 * 
	 * @param aEntity The entity to verify.
	 * 
	 * @return True in case the name represents a reserved keyword.
	 */
	private boolean isReservedEntity( String aEntity ) {
		for ( String eEntity : RESERVED_ENTITES ) {
			if ( eEntity.equalsIgnoreCase( aEntity ) ) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Inserts the provided {@link HalData} as of whether the path points to an
	 * indexed directory or not.
	 * 
	 * @param aHalData The {@link HalData} where to insert the data to.
	 * @param aPath The path for the {@link HalData} to be inserted.
	 * @param aHrefHalData The {@link HalData} to be inserted.
	 * @param aMode The {@link TraversalMode} specifies how data in retrieved
	 *        from a HAL-Resource.
	 * @param aCachedHrefs The HREFs which have been visited already, to prevent
	 *        endless recursion.
	 * @param aVisitedHrefs The HREFs which have already been visited for the
	 *        given trait.
	 */
	private void loadHref( HalData aHalData, String aPath, String aHref, TraversalMode aMode, Set<String> aVisitedHrefs, Map<String, HttpBodyMap> aCachedHrefs ) throws MalformedURLException, HttpStatusException {
		if ( aMode.isImportChildren() && !hasVisitedHref( aHref, aVisitedHrefs ) ) {
			pushVisitedHref( aHref, aVisitedHrefs );
			HttpBodyMap theHttpBody = getCachedHref( aHref, MediaType.APPLICATION_JSON, aCachedHrefs );
			if ( theHttpBody == null ) {
				final RestRequestBuilder theRequest = _restClient.buildGet( aHref ).withRedirectDepth( DEFAULT_REDIRECT_DEPTH );
				theRequest.getHeaderFields().withContentType( MediaType.APPLICATION_JSON ).withAcceptTypes( MediaType.APPLICATION_JSON );
				final RestResponse theResponse = theRequest.toRestResponse();
				handleHttpStatusException( "Cannot read HAL data", theRequest, theResponse );
				theHttpBody = theResponse.getResponse();
				addCachedHref( aHref, MediaType.APPLICATION_JSON, aCachedHrefs, theHttpBody );
			}
			final HalData theChildHalData = fromHttpBody( theHttpBody, aMode.nextMode(), aVisitedHrefs, aCachedHrefs );
			if ( theChildHalData != null ) {
				final Set<String> theDirs = theChildHalData.dirs();
				final Iterator<String> e = theDirs.iterator();
				while ( e.hasNext() ) {
					if ( e.next().startsWith( aHalData.getAnnotator() + "" ) ) {
						e.remove();
					}
				}
				if ( theDirs.size() == 1 ) {
					final Stack<String> thePathStack = theChildHalData.toPathStack( aPath );
					if ( thePathStack.size() != 0 && theChildHalData.isIndexDir( thePathStack.pop() ) ) {
						aPath = theChildHalData.toPath( thePathStack );
					}
				}
				if ( theChildHalData.isIndexDir( aPath ) ) {
					aHalData.insert( (CanonicalMap) theChildHalData );
				}
				else {
					aHalData.insertTo( aPath, (CanonicalMap) theChildHalData );
				}
			}
			else {
				if ( !aHalData.containsKey( aPath ) ) {
					aHalData.put( aPath, null );
				}
			}
			popVisitedHref( aHref, aVisitedHrefs );
		}
		else {
			aHalData.put( aPath, null );
			if ( aMode.isKeepDanglingHrefs() ) {
				aHalData.put( aHalData.toPath( aPath, HalMap.META_DATA_SELF ), aHref );
			}
		}
	}

	/**
	 * Remove this HREF from the visited ones. I use stack terminology as this
	 * seems semantically fit. This method is introduced as of debugging
	 * purposes.
	 * 
	 * @param aHref The HREF to remove.
	 * @param aVisitedHrefs The already visited HREFs.
	 */
	private void popVisitedHref( String aHref, Set<String> aVisitedHrefs ) {
		aVisitedHrefs.remove( aHref );
	}

	/**
	 * Add this HREF to the visited ones. I use stack terminology as this seems
	 * semantically fit. This method is introduced as of debugging purposes.
	 * 
	 * @param aHref The HREF to add.
	 * @param aVisitedHrefs The already visited HREFs.
	 */
	private boolean pushVisitedHref( String aHref, Set<String> aVisitedHrefs ) {
		final boolean isAlreadyVisited = !aVisitedHrefs.add( aHref );
		// if ( isAlreadyVisited ) {
		//	System.out.println( "BREAK!" );
		// }
		return isAlreadyVisited;
	}

	/**
	 * Reads the data from the given HREF with the provided mode.
	 * 
	 * @param aHref The HREF from which to retrieve the data.
	 * @param aMode The mode to use when retrieving the data.
	 * @param aQueryFields The HTTP-Query-Fields to be used when requesting the
	 *        resource.
	 * 
	 * @return The according {@link HalData}.
	 */
	private HalData read( String aHref, TraversalMode aMode, FormFields aQueryFields ) throws MalformedURLException, HttpStatusException {
		final RestRequestBuilder theRequest = _restClient.buildGet( aHref, aQueryFields ).withRedirectDepth( DEFAULT_REDIRECT_DEPTH );
		theRequest.getHeaderFields().putContentType( MediaType.APPLICATION_JSON );
		final RestResponse theResponse = theRequest.toRestResponse();
		if ( theResponse.getHttpStatusCode().isErrorStatus() ) {
			throw theResponse.getHttpStatusCode().toHttpStatusException( "Cannot create entity with HREF <" + aHref + "> as of HTTP-Status-Code <" + theResponse.getHttpStatusCode() + "> (" + theResponse.getHttpStatusCode().getStatusCode() + "): " + theResponse.getHttpBody() );
		}
		return toHalData( theResponse.getResponse(), aMode );
	}

	/**
	 * Sets the names of the entities to be excluded by the {@link HalClient}.
	 *
	 * @param theEntities the new exclude entities
	 */
	public void setExcludeEntities( String[] theEntities ) {
		_excludeEntities = theEntities;
	}

	/**
	 * Sets the names of the entities to be included by the {@link HalClient}.
	 *
	 * @param theEntities the new include entities
	 */
	public void setIncludeEntities( String[] theEntities ) {
		_includeEntities = theEntities;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setOauthToken( OauthToken aOauthToken ) {
		_restClient.setOauthToken( aOauthToken );
	}

	/**
	 * Determines the name of the embedded entity.
	 * 
	 * @param aHttpBody The HTTP-Body from which to determine the entity.
	 * 
	 * @return The according name or null if none was determinable.
	 */
	private String toEmbeddedEntity( HttpBodyMap aHttpBody ) {
		final Set<String> theDirs = aHttpBody.dirs( HalMap.HAL_EMBEDDED );
		return theDirs.size() == 1 ? theDirs.iterator().next() : null;
	}

	/**
	 * Determines the link to the data of the given entity. If none is being
	 * found, then a default assumed link is generated.
	 * 
	 * @param aEntity The entity for which to get the HAL profile link.
	 * 
	 * @return The according HAL profile link.
	 * 
	 * @throws MalformedURLException Thrown to indicate that a malformed URL has
	 *         occurred.
	 * @throws HttpResponseException Thrown by a HTTP-Response handling system
	 *         in case of some unexpected response.
	 * @throws BadResponseException This code is used when a client signals a
	 *         bad response being received by a server, e.g. the response cannot
	 *         be processed by the client and does not meet the client's
	 *         expectations.
	 */
	private String toEntityLink( String aEntity ) throws MalformedURLException, HttpStatusException {
		String theEntityLink = new Url( _halUrl, aEntity ).toHttpUrl();
		final RestRequestBuilder theRequest = _restClient.buildGet( _halUrl );
		theRequest.getHeaderFields().withContentType( MediaType.APPLICATION_JSON ).withAcceptTypes( MediaType.APPLICATION_JSON );
		final RestResponse theResponse = theRequest.toRestResponse();
		if ( theResponse.getHttpStatusCode().isSuccessStatus() ) {
			final HttpBodyMap theHttpBody = theResponse.getResponse();
			String tmp = theHttpBody.get( HalMap.HAL_LINKS, aEntity, HalMap.HAL_HREF );
			if ( tmp != null ) {
				if ( theHttpBody.getBoolean( HalMap.HAL_LINKS, aEntity, HalMap.HAL_TEMPLATED ) ) {
					tmp = tmp.replaceAll( "\\{(.*?)\\}", "" );
				}
				theEntityLink = tmp;
			}
		}
		return theEntityLink;

	}

	/**
	 * Determines the link to the data structures (profile) of the given entity.
	 * If none is being found, then a default assumed link is generated.
	 * 
	 * @param aEntity The entity for which to get the HAL profile link.
	 * @param aCachedHrefs The HREFs which have been visited already, to prevent
	 *        endless recursion.
	 * 
	 * @return The according HAL profile link.
	 * 
	 * @throws MalformedURLException Thrown to indicate that a malformed URL has
	 *         occurred.
	 * @throws HttpResponseException Thrown by a HTTP-Response handling system
	 *         in case of some unexpected response.
	 * @throws BadResponseException This code is used when a client signals a
	 *         bad response being received by a server, e.g. the response cannot
	 *         be processed by the client and does not meet the client's
	 *         expectations.
	 */
	private String toEntityProfileLink( String aEntity, Map<String, HttpBodyMap> aCachedHrefs ) throws MalformedURLException, HttpStatusException {
		final String theProfileLink = toProfileLink();
		HttpBodyMap theHttpBody = getCachedHref( theProfileLink, MediaType.APPLICATION_JSON, aCachedHrefs );
		if ( theHttpBody == null ) {
			final RestRequestBuilder theRequest = _restClient.buildGet( theProfileLink );
			theRequest.getHeaderFields().withContentType( MediaType.APPLICATION_JSON ).withAcceptTypes( MediaType.APPLICATION_JSON );
			final RestResponse theResponse = theRequest.toRestResponse();
			handleHttpStatusException( "Cannot determine data structure link for HAL entity '" + aEntity + "'", theRequest, theResponse );
			theHttpBody = theResponse.getResponse();
			addCachedHref( theProfileLink, MediaType.APPLICATION_JSON, aCachedHrefs, theHttpBody );
		}
		final String theLink = theHttpBody.get( HalMap.HAL_LINKS, aEntity, HalMap.HAL_HREF );
		return theLink;
	}

	/**
	 * Converts the enumerations contained in a single {@link String} as of HAL
	 * into an array of {@link String} instances.
	 * 
	 * @param aEnumsString The single line containing the enumerations.
	 * 
	 * @return The resulting array.
	 */
	private String[] toEnumArray( String aEnumsString ) {
		final String[] theElements = aEnumsString.split( "" + Delimiter.CSV.getChar() );
		for ( int i = 0; i < theElements.length; i++ ) {
			theElements[i] = theElements[i].trim();
		}
		return theElements;
	}

	/**
	 * Creates from a entitie's HAL HTTP-Body a {@link HalData} instance.
	 * 
	 * @param aHttpBody HAL HTTP-Body representing the entity.
	 * @param aMode The {@link TraversalMode} specifies how data in retrieved
	 *        from a HAL-Resource.
	 * 
	 * @return The accordingly evaluated {@link HalData} instance.
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status when requesting the OAuth URL.
	 * @throws MalformedURLException Thrown to indicate that a malformed OAuth
	 *         URL has occurred.
	 */
	private HalData toHalData( HttpBodyMap aHttpBody, TraversalMode aMode ) throws HttpStatusException, MalformedURLException {
		return toHalData( aHttpBody, aMode, new HashSet<>(), new HashMap<>() );
	}

	/**
	 * Creates from a entitie's HAL HTTP-Body a {@link HalData} instance.
	 * 
	 * @param aHttpBody HAL HTTP-Body representing the entity.
	 * @param aMode The {@link TraversalMode} specifies how data in retrieved
	 *        from a HAL-Resource.
	 * @param aCachedHrefs The HREFs which have been visited already, to prevent
	 *        endless recursion.
	 * @param aVisitedHrefs The HREFs which have already been visited for the
	 *        given trait.
	 * 
	 * @return The accordingly evaluated {@link HalData} instance.
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status when requesting the OAuth URL.
	 * @throws MalformedURLException Thrown to indicate that a malformed OAuth
	 *         URL has occurred.
	 */
	private HalData toHalData( HttpBodyMap aHttpBody, TraversalMode aMode, Set<String> aVisitedHrefs, Map<String, HttpBodyMap> aCachedHrefs ) throws HttpStatusException, MalformedURLException {
		HalData theHalData = null;
		if ( aMode != null ) {
			theHalData = new HalDataClient();
			final String theSelfHref = toSelfHref( aHttpBody );
			if ( hasVisitedHref( theSelfHref, aVisitedHrefs ) ) {
				if ( aMode.isKeepDanglingHrefs() ) {
					theHalData.put( HalMap.META_DATA_SELF, theSelfHref );
				}
				theHalData.put( theHalData.getRootPath(), null );
			}
			else {

				if ( aMode.isKeepSelfHrefs() ) {
					theHalData.put( HalMap.META_DATA_SELF, theSelfHref );
				}
				pushVisitedHref( theSelfHref, aVisitedHrefs );
				final Set<String> theSelfPaths = aHttpBody.findPaths( theSelfHref );
				final Set<String> theEmbeddedSelfHrefs = aHttpBody.queryPaths( HalMap.HAL_EMBEDDED, "**", HalMap.HAL_PATH_SELF_HREF );
				String eEmbeddedSelfHref;
				for ( String eHrefPath : theEmbeddedSelfHrefs ) {
					eEmbeddedSelfHref = aHttpBody.get( eHrefPath );
					theSelfPaths.addAll( aHttpBody.findPaths( eEmbeddedSelfHref ) );
				}
				String eValue;
				List<String> ePathElements;
				for ( String ePath : aHttpBody.keySet() ) {
					if ( !hasVisitedHref( ePath, theSelfPaths ) ) {
						ePathElements = aHttpBody.toPathList( ePath );
						eValue = aHttpBody.get( ePath );
						if ( ePathElements.size() > 1 && ePathElements.get( 0 ).equals( HalMap.HAL_LINKS ) && ePathElements.get( ePathElements.size() - 1 ).equals( HalMap.HAL_HREF ) ) {
							ePathElements.remove( ePathElements.size() - 1 ); // HAL_PATH_HREF
							ePathElements.remove( 0 ); // HAL_PATH_LINKS
							loadHref( theHalData, theHalData.toPath( ePathElements ), eValue, aMode, aVisitedHrefs, aCachedHrefs );
						}
						else if ( ePathElements.size() > 3 && ePathElements.get( 0 ).equals( HalMap.HAL_EMBEDDED ) && ePathElements.get( ePathElements.size() - 1 ).equals( HalMap.HAL_HREF ) ) {
							if ( ePathElements.get( ePathElements.size() - 3 ).equals( HalMap.HAL_LINKS ) ) {
								ePathElements.remove( ePathElements.size() - 3 ); // HAL_LINKS
							}
							ePathElements.remove( ePathElements.size() - 1 ); // HAL_PATH_HREF
							ePathElements.remove( 0 ); // HAL_EMBEDDED
							loadHref( theHalData, theHalData.toPath( ePathElements ), eValue, aMode, aVisitedHrefs, aCachedHrefs );
						}
						else if ( ePathElements.size() > 1 && ePathElements.get( 0 ).equals( HalMap.HAL_EMBEDDED ) ) {
							ePathElements.remove( 0 ); // HAL_EMBEDDED
							theHalData.put( ePathElements, eValue );
						}
						else if ( !isReservedEntity( ePath ) ) {
							theHalData.put( ePath, eValue );
						}
					}
				}
				popVisitedHref( theSelfHref, aVisitedHrefs );
			}
		}
		return theHalData;
	}

	/**
	 * Creates an {@link OauthTokenHandler} from the provided arguments.
	 * 
	 * @param aHalUrl The URL pointing to the HAL-Endpoint.
	 * @param aOauthUrl The URL pointing to the OAuth authentication endpoint.
	 * @param aOauthClientId The assigned OAuth client TID.
	 * @param aOauthClientSecret The client's OAuth secret.
	 * @param aOauthUserName The OAuth authorization user name.
	 * @param aOauthUserSecret The OAuth authorization user secret.
	 * 
	 * @throws HttpStatusException Thrown in case a HTTP response was of an
	 *         erroneous status when requesting the OAuth URL.
	 * @throws MalformedURLException Thrown to indicate that a malformed OAuth
	 *         URL has occurred.
	 */
	private OauthTokenHandler toOauthTokenHandler( String aOauthUrl, String aOauthClientId, String aOauthClientSecret, String aOauthUserName, String aOauthUserSecret ) throws MalformedURLException, HttpStatusException {
		final RestfulHttpClient theRestClient = new HttpRestClient();
		HttpStatusException theException = null;
		for ( int i = 0; i < IoRetryCount.NORM.getValue(); i++ ) {
			try {
				final OauthTokenHandler theToken = new OauthTokenHandler( aOauthUrl, theRestClient, aOauthClientId, aOauthClientSecret, aOauthUserName, aOauthUserSecret );
				return theToken;
			}
			catch ( HttpResponseException e ) {
				theException = e;
			}
			if ( i < IoRetryCount.NORM.getValue() ) {
				try {
					Thread.sleep( IoSleepLoopTime.NORM.getTimeMillis() );
				}
				catch ( InterruptedException ignore ) {}
			}
		}
		throw theException;
	}

	/**
	 * Determines the link to the entities profiles:
	 *
	 * @formatter:off
	 * <ccode>
	 * {
	 *  "_links" : {
	 *    "users" : {
	 *      "href" : "http://localhost:8080/users{?page,size,sort}",
	 *      "templated" : true
	 *    },
	 *    "profile" : {
	 *      "href" : "http://localhost:8080/profile"
	 *    }
	 *  }
	 * }
	 * </code>
	 * @formatter:on
	 * 
	 * @return The link pointing to the entities profiles.
	 * 
	 * @throws MalformedURLException Thrown to indicate that a malformed URL has
	 *         occurred.
	 * @throws HttpResponseException Thrown by a HTTP-Response handling system
	 *         in case of some unexpected response.
	 * @throws BadResponseException This code is used when a client signals a
	 *         bad response being received by a server, e.g. the response cannot
	 *         be processed by the client and does not meet the client's
	 *         expectations.
	 */
	private String toProfileLink() throws MalformedURLException, HttpResponseException {
		if ( _profileLink == null ) {
			synchronized ( this ) {
				if ( _profileLink == null ) {
					// |--> Default "profile" link
					_profileLink = new Url( _halUrl, HalMap.HAL_PROFILE ).toHttpUrl();
					// Default profile link <--|
					// |--> Get "profile" Link from HAL
					final RestRequestBuilder theRequest = _restClient.buildGet( _halUrl );
					theRequest.getHeaderFields().withContentType( MediaType.APPLICATION_JSON ).withAcceptTypes( MediaType.APPLICATION_JSON );
					final RestResponse theResponse = theRequest.toRestResponse();
					if ( theResponse.getHttpStatusCode().isSuccessStatus() ) {
						final HttpBodyMap theHttpBody = theResponse.getResponse();
						final String tmp = theHttpBody.get( HalMap.HAL_LINKS, HalMap.HAL_PROFILE, HalMap.HAL_HREF );
						if ( tmp != null ) {
							_profileLink = tmp;
						}
					}
				}
			}
		}
		return _profileLink;
	}

	/**
	 * Normalizes a relation's link.
	 * 
	 * @param aRelationLink The relation's link to be normalized.
	 * 
	 * @return The normalized link.
	 */
	private String toRelation( String aRelationLink ) {
		if ( aRelationLink != null && aRelationLink.length() != 0 ) {
			int theIndex = aRelationLink.lastIndexOf( '/' );
			if ( theIndex != -1 && theIndex < aRelationLink.length() ) {
				aRelationLink = aRelationLink.substring( theIndex + 1 );
				theIndex = aRelationLink.indexOf( '#' );
				if ( theIndex != -1 ) {
					aRelationLink = aRelationLink.substring( 0, theIndex );
				}
			}
		}
		return aRelationLink;
	}

	/**
	 * Determines the HAL entity's "self" HREF from the provided
	 * {@link HttpBodyMap}.
	 * 
	 * @param aHttpBody The {@link HttpBodyMap} from which to retrieve the
	 *        "self" reference.
	 * 
	 * @return The according "self" reference or null if there is none such HREF
	 *         reference.
	 */
	private String toSelfHref( HttpBodyMap aHttpBody ) {
		return aHttpBody.get( HalMap.HAL_PATH_SELF_HREF );
	}

	/**
	 * Validates the given URL to throw a {@link MalformedURLException} when it
	 * is malformed.
	 * 
	 * @param aUrl The URL to be validated.
	 * 
	 * @return The URL if successfully validated.
	 * 
	 * @throws MalformedURLException Thrown to indicate that a malformed URL has
	 *         occurred.
	 */
	private String toValidUrl( String aUrl ) throws MalformedURLException {
		final Url theUrl = new Url( aUrl );
		return theUrl.toHttpUrl();
	}

	/**
	 * Generic HTTP-Response handler, creating an exception in case the response
	 * was other than success.
	 * 
	 * @param aMessage The custom aMessage to include in an exception if thrown.
	 * @param aResponse The HTTP-Response to process and generate the exception
	 *        if required.
	 * @param aRequest The HTTP-Request which to include in case of an exception
	 *        being thrown.
	 * 
	 * @throws HttpStatusException thrown in case the HTTP-Response is anything
	 *         other than success.
	 */
	private void handleHttpStatusException( String aMessage, RestRequest aRequest, RestResponse aResponse ) throws HttpStatusException {
		if ( !aResponse.getHttpStatusCode().isSuccessStatus() ) {
			throw toHttpStatusExcpetion( aMessage, aRequest, aResponse );
		}
	}

	/**
	 * Generates a {@link HttpResponseException} with additional information.
	 * 
	 * @param aMessage The custom aMessage to include in the exception.
	 * @param aResponse The HTTP-Response from which to generate the exception.
	 * @param aRequest The HTTP-Request which to include in the exception being
	 *        generated.
	 * 
	 * @throws HttpStatusException thrown in case the HTTP-Response is anything
	 *         other than success.
	 */
	private HttpStatusException toHttpStatusExcpetion( String aMessage, RestRequest aRequest, RestResponse aResponse ) {
		String theBody = "";
		try {
			theBody = "HTTP-Response body was: " + aResponse.getHttpBody();
		}
		catch ( Exception ignore ) {}
		return aResponse.getHttpStatusCode().toHttpStatusException( aMessage + ": HTTP-Request for URL <" + aRequest.getUrl().toHttpUrl() + "> with method <" + aRequest.getHttpMethod().toString() + "> caused a HTTP-Status-Code <" + aResponse.getHttpStatusCode() + "> (<" + aResponse.getHttpStatusCode().getStatusCode() + ">) for the HTTP-Response." + theBody );
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Implementation of the {@link HalData} interface with access to the
	 * enclosing {@link HalClient}.
	 */
	private class HalDataClient extends HalData {

		private static final long serialVersionUID = 1L;

		public HalDataClient() {}

		@Override
		public void importHref( TraversalMode aMode, String aPath, FormFields aQueryFields ) throws HttpStatusException {
			final String thePathHref = toPath( aPath, META_DATA_SELF );
			final String theHref = get( thePathHref );
			if ( theHref != null ) {
				try {
					final HalData theHalData = read( theHref, aMode, aQueryFields );
					insertTo( aPath, (CanonicalMap) theHalData );
				}
				catch ( MalformedURLException e ) {
					throw new InternalClientErrorException( "Internal client error as of a malformed" + ( theHref != null ? " <" + theHref + ">" : "" ) + " URL!", e );
				}
			}
			throw new IllegalStateException( "The state of this instance does not allow this operation to be executed as the path <" + thePathHref + "> does not point to any HREF." );
		}

		@Override
		public HalData exportHref( TraversalMode aMode, String aPath ) throws HttpStatusException {
			throw new UnsupportedOperationException( "The implementing class does not provide any support for this functionality which required access something like a <" + HalClient.class.getSimpleName() + "> implementation." );
		}
	}
}
