// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.hal;

import org.refcodes.struct.CanonicalMap;

/**
 * The {@link TraversalMode} specifies on how data in retrieved from a
 * HAL-Resource. HAL-Data is processed depending on the {@link TraversalMode}
 * bing provided to the operation. The {@link TraversalMode} specifies a
 * combination of the below means:
 * <p>
 * Self-HREFs are links to the retrieved data entities themselves and, when
 * enabled by the {@link TraversalMode}, are stored below a
 * https://www.metacodes.pro Meta-Data section relative to the path of the
 * entity in question.
 * <p>
 * Dangling-HREFs are links to the data entities skipped (not retrieved) and,
 * when enabled, are stored below a https://www.metacodes.pro Meta-Data section
 * relative to the path of the entity in question.
 * <p>
 * Importing children, when enabled by the {@link TraversalMode}, instructs the
 * operation in question to retrieve the data referenced by the HREF. This means
 * that the child elements of an entity are retrieved too. Depending on the
 * result of the {@link TraversalMode#nextMode()} operation, just the direct
 * children are retrieved, all children recursively are retrieved or no children
 * at all are retrieved.
 * <P>
 * Dangling HREFs are considered to be those links which were not retrieved as
 * of the {@link TraversalMode} and its {@link TraversalMode#nextMode()}
 * configuration as well as those HREFs which are skipped as of HREF reference
 * cycles which would aCause infinite loops upon recursive HREF retrieval.
 */
public enum TraversalMode {

	/**
	 * Hypermedia references (HREF) pointed to by a path (in terms of a key
	 * regarding the concept of the {@link CanonicalMap}) are handled as
	 * follows:
	 * <ul>
	 * <li>Data related to direct HREFs is imported: No
	 * <li>Data related to child HREFs is imported recursively: No
	 * <li>Self-HREF Meta-Data for retrieved HAL data: No
	 * <li>Dangling-HREF Meta-Data for skipped HAL data: No
	 * </ul>
	 */
	NONE(false, false, false),

	/**
	 * Hypermedia references (HREF) pointed to by a path (in terms of a key
	 * regarding the concept of the {@link CanonicalMap}) are handled as
	 * follows:
	 * <ul>
	 * <li>Data related to direct HREFs is imported: No
	 * <li>Data related to child HREFs is imported recursively: No
	 * <li>Self-HREF Meta-Data for retrieved HAL data: Yes
	 * <li>Dangling-HREF Meta-Data for skipped HAL data: No
	 * </ul>
	 */
	NONE_KEEP_SELF_HREFS(false, true, false),

	/**
	 * Hypermedia references (HREF) pointed to by a path (in terms of a key
	 * regarding the concept of the {@link CanonicalMap}) are handled as
	 * follows:
	 * <ul>
	 * <li>Data related to direct HREFs is imported: No
	 * <li>Data related to child HREFs is imported recursively: No
	 * <li>Self-HREF Meta-Data for retrieved HAL data: No
	 * <li>Dangling-HREF Meta-Data for skipped HAL data: Yes
	 * </ul>
	 */
	NONE_KEEP_DANGLING_HREFS(false, false, true),

	/**
	 * Hypermedia references (HREF) pointed to by a path (in terms of a key
	 * regarding the concept of the {@link CanonicalMap}) are handled as
	 * follows:
	 * <ul>
	 * <li>Data related to direct HREFs is imported: No
	 * <li>Data related to child HREFs is imported recursively: No
	 * <li>Self-HREF Meta-Data for retrieved HAL data: Yes
	 * <li>Dangling-HREF Meta-Data for skipped HAL data: Yes
	 * </ul>
	 */
	NONE_KEEP_SELF_HREFS_KEEP_DANGLING_HREFS(false, true, true),

	/**
	 * Hypermedia references (HREF) pointed to by a path (in terms of a key
	 * regarding the concept of the {@link CanonicalMap}) are handled as
	 * follows:
	 * <ul>
	 * <li>Data related to direct HREFs is imported: Yes
	 * <li>Data related to child HREFs is imported recursively: No
	 * <li>Self-HREF Meta-Data for retrieved HAL data: No
	 * <li>Dangling-HREF Meta-Data for skipped HAL data: No
	 * </ul>
	 */
	IMPORT_CHILDREN(true, false, false),

	/**
	 * Hypermedia references (HREF) pointed to by a path (in terms of a key
	 * regarding the concept of the {@link CanonicalMap}) are handled as
	 * follows:
	 * <ul>
	 * <li>Data related to direct HREFs is imported: Yes
	 * <li>Data related to child HREFs is imported recursively: No
	 * <li>Self-HREF Meta-Data for retrieved HAL data: Yes
	 * <li>Dangling-HREF Meta-Data for skipped HAL data: No
	 * </ul>
	 */
	IMPORT_CHILDREN_KEEP_SELF_HREFS(true, true, false),

	/**
	 * Hypermedia references (HREF) pointed to by a path (in terms of a key
	 * regarding the concept of the {@link CanonicalMap}) are handled as
	 * follows:
	 * <ul>
	 * <li>Data related to direct HREFs is imported: Yes
	 * <li>Data related to child HREFs is imported recursively: No
	 * <li>Self-HREF Meta-Data for retrieved HAL data: No
	 * <li>Dangling-HREF Meta-Data for skipped HAL data: Yes
	 * </ul>
	 */
	IMPORT_CHILDREN_KEEP_DANGLING_HREFS(true, false, true),

	/**
	 * Hypermedia references (HREF) pointed to by a path (in terms of a key
	 * regarding the concept of the {@link CanonicalMap}) are handled as
	 * follows:
	 * <ul>
	 * <li>Data related to direct HREFs is imported: Yes
	 * <li>Data related to child HREFs is imported recursively: No
	 * <li>Self-HREF Meta-Data for retrieved HAL data: Yes
	 * <li>Dangling-HREF Meta-Data for skipped HAL data: Yes
	 * </ul>
	 */
	IMPORT_CHILDREN_KEEP_SELF_HREFS_KEEP_DANGLING_HREFS(true, true, true),

	/**
	 * Hypermedia references (HREF) pointed to by a path (in terms of a key
	 * regarding the concept of the {@link CanonicalMap}) are handled as
	 * follows:
	 * <ul>
	 * <li>Data related to direct HREFs is imported: Yes
	 * <li>Data related to child HREFs is imported recursively: Yes
	 * <li>Self-HREF Meta-Data for retrieved HAL data: No
	 * <li>Dangling-HREF Meta-Data for skipped HAL data: No
	 * </ul>
	 */
	RECURSIVE_IMPORT_CHILDREN(true, false, false),

	/**
	 * Hypermedia references (HREF) pointed to by a path (in terms of a key
	 * regarding the concept of the {@link CanonicalMap}) are handled as
	 * follows:
	 * <ul>
	 * <li>Data related to direct HREFs is imported: Yes
	 * <li>Data related to child HREFs is imported recursively: Yes
	 * <li>Self-HREF Meta-Data for retrieved HAL data: Yes
	 * <li>Dangling-HREF Meta-Data for skipped HAL data: No
	 * </ul>
	 */
	RECURSIVE_IMPORT_CHILDREN_KEEP_SELF_HREFS(true, true, false),

	/**
	 * Hypermedia references (HREF) pointed to by a path (in terms of a key
	 * regarding the concept of the {@link CanonicalMap}) are handled as
	 * follows:
	 * <ul>
	 * <li>Data related to direct HREFs is imported: Yes
	 * <li>Data related to child HREFs is imported recursively: Yes
	 * <li>Self-HREF Meta-Data for retrieved HAL data: No
	 * <li>Dangling-HREF Meta-Data for skipped HAL data: Yes
	 * </ul>
	 */
	RECURSIVE_IMPORT_CHILDREN_KEEP_DANGLING_HREFS(true, false, true),

	/**
	 * Hypermedia references (HREF) pointed to by a path (in terms of a key
	 * regarding the concept of the {@link CanonicalMap}) are handled as
	 * follows:
	 * <ul>
	 * <li>Data related to direct HREFs is imported: Yes
	 * <li>Data related to child HREFs is imported recursively: Yes
	 * <li>Self-HREF Meta-Data for retrieved HAL data: Yes
	 * <li>Dangling-HREF Meta-Data for skipped HAL data: Yes
	 * </ul>
	 */
	RECURSIVE_IMPORT_CHILDREN_KEEP_SELF_HREFS_KEEP_DANGLING_HREFS(true, true, true);

	private boolean _isSelfKeepHrefs;
	private boolean _isImportChildren;
	private boolean _isKeepDanglingHrefs;

	private TraversalMode( boolean isImportChildren, boolean isKeepSelfHrefs, boolean isKeepDanglingHrefs ) {
		_isSelfKeepHrefs = isKeepSelfHrefs;
		_isImportChildren = isImportChildren;
		_isKeepDanglingHrefs = isKeepDanglingHrefs;
	}

	/**
	 * Indicates that HREFs for HAL-Data being loaded are(!) preserved as values
	 * below the according paths of the attributes in question in
	 * https://www.metacodes.pro Meta-Data sections.
	 * <p>
	 * Self-HREFs are are a result of loaded HAL-Data as of following a HREF.
	 * 
	 * @return True in case the according HREFs are(!) preserved as values below
	 *         https://www.metacodes.pro Meta-Data sections of the according
	 *         paths.
	 */
	public boolean isKeepSelfHrefs() {
		return _isSelfKeepHrefs;
	}

	/**
	 * Indicates HREFs for HAL-Data not being loaded (e.g. we have a dangling
	 * link) are(!) preserved below the according paths of the attributes in
	 * question in https://www.metacodes.pro Meta-Data sections.
	 * <p>
	 * Dangling-HREFs may be a result of not loading direct HREFs or recursive
	 * HREFs or we encountered a cycle when loading HREFs recursively.
	 * 
	 * @return True in case the according HREFs are(!) preserved as values below
	 *         https://www.metacodes.pro Meta-Data sections of the according
	 *         paths.
	 */
	public boolean isKeepDanglingHrefs() {
		return _isKeepDanglingHrefs;
	}

	/**
	 * Indicates that HREFs for HAL-Data being loaded are followed and loaded as
	 * well.
	 * <p>
	 * The method {@link TraversalMode#nextMode()} determines whether the
	 * children of this HAL-Data is being loaded as well. Depending on the
	 * configuration of the according {@link TraversalMode} we may just import
	 * the direct childtren of a HAL-Data or all children recursively.
	 * 
	 * @return True in case the according HAL-Data's children are imported as
	 *         well.
	 */
	public boolean isImportChildren() {
		return _isImportChildren;
	}

	/**
	 * Returns the next TraversalMode to be applied to the children when the
	 * current retrieval operation has finished.
	 * 
	 * @return The according {@link TraversalMode}.
	 */
	public TraversalMode nextMode() {
		switch ( this ) {
		case RECURSIVE_IMPORT_CHILDREN -> {
			return RECURSIVE_IMPORT_CHILDREN;
		}
		case RECURSIVE_IMPORT_CHILDREN_KEEP_SELF_HREFS -> {
			return RECURSIVE_IMPORT_CHILDREN_KEEP_SELF_HREFS;
		}
		case RECURSIVE_IMPORT_CHILDREN_KEEP_SELF_HREFS_KEEP_DANGLING_HREFS -> {
			return RECURSIVE_IMPORT_CHILDREN_KEEP_SELF_HREFS_KEEP_DANGLING_HREFS;
		}
		case RECURSIVE_IMPORT_CHILDREN_KEEP_DANGLING_HREFS -> {
			return RECURSIVE_IMPORT_CHILDREN_KEEP_DANGLING_HREFS;
		}
		case IMPORT_CHILDREN -> {
			return NONE;
		}
		case IMPORT_CHILDREN_KEEP_SELF_HREFS -> {
			return TraversalMode.NONE_KEEP_SELF_HREFS;
		}
		case IMPORT_CHILDREN_KEEP_DANGLING_HREFS -> {
			return TraversalMode.NONE_KEEP_DANGLING_HREFS;
		}
		case IMPORT_CHILDREN_KEEP_SELF_HREFS_KEEP_DANGLING_HREFS -> {
			return NONE_KEEP_SELF_HREFS_KEEP_DANGLING_HREFS;
		}
		case NONE -> {
			return null;
		}
		case NONE_KEEP_SELF_HREFS -> {
			return null;
		}
		case NONE_KEEP_DANGLING_HREFS -> {
			return null;
		}
		case NONE_KEEP_SELF_HREFS_KEEP_DANGLING_HREFS -> {
			return null;
		}
		default -> {
		}
		}
		return null;
	}
}