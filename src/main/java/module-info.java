module org.refcodes.hal {
	requires transitive org.refcodes.web;
	requires org.refcodes.rest;
	requires java.logging;

	exports org.refcodes.hal;

	opens org.refcodes.hal; // Opens access for unnamed modules (our spring boot test)
}
